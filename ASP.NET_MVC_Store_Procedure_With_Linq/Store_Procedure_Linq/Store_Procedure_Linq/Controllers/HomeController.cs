﻿using Store_Procedure_Linq.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store_Procedure_Linq.Controllers
{
    public class HomeController : Controller
    {
        StudentDataContext db = new StudentDataContext();
        // GET: Home
        public ActionResult Index()
        {
            var data=db.spShowStudents();
            return View(data);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create( Student s)
        {
            db.spInsert(s.id,s.name,s.gender);
            db.SubmitChanges();
            return View();
        }
        public ActionResult Details(int Id)
        {
            var data = db.Students.Where(x => x.id==Id).FirstOrDefault();
            return View(data);
        }

        public ActionResult Edit(int Id)
        {
            var data = db.Students.Where(x => x.id == Id).FirstOrDefault();
            return View(data);
        }
        [HttpPost]
        public ActionResult Edit(int Id,Student s)
        {
            var data = db.spupdate(s.id, s.name, s.gender);
            db.SubmitChanges();
            return View();
        }
        public ActionResult Delete(int Id)
        {
            var data = db.Students.Where(x => x.id == Id).FirstOrDefault();
            return View(data);
        }
        [HttpPost]
        public ActionResult Delete(int Id,Student s)
        {
            var data = db.spdelete(s.id);
            return View();
        }
    }
}