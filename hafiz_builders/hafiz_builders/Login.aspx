﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="hafiz_builders.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Log In</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0 " />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="Bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="CSS/login.css" />
</head>
<body style="background-image: url('Images/Home.jpg');" >
 <form id="frm" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <div class="container">
                 <div class="row ">
                     <div class="col-lg-3 col-sm-12  main_col_div">
                         <img src="Images/images.png" alt="Alternate Text" class="user" />
                         <h4>HAFIZ BUILDERS</h4>
                         <asp:TextBox ID="TextBox1" runat="server" CssClass="txtemail mt-5" placeholder="Enter Username" />
                         <asp:TextBox ID="TextBox2" TextMode="Password" runat="server" CssClass="txtpass" placeholder="*********" />
                         <asp:Button Text="Sign In" CssClass="btnsubmit mt-5" runat="server" OnClick="Unnamed1_Click" />
                         <asp:LinkButton Text="Forget Password" CssClass="btnforget mt-5" runat="server" />
                     </div>
                 </div>
             </div>
         </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
