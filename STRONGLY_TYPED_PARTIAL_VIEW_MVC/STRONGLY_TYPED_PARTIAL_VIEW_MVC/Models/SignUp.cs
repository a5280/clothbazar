﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STRONGLY_TYPED_PARTIAL_VIEW_MVC.Models
{
    public class SignUp
    {
        public int ID { get; set; }
        public string Username { get; set; }

        public string Password { get; set; }

        public string Gender { get; set; }
        public string Email { get; set; }

        public string Comment { get; set; }


    }
}