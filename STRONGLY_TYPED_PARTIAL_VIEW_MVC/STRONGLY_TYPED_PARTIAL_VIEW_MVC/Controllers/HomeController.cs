﻿using STRONGLY_TYPED_PARTIAL_VIEW_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace STRONGLY_TYPED_PARTIAL_VIEW_MVC.Controllers
{
    public class HomeController : Controller
    {
       List<Products> ProductList=new List<Products>()
        {
           new Products { ID=1,Name="Nike",Price=1234,Image="~/Image/nike.jpg"},
              new Products { ID=2,Name="Reebok",Price=214,Image="~/Image/mens-sport-shoes.jpg"},
                 new Products { ID=3,Name="Puma",Price=4132,Image="~/Image/photo.jpg"}
        };
        // GET: Home
        public ActionResult Index()
        {
            return View(ProductList);
        }
        public ActionResult About()
        {
            return View();
        }
        public ActionResult Contact()
        {
            return View();
        }
    }
}