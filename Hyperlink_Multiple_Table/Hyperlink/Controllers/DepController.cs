﻿using Hyperlink.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hyperlink.Controllers
{
    public class DepController : Controller
    {
        FormsAuthenticationEntities db = new FormsAuthenticationEntities();
        // GET: Dep
        public ActionResult Index()
        {
            var data = db.Departments.ToList();
            return View(data);
        }
    }
}