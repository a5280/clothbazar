﻿using Hyperlink.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hyperlink.Controllers
{
    public class HomeController : Controller
    {
        FormsAuthenticationEntities db = new FormsAuthenticationEntities();
        // GET: Home
        public ActionResult Index(int deprId)
        {
            List<Employee> data = db.Employees.Where(x => x.Dep_Id== deprId).ToList();
            return View(data);
        }

        public ActionResult Details(int id)
        {
            var data = db.Employees.Where(x => x.ID == id).FirstOrDefault();
            return View(data);
        }
    }
}