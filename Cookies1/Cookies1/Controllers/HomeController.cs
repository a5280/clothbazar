﻿using Cookies1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Cookies1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public   ActionResult Index(User u)
        {
            if(ModelState.IsValid==true)
            {
                //encrypt cookie
               


                //create cookies
                HttpCookie cookie = new HttpCookie("Login");

                byte[] cookietext = ASCIIEncoding.ASCII.GetBytes(u.Username);
                string encryptedValue = Convert.ToBase64String(cookietext);
                cookie["name"] = encryptedValue;

                byte[] cookietext1 = ASCIIEncoding.ASCII.GetBytes(u.Password);
                string encryptedValue1 = Convert.ToBase64String(cookietext1);
                cookie["pass"] = encryptedValue1;
           
                //add cookies browser
                HttpContext.Response.Cookies.Add(cookie);
               
                //expire cookie
                cookie.Expires = DateTime.Now.AddDays(3);
                ModelState.Clear();
       
                return  RedirectToAction("Index","SecondPage");
            }

            return View();
        }
    }
}