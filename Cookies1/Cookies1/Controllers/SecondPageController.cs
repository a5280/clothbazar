﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Cookies1.Controllers
{
    public class SecondPageController : Controller
    {
        public object TextArea1 { get; private set; }

        // GET: SecondPage
        public ActionResult Index()
        {

            
            HttpCookie cookie = Request.Cookies["Login"];
            if (cookie !=null)
            {


                string encryptedValue = Request.Cookies["name"].Value.ToString();
                byte[] cookietext = Convert.FromBase64String(encryptedValue);
                string decryptedValuename = ASCIIEncoding.ASCII.GetString(cookietext);
                ViewBag.Message = decryptedValuename;


                //string encryptedValue1 = Request.Cookies["pass"].Value.ToString();
                //byte[] cookietext1 = Convert.FromBase64String(encryptedValue1);
                //string decryptedValuepass = ASCIIEncoding.ASCII.GetString(cookietext1);
                //ViewBag.Message1 = decryptedValuepass;

            


            }
            else
            {
                return RedirectToAction("Index","Home");
            }
            return View();
        }
    }
}