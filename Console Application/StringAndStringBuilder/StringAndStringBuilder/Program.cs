﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
namespace StringAndStringBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch spw = new Stopwatch();
            string a1 = "Mehran";
            spw.Start();
            for (int i = 0; i < 100000; i++)
            {
                a1 = a1 + i;
            }
            spw.Stop();

            Stopwatch sp1 = new Stopwatch();
            StringBuilder Sb = new StringBuilder("Mehran");
            sp1.Start();
            for (int i = 0; i < 100000; i++)
            {
                Sb.Append(i);
            }
            sp1.Stop();
            Console.WriteLine("Time String {0}",spw.ElapsedMilliseconds);
            Console.WriteLine("Time StringBuilder {0}", sp1.ElapsedMilliseconds);
            Console.ReadLine();
        }
    }
}
