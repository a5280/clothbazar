﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_method
{
    class Program
    {
        public delegate void sum(int a);
        public static void sum1(sum del,int a)
        {
            a += 10;
            del.Invoke(a);
        }
        static void Main(string[] args)
        {
            //sum s = delegate (int a)
            //  {
            //      a += 10;
            //      Console.WriteLine(a);
            //      return a;
            //  };
            //s.Invoke(5);
            Program.sum1(delegate (int num)
            {
                num += 10;
                Console.WriteLine(num);
            },5);
            Console.ReadLine();
        }
    }
}
