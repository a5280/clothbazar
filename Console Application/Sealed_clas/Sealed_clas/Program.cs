﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sealed_clas
{
  sealed class parent
    {
        public void print1()
        {
            Console.WriteLine("Parent Method");
        }
    }
    class child : parent
    {
        public void print2()
        {
            Console.WriteLine("Parent Method");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            parent c = new parent();
            c.print1();
            child ch = new child();
            ch.print2();
            Console.ReadLine();
        }
    }
}
