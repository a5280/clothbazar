﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Params_Keyword
{
    class emp
    {
        public static void sum(params int[] arr)
        {
            int sum = 0;
            foreach (var item in arr)
            {
                sum = sum + item;
            }
            Console.WriteLine(sum);

        }
    }
    class Program
    {

        static void Main(string[] args)
        {
            emp.sum(10,20,30,40,50,60,70,80);
            Console.ReadLine();
        }
    }
}
