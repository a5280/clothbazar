﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Queues
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue Q = new Queue();
            Q.Enqueue(12);
            Q.Enqueue("Mehran");
            Q.Enqueue("Male");
            Q.Enqueue("Manger");
            Q.Enqueue(12.54);

            foreach (object item in Q)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("<------------>");
            Q.Dequeue();
            Console.WriteLine(Q.Count);
            Console.WriteLine("<------------>");

            foreach (object item in Q)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
        }
    }
}
