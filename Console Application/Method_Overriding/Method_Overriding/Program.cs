﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Method_Overriding
{
     class  parent
    {
        public virtual void print()
        {             
            Console.WriteLine("Parent Method Invoke");
        }
    }

    class child : parent
    {
        public override void print()
        {
            base.print();
        
            Console.WriteLine("child Method Invoke");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            child cd = new child();
            cd.print();
            Console.ReadLine();
        }
    }
}
