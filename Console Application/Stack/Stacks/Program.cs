﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace Stacks
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack S = new Stack();
            S.Push(12);
            S.Push("Mehran");
            S.Push("Manager");
            S.Push("Software");
            S.Push(null);
            S.Push("Software");

            foreach (object item in S)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("<------------>");
            Console.WriteLine(S.Pop());
            Console.WriteLine(S.Peek());
            Console.WriteLine(S.Contains("Mehran"));
            Console.WriteLine(S.Count);
            Console.WriteLine("<------------>");
            foreach (object item in S)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("<------------>");
            S.Clear();
            foreach (object item in S)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
        }
    }
}
