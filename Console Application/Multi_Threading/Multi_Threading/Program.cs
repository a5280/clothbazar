﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
namespace Multi_Threading
{
    class Program
    {
        public static void fun1()
        {
            for (int i = 0; i < 50; i++)
            {
                Console.WriteLine("Fun1 "+i);
            }
        }
        public static void fun2()
        {
            for (int i = 0; i < 50; i++)
            {
                Console.WriteLine("Fun2 " + i);
                Thread.Sleep(5000);
            }
        }
        public static void fun3()
        {
            for (int i = 0; i < 50; i++)
            {
                Console.WriteLine("Fun3 " + i);
            }
        }
        static void Main(string[] args)
        {

            //Thread t = Thread.CurrentThread;
            //t.Name = "Main Thread";
            //Console.WriteLine("Current Thread : "+Thread.CurrentThread.Name);

            Thread T1 = new Thread(fun1);
            Thread T2 = new Thread(fun2);
            Thread T3 = new Thread(fun3);

            T1.Start();
            T2.Start();
            T3.Start();
            //Program.fun1();
            //Program.fun2();
            //Program.fun3();
            Console.ReadLine();
        }
    }
}
