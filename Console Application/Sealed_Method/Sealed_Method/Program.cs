﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sealed_Method
{
  public class  class1
    {
        public virtual void show()
        {
            Console.WriteLine("This is method C1");
        }

    }
    class class2 : class1
    {
        public sealed override void show()
        {
            Console.WriteLine("This is method C2");
        }
    }
    class class3 : class2
    {
        public override void show()
        {
            Console.WriteLine("This is method C3");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
