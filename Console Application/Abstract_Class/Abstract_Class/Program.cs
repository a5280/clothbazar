﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Class
{
 abstract class employees
    {
      public int Ploat_id;
      public string Fname;
      public string Sname;
        
      public abstract void Details();

        //Contractor
        public employees()
        {
            Console.WriteLine("\t Employees Details\n");
        }
        //Abstract property
        public abstract int Id
        {
            get; set;
        }

    }
    class Woodemp : employees
    {
      public int salary;
      public string work;
        int id;
        public override void Details()
        {
            Console.WriteLine("\t Ploat_ID : {0}", Ploat_id);
            Console.WriteLine("\t First Name : {0}", Fname);
            Console.WriteLine("\t Second Name : {0}", Sname);
            Console.WriteLine("\t Salary : {0}", salary);
            Console.WriteLine("\t Work : {0}", work);
        }

        public override int Id
        {
            get
            {
                return this.id;
            }

            set
            {
                this.id = value;
            }
        }
    }
    class Constractionemp :employees
    {
       public  int salary;
       public  string work;
        int id;
        public override void Details()
        {
            Console.WriteLine("\t Ploat_ID : {0}", Ploat_id);
            Console.WriteLine("\t First Name : {0}", Fname);
            Console.WriteLine("\t Second Name : {0}", Sname);
            Console.WriteLine("\t Salary : {0}", salary);
            Console.WriteLine("\t Work : {0}", work);
        }
        public override int Id
        {
            get
            {
                return this.id;
            }

            set
            {
                this.id = value;
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {           
            Woodemp we = new Woodemp();
            we.Id = 102;
            Console.WriteLine("\t Employee ID :{0}",we.Id);
            we.Ploat_id = 1;
            we.Fname = "Talha";
            we.Sname = "Mehar";
            we.work = "Wood";
            we.salary = 25000;
            we.Details();

            Console.WriteLine("\t <-------------------->");

            Constractionemp Con = new Constractionemp();
            Con.Id = 103;
            Console.WriteLine("\t Employee ID :{0}",Con.Id);
            Con.Ploat_id = 2;
            Con.Fname = "Muhammad";
            Con.Sname = "Mehran";
            Con.work = "Constraction";
            Con.salary = 30000;
            Con.Details();

            Console.ReadLine();
            
        }
    }
}
