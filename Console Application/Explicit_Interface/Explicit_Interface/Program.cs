﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Explicit_Interface
{
    interface I1
    {
        void show();
    }
    interface I2
    {
        void show();
    }
    class emp : I1, I2
    {
        void I2.show()
        {
            Console.WriteLine("This is method I2");
        }

        void I1.show()
        {
            Console.WriteLine("This is method I1");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            //emp e = new emp();
            //((I1)e).show();
            //((I2)e).show();

            //Parent Object Refrence variable 
            //I1 e = new emp();
            //e.show();
            Console.ReadLine();
        }
    }
}
