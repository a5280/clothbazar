﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project2;

namespace Inheritance
{
    class PermanentEmployee : Employee
    {
       public  string details;

        public PermanentEmployee():base("Employee Constructor")
        {

        }
   
    }
    class VisitedEmployee : PermanentEmployee 
    {
      public string whytovisite;
    }

    class Program
    {
        static void Main(string[] args)
        {
            PermanentEmployee pe = new PermanentEmployee();
            pe.roll = -11;
            pe.ename = "talha";
            Console.WriteLine(pe.roll);
            Console.WriteLine(pe.ename);

            //VisitedEmployee ve = new VisitedEmployee();
            //ve.id=13;
            //ve.name = "mehran";
            //Console.WriteLine(ve.id);
            //Console.WriteLine(ve.name);


            Console.ReadLine();
        }
    }
}
