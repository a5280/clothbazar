﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
   public  class Employee 
    {
        public int id;
        public string name;

        private int _roll;
        private string _name;

        public int roll
        {
            set{
                if (value >= 0)
                {
                    this._roll = value;
                }
                else
                {
                    Console.WriteLine("Enter Value Greater than 0");
                }
            }
            get
            {
                return this._roll;
            }
        }
        public string ename
        {
            set
            {
                if (string.IsNullOrEmpty(value)==true)
                {
                    Console.WriteLine("Enter Value Required");
                }
                else
                {
                    this._name = value;
                }
            }
            get
            {
                return this._name ;
            }
        }
   
        public Employee( string messgae)
        {
            Console.WriteLine(messgae);
        }
    }
}
