﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extension_Method
{
    class MainClass
    {
        static void Main()
        {
            Program p = new Program();
            p.fun1();
            p.fun2();
            p.fun3(30);
            Console.ReadLine();
        }
    }
}
