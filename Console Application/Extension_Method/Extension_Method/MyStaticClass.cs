﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extension_Method
{
  static class MyStaticClass
    {
        public static void fun3(this Program p,int a)
        {
            Console.WriteLine("Fun 3 "+a);
        }
    }
}
