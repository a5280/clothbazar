﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    public sealed class Singleton
    {
        private static readonly Singleton _instance = new Singleton();
    }
    interface IEmployee
    {
        void show();
    }
    interface IEmployee1
    {
        void show1();
    }
    interface IEmployee2 : IEmployee1,IEmployee
    {
        void show2();
    }
    class employee : IEmployee2
    {
        public void show()
        {
          Console.WriteLine("This is Method of IEmployee Show");
        }

        public void show1()
        {
            Console.WriteLine("This is Method of IEmployee Show1");
        }

        public void show2()
        {
            Console.WriteLine("This is Method of IEmployee Show2");
        }
    }
    class Test
    {
        readonly int read = 10;
        const int cons = 10;
        public Test()
        {
            read = 100;
           // cons = 100;
        }
        public void Check()
        {
            Console.WriteLine("Read only : {0}", read);
            Console.WriteLine("const : {0}", cons);
        }
    }
    //class employee : IEmployee,IEmployee1,IEmployee2
    //{
    //    public void show()
    //    {
    //        Console.WriteLine("This is Method of IEmployee Show"); 
    //    }

    //    public void show1()
    //    {
    //        Console.WriteLine("This is Method of IEmployee Show1");
    //    }

    //    public void show2()
    //    {
    //        Console.WriteLine("This is Method of IEmployee Show2");
    //    }
    //}

    class Program
    {
        static void Main(string[] args)
        {
            Test t = new Test();
            t.Check();
            employee e = new employee();
            e.show();
            e.show1();
            e.show2();
            Console.ReadLine();
        }
    }
}
