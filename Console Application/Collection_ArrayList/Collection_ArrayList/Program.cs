﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace Collection_ArrayList
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList Alist = new ArrayList();
            Alist.Add(2);
            Alist.Add("Mehran");
            Alist.Add('M');
            //Object data type store all data type
            foreach (object item in Alist)
            {
                Console.WriteLine(item);
            }
            Alist.Insert(1,"Male");
            foreach (object item in Alist)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
        }
    }
}
