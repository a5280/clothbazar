﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PassByRef_PassByValue_PassByOut
{
    class Program
    {           
        //CallByValue
        public static void CallByValue(int a)
        {
            a = a + 10;
            Console.WriteLine(a);
        }

        //CallByRef
        public static void CallByRef(ref int a)
        {
            a = a + 10;
            Console.WriteLine(a);
        }
        //CallByOut
        public static void CallByOut(out int a)
        {
            a = 20;
            Console.WriteLine(a);
        }
        static void Main(string[] args)
        {
            //CallByValue
            int value = 5;
            Program.CallByValue(value);
            Console.WriteLine(value);


            //CallByRef
            //int value =5;
            //Program.CallByRef(ref value);
            //Console.WriteLine(value);


            //CallByRef
            //int value;
            //Program.CallByOut(out value);
            //Console.WriteLine(value);

            Console.ReadLine();
        }
    }
}
