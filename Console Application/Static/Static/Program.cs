﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Static
{
    //static class not decralaration object because cannot decleare static class constructor
    static class Student
    {
        public static string abc = "ABC School";
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Student.abc);
            Console.WriteLine(Student.abc);

            Console.ReadLine();
        }
    }
}
