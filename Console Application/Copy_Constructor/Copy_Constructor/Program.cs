﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Copy_Constructor
{
    class Program
    {
       public  int roll;
        public string name;

        public Program( int roll,string name)
        {
            this.roll = roll;
            this.name = name;

        }

        public Program(Program e)
        {
            this.roll = e.roll;
            this.name = e.name;
        }

        static void Main(string[] args)
        {
            Program p = new Program(11,"mehran");
            Program p1 = new Program(p);
            Console.WriteLine(p.roll);
            Console.WriteLine(p.name);
            Console.WriteLine(p1.roll);
            Console.WriteLine(p1.name);
            Console.ReadLine();
        }
    }
}
