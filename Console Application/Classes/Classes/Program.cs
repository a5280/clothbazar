﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    class Program
    {
        class Student
        {
            int rollnum;
            string name, Nclass;
            public void setrollnum(int rollnum,string name,string Nclass)
            {
                this.rollnum = rollnum;
                this.name = name;
                this.Nclass = Nclass;

            }
            public void getrollnum()
            {
                Console.WriteLine("Student Detail : {0} {1} {2}",this.rollnum,this.name,this.Nclass);
            }
            
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Roll num");
           int roll=int.Parse( Console.ReadLine());

            Console.WriteLine("Enter Name");
            string name = Console.ReadLine();

            Console.WriteLine("Enter Class Name");
            string nclass = Console.ReadLine();

            Student dt = new Student();
            dt.setrollnum(roll,name,nclass);
            dt.getrollnum();
            Console.ReadLine();
        }
    }
}
