﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Value_Type_and_Refrence_Type
{
  
    class Program
    {
       // class Student
        struct Student
        {
            public int rollnum;
            public string name;
        }
        static void Main(string[] args)
        {
            Student st = new Student();
            st.rollnum = 12;
            st.name = "Talha";

            Student s1 = st;
            Student s2 = st;

            st.rollnum = 13;
            Console.WriteLine(st.rollnum);
            Console.WriteLine(s1.rollnum);
            Console.WriteLine(s2.rollnum);
            Console.ReadLine();
        }
    }
}
