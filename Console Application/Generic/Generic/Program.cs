﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic
{
    class Generic
    {
        public static void  ShowArray<T>(T[] Arr)
        {
            for (int i = 0; i < Arr.Length; i++)
            {
                Console.WriteLine(Arr[i]);
            }
        }
        public static bool Check<T>(T a, T b)
        {
            bool c = a.Equals(b);
            Console.WriteLine(typeof(T));
            return c;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            int[] Arrays = new int[3];
            Arrays[0] = 1;
            Arrays[1] = 1;
            Arrays[2] = 1;
            string[] name = { "Talha", "Moazzam", "Mehran" };
            float[] Fl = { 0.24f,2.3f,236.1f};
            Generic.ShowArray(Fl);
           // Console.WriteLine(Generic.Check(12, 12));
            Console.WriteLine(Generic.Check("Mehran", "Mehran"));
            Console.ReadLine();
        }
    }
}
