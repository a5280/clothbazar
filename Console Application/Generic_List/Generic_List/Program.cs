﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_List
{
    class Program
    {
        static void Main(string[] args)
        {
            //Employee emp = new Employee()
            //{
            //    Id=1,
            //    name="Mehran",
            //    gender="Male"
            //};
            //Employee emp1 = new Employee()
            //{
            //    Id = 2,
            //    name = "Talha",
            //    gender = "Male"
            //};
            //List<Employee> Emp = new List<Employee>();
            //Emp.Add(emp);
            //Emp.Add(emp1);

            //foreach (Employee item in Emp)
            //{
            //    Console.WriteLine("Employee ID : {0} Employee Name : {1} Employee Gender : {2}",item.Id,item.name,item.gender);

            //}

            List<int> num = new List<int>();
            num.Add(11);
            num.Add(12);
            num.Add(13);
            num.Add(14);
            num.Add(15);
            num.Add(16);
            num.Add(17);
            num.Add(18);
            num.Add(19);
            num.Add(20);
            foreach (int item in num)
            {
                Console.WriteLine(item);
            }
            num.AddRange(num);
            foreach (int item in num)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
        }
    }
}
