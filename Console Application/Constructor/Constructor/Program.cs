﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructor
{
    class Program
    {
        int roll;
        string name;
        string designation;
        static Program()
        {
            Console.WriteLine("Static Construction invoke");
        }
        public Program()
        {
            Console.WriteLine("Construction invoke");
        }
        public Program(int a)
        {
            Console.WriteLine("Construction Overloading {0}",a);
        }
        public  Program(int roll,string name,string designation)
        {
            this.roll = roll;
            this.name = name;
            this.designation = designation;
            
        }
        public int getrol()
        {
            return this.roll;
        }
        public string getname()
        {
            return this.name;
        }
        public string getdesi()
        {
            return this.designation;
        }

        public static int main(int a)
        {
            return a;
        }
        static void Main(string[] args)
        {
            Program p = new Program();
            Program pr = new Program();

            Program PL = new Program(10);

            Program.main(2);

            Program Pr = new Program(11, "Mehran", "Designation");
            Console.WriteLine("Student Roll {0}", Pr.getrol());
            Console.WriteLine("Student Name {0}", Pr.getname());
            Console.WriteLine("Student Designation {0}", Pr.getdesi());


            Console.ReadLine();
        }
    }
}
