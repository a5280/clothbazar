﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array
{
    class Program
    {
        static void Main(string[] args)
        {
    
            //float[] array_float = {2.3f,5.36f,458.3f};
            //string[] array_string= new string[] {"Talha","Moazzam","Mehran" };
            //int[] array_int = new int[4];

            //Student st = new Student();
            //Console.WriteLine(st.fun());
            //int a=Convert.ToInt32( st.fun());

            //array_int[0] = 1;
            //array_int[1] = 2;
            //array_int[2] = 3;
            //array_int[3] = 4;
            //for (int i = 0; i < array_string.Length; i++)
            //{
            //    Console.WriteLine(array_string[i]);
            //}
            //foreach (var item in array_int)
            //{
            //    Console.WriteLine(item);
            //}


            //int[,] array_2D = new int[2, 4]
            //{
            //    {11,22,33,44 },
            //    {55,66,77,88 }
            //};

            //for (int i = 0; i < array_2D.GetLength(0); i++)
            //{
            //    for (int j = 0; j < array_2D.GetLength(1); j++)
            //    {
            //        Console.Write(array_2D[i,j] +" ");
            //    }
            //    Console.WriteLine();
            //}

            int[][] arrayJegged = new int[2][];

            arrayJegged[0] = new[] { 11, 22, 33, 44, 55,66 };
            arrayJegged[1] = new[] { 66, 77, 88 };

            for (int i = 0; i < arrayJegged.GetLength(0); i++)
            {
                for (int j = 0; j < arrayJegged[i].Length; j++)
                {
                    Console.Write(arrayJegged[i][j] + " ");
                }
                Console.WriteLine();
            }

            //foreach (int[] item in arrayJegged)
            //{
            //    foreach (int  i in item)
            //    {
            //        Console.Write(i+" ");
            //    }
            //    Console.WriteLine();
            //}

            //User Input Array

            Console.WriteLine("Enter Array Range");
            int num= int.Parse(Console.ReadLine());

            int[] array = new int[num];

            for (int i = 0; i < num; i++)
            {
                Console.WriteLine("Enter Number Value");
                int data=int.Parse(Console.ReadLine());
                array[i] = data;
            }
            Console.WriteLine("Data");
            foreach (int item in array)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
            
        }
    }
}
