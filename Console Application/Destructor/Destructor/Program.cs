﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Destructor
{
    class Student
    {
        int roll;
        string name;
        public Student(int roll,string name)
        {
            this.roll = roll;
            this.name = name;
        }
        public int getroll()
        {
            return this.roll;
        }
        public string  getname()
        {
            return this.name;
        }
        ~Student()
        {
            Console.WriteLine("Destructor has been Invoked");
            Console.ReadLine();

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student dt = new Student(12,"mehran");
            
            Console.WriteLine(dt.getroll());
            Console.WriteLine(dt.getname());
         //   Console.ReadLine();

        }
    }
}
