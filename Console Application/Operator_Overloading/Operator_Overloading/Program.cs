﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operator_Overloading
{
    class Class1
    {
        public string  str;
        public int num;

        public static Class1 operator +(Class1 obj1, Class1 obj)
        {
            Class1 obj3 = new Class1();
            obj3.str = obj1.str +" "+ obj.str;
            obj3.num = obj1.num + obj.num;
            //Console.WriteLine(obj.str);
            //Console.WriteLine(obj.num);
            return obj3;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Class1 obj = new Class1();
            obj.str = "Mehran";
            obj.num = 13;

            Class1 obj1 = new Class1();
            obj1.str = "Muhammad";
            obj1.num = 12;

          
            Class1 obj3 = new Class1();
            obj3 = obj1 + obj;

            Console.WriteLine(obj3.str);
            Console.WriteLine(obj3.num);
             
            Console.ReadLine();
        }
    }
}
