﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_Class
{
    class Example<T>
    {
        T Box;
        public Example(T a)
        {
            this.Box = a;
        }
        public T GetValue()
        {
            return this.Box;
        }

        //public T Name
        //{
        //    set
        //    {
        //        this.Box = value;
        //    }
        //    get
        //    {
        //        return this.Box;
        //    }

        //}
    }
    class Program
    {
        static void Main(string[] args)
        {
            Example<int> e = new Example<int>(2);
            Console.WriteLine(e.GetValue());
            Example<string> e1 = new Example<string>("Generic");
            Console.WriteLine(e1.GetValue());

            //Example<string> e2 = new Example<string>();       
            //Console.WriteLine(e2.Name = "Mehran");
            //Console.ReadLine();

        }
    }
}
