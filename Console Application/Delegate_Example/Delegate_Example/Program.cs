﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_Example
{
    class Program
    {
        //Single Type Delegate
        public delegate void Mydetails();
        public delegate void Myid(int a);
        public delegate void Myname(string name,string fname);
        public delegate void Mydesig(string job);

        //Multiply Type Delegate
        public delegate int MyCalculation(int a,int b);

        //Anonymous Type Delegate
        public delegate void MyAllinfo(int id);

        //Call by referance
        public delegate int Myref(ref int a);

        //
        public delegate string MyPersonprint();

        public static void details()
        {
            Console.WriteLine("Details");
        }
        public static void id(int a)
        {
            a += 1;
            Console.WriteLine("ID : {0}",a);
        }
        public static void name(string name,string fname)
        {
            Console.WriteLine("First Name :" +" "+name+" "+"Second Name :"+" "+fname);
        }
        public static void designation(string job)
        {
            Console.WriteLine("Designation :{0}" ,job );
        }
        public static int Sum(int A,int B)
        {
            int sum = A + B;
            Console.WriteLine(sum);
            return sum;
        }
        public static int Minus(int A, int B)
        {
            int sum = A - B;
            Console.WriteLine(sum);
            return sum;
        }
        public static int Multiple(int A, int B)
        {
            int sum = A * B;
            Console.WriteLine(sum);
            return sum;
        }
        public static int Divide(int A, int B)
        {
            int sum = A / B;
            Console.WriteLine(sum);
            return sum;
        }
        //Anonymous Function /Method
        public static void Alldetails(MyAllinfo obj,string name,string fname,string designation)
        {
            string _name = name;
            string _fname = fname;
            string _designation = designation;
           // obj(2);
            for (int i = 0; i < 99; i++)
            {
                if (i % 25 == 0)
                {
                    obj(i);
                }
            }
            Console.WriteLine(_name);
            Console.WriteLine(_fname);
            Console.WriteLine(_designation);

        }
        //Call By Referance Function 
        public static int Refe(ref int a)
        {
            a = 10;
            Console.WriteLine(a);
            return a;
        }

        //Return type with print using Contructor
        int _a, _b;
        public Program(int a,int b)
        {
             _a = a;
             _b = b;
        }
        public  string Personprint()
        {
            return ("Value of A : "+_a+" "+"Value of B : "+_b);
        }
        static void Main(string[] args)
        {

            //Return With Value use Contructor in Delegate
            //Program p = new Program(14,15);
            //MyPersonprint Mp = new MyPersonprint(p.Personprint);
            //string output = Mp();
            //Console.WriteLine(output);

            //Referance Function Call
            //Myref mr = new Myref(Program.Refe);
            //int x = 15;
            //mr(ref x);

            //Get Output Anonymous Lambda
            //Program.Alldetails((a)=>
            //{
            //    a = 3;
            //    Console.WriteLine(a);
            //},"Muhammd","Mehran","Engineer");

            //Get Output Anonymous Method
            //Program.Alldetails(delegate (int a)
            //{
            //  //  a = 25;
            //    Console.WriteLine(a);
            //}, "Muhammad", "Mehran", "Software Engineer");


            //simple delegate method call 
            //Mydetails md = new Mydetails(Program.details);
            //md.Invoke();

            //simple delegate method call 
            //Myid mi = new Myid(Program.id);
            //mi.Invoke(5);

            //simple delegate method call 
            //Myname mn = new Myname(Program.name);
            //mn.Invoke("Muhammad","Mehran");

            //simple delegate method call 
            //Mydesig mde = new Mydesig(Program.designation);
            //mde.Invoke("Software Engineer");


            //Multiple method call single object 
            //MyCalculation obj = new MyCalculation(Program.Sum);
            //obj += Minus;
            //obj += Multiple;
            //obj += Divide;
            //int a = obj(11, 12);
            //Console.WriteLine("Value {0}", a);
            // Console.WriteLine("Value {0}", obj(11, 22));


            //Simple method call in class
            //Program p = new Program();
            //p.details();
            //p.id(0);
            //p.name("Muhammad","Mehran");
            //p.designation("Software Engineer");


            Console.ReadLine();
        }
    }
}
