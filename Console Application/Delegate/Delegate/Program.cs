﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate
{
    class Program
    {
        public delegate void Calculation(int a,int b);
        public delegate void geleshow();
        public delegate int gelesquare(int num);

        public static void show()
        {
            Console.WriteLine("Invoke");
        }
        public static int sqared(int num)
        {
            int square = num * num;
            return square;
        }
        public static void multiplication(int a, int b)
        {
            int mul = a * b;
            Console.WriteLine(mul);
        }
        public static void sum(int a,int b)
        {
            int sum = a + b;
            Console.WriteLine(sum);
        }
        public static void subtraction(int a, int b)
        {
            int sub = a - b;
            Console.WriteLine(sub);
        }
  
         static void Main(string[] args)
        {
            //Calculation c = new Calculation(Program.sum);
            //c += multiplication;
            //c -= subtraction;
            //c(12,13);

            //c.Invoke(12, 13);
            //c = multiplication;
            //c.Invoke(12, 13);
            //c = subtraction;
            //c(12, 13);

            //geleshow dt = new geleshow(Program.show);
            //dt.Invoke();
            gelesquare ds = new gelesquare(Program.sqared);
            Console.WriteLine(ds.Invoke(15));

            Console.ReadLine();
        }
    }
}
