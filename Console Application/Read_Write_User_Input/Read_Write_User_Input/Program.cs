﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Read_Write_User_Input
{
    class Program
    {
        enum WeekDay
        {
            Monday,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday,
            Sunday

        }

       
        static void Main(string[] args)
        {       
        //    Console.WriteLine(String.Format(new CustomFormat(), "{0}", 0));
        //    Console.WriteLine(String.Format(new CustomFormat(), "{0}", 911));
        //    Console.WriteLine(String.Format(new CustomFormat(), "{0}", 8490216));
        //    Console.WriteLine(String.Format(new CustomFormat(), "{0}", 4257884748));
        //    Console.WriteLine(String.Format(new CustomFormat(), "{0:N}", 0));
        //    Console.WriteLine(String.Format(new CustomFormat(), "{0:N}", 911));
        //    Console.WriteLine(String.Format(new CustomFormat(), "{0:N}", 8490216));
        //    Console.WriteLine(String.Format(new CustomFormat(), "{0:N}", 4257884748));
        //    Console.WriteLine(String.Format(new CustomFormat(), "{0:I}", 4257884748));

  //          Console.WriteLine(String.Format(new CustomFormat(), "-> {0:Reverse} <-", "Hello World"));
//            Console.WriteLine(String.Format(new CustomFormat(), "-> {0:03117350500} <-", 03117350500));

          //  Console.WriteLine("{0:c}",123.225);
            
            NumberFormatInfo nfi = new CultureInfo("hi-IN", false).NumberFormat;
            nfi = (NumberFormatInfo) nfi.Clone();
            nfi.CurrencySymbol = "?";

            Console.WriteLine(string.Format(nfi, "{0:c}", 112.236677));
            nfi.CurrencySymbol = "@";
            nfi.CurrencyPositivePattern = 3;
            Console.WriteLine(string.Format(nfi, "{0:c}", 112.236677));

            string money = "124.365";
            Console.WriteLine(string.Format("{0,10:C }",money));

            int  a = 20;
            int b = 30;
            string sum = (a > b) ? "grater" : "less";
            Console.WriteLine(sum);

            string av = "apple";
            switch (av)
            {
                case "bnana":
                    Console.WriteLine("Banana");
                    break;
                default:
                    Console.WriteLine("Default");
                    break;
            }

            //   DateTime dt = DateTime.Now;
            //Console.WriteLine("{0:d}",dt);
            //Console.WriteLine("{0:dd}", dt);
            //Console.WriteLine("{0:ddd}", dt);
            //Console.WriteLine("{0:dddd}", dt);
            //Console.WriteLine("{0:D}", dt);
            //Console.WriteLine("{0:f}", dt);
            //Console.WriteLine("{0:F}", dt);
            //Console.WriteLine("{0:g}", dt);
            //Console.WriteLine("{0:G}", dt);
            //Console.WriteLine("{0:d} {1:D}", dt,dt);
            //Console.WriteLine("{0:m}", dt);
            //Console.WriteLine("{0:MM}", dt);
            //Console.WriteLine("{0:MMM}", dt);
            //Console.WriteLine("{0:y}", dt);
            //Console.WriteLine("{0:yy}", dt);
            //Console.WriteLine("{0:yyyy}", dt);

            //  Console.WriteLine("<--------------> Time");
            //Console.WriteLine("{0:t}", dt);
            //Console.WriteLine("{0:T}", dt);
            //Console.WriteLine("{0:hh}", dt);
            //Console.WriteLine("{0:HH}", dt);
            //Console.WriteLine("{0:mm}", dt);
            //Console.WriteLine("{0:ss}", dt);
            //Console.WriteLine("{0:hh:mm:ss tt}", dt);
            //Console.WriteLine("{0:HH:mm:ss}", dt);
            //Console.WriteLine("{0:dd-MM-yyyy}", dt);

            //string name = "Mehran";
            //string where = "from pakistan";
            //Console.WriteLine($"Hello {name} {where}!");
            //Console.WriteLine("{0}",name);


            //float a = 2.22f;
            //float b = 223.5f;

            //UInt32 c =(UInt32)a + (UInt32)b;
            //int A = (int)WeekDay.Friday;
            //Console.WriteLine(WeekDay.Friday+" "+A);

            //var wd = (WeekDay)5;
            //Console.WriteLine(wd);



            //    //string value
            //    Console.WriteLine("Enter your first name");
            //    string fname = Console.ReadLine();
            //    Console.WriteLine("Your first name: "+fname);//concatenation
            //    Console.WriteLine("Your first name: {0}",fname);//placehorder
            //    //numeric value
            //    Console.WriteLine("Enter fisrt num");
            //    int a = int.Parse(Console.ReadLine());
            //    Console.WriteLine("Enter Second Num");
            //    int b =int.Parse(Console.ReadLine()); 
            //    int sum = a + b;
            //    bool conditionsum = a > b;
            //    Console.WriteLine("Sum of Two Num: {0}",sum);
            //    Console.WriteLine(conditionsum);
            //    uint ui = 5U;
            //    Console.WriteLine(ui);
            //    Console.WriteLine(int.MaxValue);                                          
            //    Console.WriteLine("\" \t Welcome to \n Home\"");  




            Console.ReadLine();
        }
    }
}
