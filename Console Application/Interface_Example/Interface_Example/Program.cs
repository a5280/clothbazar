﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface_Example
{
    //Simple INterface Example
    interface Ifile
    {
        void file();
    }
    class Files : Ifile
    {
        public void file()
        {
            Console.WriteLine("File upload successfully");
        }
    }
    //End
    //Inheritance Example
    interface IFile1
    {
        void Download1();
       
    }
    interface IFile2
    {
        void Download2();
    }
    class Files1 : IFile1, IFile2
    {
        public void Download1()
        {
            Console.WriteLine("File Download 1 successfully");
        }

        public void Download2()
        {
            Console.WriteLine("File Download 2 successfully");
        }
    }
    //End
    //Explicit Interface
    interface f1
    {
        void f();
    }
    interface f2
    {
        void f();
    }
    class File2 : f1, f2
    {
        void f2.f()
        {
            Console.WriteLine("This is method F2");
        }

        void f1.f()
        {
            Console.WriteLine("This is method F1");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            //Simple Interface Run Object
            //Files F = new Files();
            //F.file();

            //Inheritance Interface Run Object
            //Files1 F1 = new Files1();
            //F1.Download1();
            //F1.Download2();

            //Explicit INterfaces
            File2 F2 = new File2();
            ((f1)F2).f();
            ((f2)F2).f();

            Console.ReadLine();
        }
    }
}
