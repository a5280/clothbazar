﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Method_Hidding
{
    class parent
    {
        public void show()
        {
            Console.WriteLine("Parent Method Invoke");
        }
    }
    class child : parent
    {
        public new void show()
        {
           // base.show();
            Console.WriteLine("Child Method Invoke");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            //parent p = new child();
            //p.show();
            child ch = new child();
            ch.show();
            // ((parent)ch).show();
            Console.ReadLine();
        }
    }
}
