﻿using Core_app.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core_app.Controllers
{
    [Route("[controller]/[action]")]
    [Route("Home")]
    public class HomeController : Controller
    {
        //aik or concept hai tokens ka contollers or action method k name kii jagha per ape token kii pre define class lga do jasy k ctroller k liye [Route("[controller]")] or action k liye [Route("[action]")]
        // route Home controller kii class kobe da skty hai ais sa har aik index ma ni likhna pary ga wo oski main directory ko lay la ga
        //example controller and index name change no effect routing path
        //[Route("")]
        //[Route("Home")]
        //[Route("Home/Index")]
        //public IActionResult Data()
        //{
        //    return View("~/Views/Home/Index.cshtml");
        //}

        [Route("")]
        // [Route("Home")]
        // [Route("Home/Index")]
        //[Route("Index")]
        //  [Route("[action]")]
        //jab path khali hoga to y wala route chly ga wo by default route da da ga
       // [Route("~/")]
       // [Route("~/Home")]
        public IActionResult Index()
        {
            ViewData["var1"] = "Message come from view data";
            ViewData["var2"] = DateTime.Now.Date;

            string[] emp = {"1","Ali","Male" };

            ViewData["Ary"] =emp;

            ViewData["lt"] = new List<string>()
            {
                "Cricket",
                "Football",
                "Hockey"
            };

            //Student Stu = new Student()
            //{
            //    id = 1,
            //    name = "Mehran",
            //    Gender = "Gender"
            //};

           // ViewData["var5"] = Stu;



            ViewBag.Var11 = "Message come from view data";
            ViewBag.Var22 = DateTime.Now.AddDays(2);

            string[] emp1 = { "1", "Ali", "Male" };

            ViewBag.Ary1 = emp1;


            //Student std1 = new Student();
            //std1.id = 1;
            //std1.name = "Talha";
            //std1.Gender = "Male";

            Student std2 = new Student();

            //std2.id = 2;
            //std2.name = "Adnan";
            //std2.Gender = "Male";

            Student std3 = new Student();

            //std3.id = 1;
            //std3.name = "Huzaifa";
            //std3.Gender = "Male";


            Student std4 = new Student();

            //std4.id = 1;
            //std4.name = "Usama";
            //std4.Gender = "Male";

            List<Student> stt = new List<Student>();

           // stt.Add(std1);
            //stt.Add(std2);
            //stt.Add(std3);
            stt.Add(std4);

            TempData["tempData"] = "Message TempData";
            TempData.Keep("tempData");

            return View(stt);
        }

        // [Route("Home/Details/{id?}")]
        // [Route("Details/{id?}")]
        //  [Route("[action]/{id?}")]
        // [Route("{id?}")]

        public int Details(int? id)
        {
            return id ?? 1;
        }


        public ActionResult About()
        {
            return View();
        }

        public ActionResult Stud()
        {
            return View();
        }
    }
}
