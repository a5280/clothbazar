﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core_app.Models
{
    public class Student
    {

        public int id { get; set; }
        public string name { get; set; }
        public Gender Gender { get; set; }
        public string Married { get; set; }
    }

    public enum Gender{
        Male,Female
    }
}
