﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Reflection;

namespace LINQ_FIRST_APP
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("< ALL TO ALL SELECT QUERY IN LINQ>");

            List<employee> employees = new List<employee>()
            {
                new employee() {id=1,name="mehran",skills= new List<string>() {"C#","PHP","JAVA" } },
                new employee() {id=2,name="talha",skills=new List<string>() {"LINQ","ASP","MVC"  } },
                new employee() {id=3,name="adnan",skills=new List<string>() {"LINQ","VB","VUE.JS"  } }
            };

            //IEnumerable<employee> query = from emp in employees where emp.id == 2 select emp;

            //foreach (var item in query)
            //{
            //    Console.WriteLine(item.name);
            //}

            //IQueryable<employee> query1 = employees.AsQueryable().Where(x => x.id == 2);

            //foreach (var item in query1)
            //{
            //    Console.WriteLine(item.name);
            //}

            //var query2 = (from emp1 in employees select emp1).ToList();
            //foreach (var item in query2)
            //{
            //    Console.WriteLine(item.name);
            //}

            //Console.WriteLine("< -------------- simple select base>");

            //var query3 = employees.ToList();
            //foreach (var item in query3)
            //{
            //    Console.WriteLine(item.name);
            //}

            //Console.WriteLine("< -------------- Index Base>");

            //var query4 = employees.Select(emp => emp.name).ToList();
            //var query7 = employees.Select((emp, index) => new
            //{
            //   Index=index,Fullname=emp.name
            //}).ToList();

            //Console.WriteLine("< -------------- List Base>");

            //var query5 =( from emp in employees
            //             select  new employee()
            //             {
            //                 id=emp.id,
            //                 name=emp.name
            //             }).ToList();

            //foreach (var item in query5)
            //{
            //    Console.WriteLine($"Emp ID= {item.id},  Emp Name= { item.name}");
            //}

            //Console.WriteLine("< -------------- List Base>");

            //var query6 = employees.Select(x => new employee()
            //{
            //    id = x.id,
            //    name = x.name
            //}).ToList();

            //foreach (var item in query6)
            //{
            //    Console.WriteLine($"Emp ID= {item.id},  Emp Name= { item.name}");
            //}

            //Console.WriteLine("< --------------Slect Many query>");

            //var query8 = (from emp in employees
            //              from skill in emp.skills
            //              select skill).ToList();

            //foreach (var item in query8)
            //{
            //    Console.WriteLine("Programming -" +item);
            //}

            //Console.WriteLine("<-------- Select Type waise >");
            //var datasource = new List<object>() {"Talha ","Moazzam","Usama",1,2,3 };

            //var methodsyntax = datasource.OfType<int>().ToList();

            //var methodsysuntax1 = (from i in datasource
            //                       where i is string
            //                       select i).ToList();

      


            //int[] age = { 12, 18, 19, 22, 23, 25, 29 };
            //var a = from i in age where i > 20 orderby i descending select i;
            //foreach (var item in a)
            //{
            //    Console.WriteLine(item);
            //}



            //string[] names = {"Adil","Ali","Mehran","Adnan","Zesshan","Numan" };

            //var na = from name in names where name.StartsWith("M") select name;
            //foreach (var item in na)
            //{
            //    Console.WriteLine(item);

            //}

            Console.ReadLine();
        }

        class employee{
            public int id
            {
                get; set;
            }
            public string name
            {
                get; set;
            }
            public List<string> skills
            {
                get; set;
            }
        }
    }
}
