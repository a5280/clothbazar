﻿using Multiple_Models_in_Single_View_in_MVC.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Multiple_Models_in_Single_View_in_MVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var students = GetStudent();
            var teacher  =  GetTeacher();

            // VIEW MODEL SOLUTION
            //VMIndex Model = new VMIndex();
            //Model.Student = students;
            //Model.Teacher = teacher;

            //DYNAMIC MODEL SOLUTION BEST PROFESSIONAL
            //dynamic model = new ExpandoObject();
            //model.Student = students;
            //model.Teacher = teacher;


            //TUPLES MODEL SOLUTION
            // var model = new Tuple<List<Student>,List<Teacher>  >(students,teacher);

            //VIEWBAG MODEL SOLUTION
            ViewBag.Student = students;
            ViewBag.Teacher = teacher;
            return View();
        }

        public List<Student> GetStudent()
        {
            return new List<Student>
            {
                new Student() { StudentId = 1, Code = "L0001", Name = "Amit Gupta" },
                new Student() { StudentId = 2, Code = "L0002", Name = "Chetan Gujjar"},
                new Student() { StudentId = 3, Code = "L0003", Name = "Bhavin Patel" }
            };
        }

        public List<Teacher> GetTeacher()
        {
            return new List<Teacher>
            {
                new Teacher() { TeacherId = 1, Code = "L0001", Name = "Amit " },
                new Teacher() { TeacherId = 2, Code = "L0002", Name = "Chetan"  },
                new Teacher { TeacherId = 3, Code = "L0003", Name = "Bhavin " }
            };
        }
    }
}