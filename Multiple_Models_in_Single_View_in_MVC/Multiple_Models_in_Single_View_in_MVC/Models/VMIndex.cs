﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Multiple_Models_in_Single_View_in_MVC.Models
{
    public class VMIndex
    {
        public List<Student> Student
        {
            get; set;
        }
        public List<Teacher> Teacher
        {
            get; set;
        }
    }
}