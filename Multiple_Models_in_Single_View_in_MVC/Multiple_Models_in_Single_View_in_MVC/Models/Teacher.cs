﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Multiple_Models_in_Single_View_in_MVC.Models
{
    public class Teacher
    {
        public int TeacherId
        {
            get; set;
        }
        public string Code
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
    }
}