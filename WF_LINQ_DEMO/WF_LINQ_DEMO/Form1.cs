﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_LINQ_DEMO
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        StudentDataContext db;
        List<Student> stu;
        int index_no=0;
        private void Form1_Load(object sender, EventArgs e)
        {
            StudentDataContext db = new StudentDataContext();
            //// dataGridView1.DataSource = db.Students;
            Table<Student> stu_no = db.Students;
            dataGridView1.DataSource = stu_no;

            db = new StudentDataContext();
            stu = db.Students.ToList();

            Display();
        }
        public void Display()
        {
            textBox1.Text = stu[index_no].id.ToString();
            textBox2.Text = stu[index_no].name;
            textBox3.Text = stu[index_no].gender;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (index_no <stu.Count() -1)
            {
                index_no += 1;
                Display();
            }
            else
            {
                MessageBox.Show("Last Record");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (index_no >0)
            {
                index_no -= 1;
                Display();  
            }
            else
            {
                MessageBox.Show("First Record");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            db = new StudentDataContext();
            Student studen = new Student();
            studen.id =Convert.ToInt32(textBox1.Text);
            studen.name = textBox2.Text;
            studen.gender = textBox3.Text;

            db.Students.InsertOnSubmit(studen);
            db.SubmitChanges();

            dataGridView1.DataSource = db.Students;

            button5.PerformClick();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            foreach (Control item in this.Controls)
            {
                if (item is TextBox)
                {
                    TextBox txt = item as TextBox;
                    txt.Clear();
                }
            }
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            textBox1.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count >0)
            {
                int Id = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value);
                db = new StudentDataContext();
                Student dtu = db.Students.FirstOrDefault(s => s.id == Id);
                dtu.id = Convert.ToInt32(textBox1.Text);
                dtu.name = textBox2.Text;
                dtu.gender = textBox3.Text;
                db.SubmitChanges();
                dataGridView1.DataSource = db.Students;
                button5.PerformClick();

            }

        }

        private void button6_Click(object sender, EventArgs e)
        {
            int ID = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value);
            db = new StudentDataContext();
          //  Student stu = db.Students.FirstOrDefault(s => s.id == ID);
            Student stu = db.Students.Where(s => s.id == ID).FirstOrDefault();
            db.Students.DeleteOnSubmit(stu);
            db.SubmitChanges();

            dataGridView1.DataSource = db.Students;
            button5.PerformClick();
        }
    }
}
