﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_LINQ_DEMO
{
    public partial class CRUD_WITH_STORE_PROCEDURE : Form
    {
        public CRUD_WITH_STORE_PROCEDURE()
        {
            InitializeComponent();
        }
        StudentDataContext db = new StudentDataContext();
        private void CRUD_WITH_STORE_PROCEDURE_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = db.spShowStudents();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            db.spInsert(int.Parse(textBox1.Text),textBox2.Text,textBox3.Text);

            foreach (Control item in this.Controls)
            {
                if (item is TextBox)
                {
                    TextBox txt = item as TextBox;
                    txt.Clear();
                }
            }
            dataGridView1.DataSource = db.spShowStudents();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {

            db.spupdate(int.Parse(textBox1.Text), textBox2.Text, textBox3.Text);

            foreach (Control item in this.Controls)
            {
                if (item is TextBox)
                {
                    TextBox txt = item as TextBox;
                    txt.Clear();
                }
            }
            dataGridView1.DataSource = db.spShowStudents();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            textBox1.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            db.spdelete(int.Parse(textBox1.Text));

            foreach (Control item in this.Controls)
            {
                if (item is TextBox)
                {
                    TextBox txt = item as TextBox;
                    txt.Clear();
                }
            }
            dataGridView1.DataSource = db.spShowStudents();
        }
    }
}
