﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_LINQ_DEMO
{
    public partial class Store_Procedure_with_linq : Form
    {
        public Store_Procedure_with_linq()
        {
            InitializeComponent();
        }
        StudentDataContext db = new StudentDataContext();
        private void Store_Procedure_with_linq_Load(object sender, EventArgs e)
        {
           
            ISingleResult<spShowStudentsResult> showresult = db.spShowStudents();
            dataGridView1.DataSource = showresult;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text) ==true)
            {
                MessageBox.Show("Please Fill The Field");
            }
            else
            {

                ISingleResult<spSearchsResult> showsearch = db.spSearchs(int.Parse(textBox1.Text));
                dataGridView1.DataSource = showsearch;
                if (dataGridView1.Rows.Count ==1)
                {
                    MessageBox.Show("No Rows Found");
                }
            }
        }
    }
}
