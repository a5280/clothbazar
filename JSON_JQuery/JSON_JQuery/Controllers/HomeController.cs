﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using JSON_JQuery.Models;

namespace JSON_JQuery.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetJSON()
        {
            Student st = new Student()
            {
                id=1, 
                name="Mehran",
                gender="Male"
            };
            var json = JsonConvert.SerializeObject(st);
            return Json(json,JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddStd(Student std)
        {
            return Json("true", JsonRequestBehavior.AllowGet);
        }
    }
}