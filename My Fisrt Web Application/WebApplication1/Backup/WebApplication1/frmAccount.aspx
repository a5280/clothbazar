﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmAccount.aspx.cs" Inherits="WebApplication1.frmAccount" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add & Update Account</title>
    <link href="CSS/Styleaccount.css" rel="stylesheet" />
</head>
<body>
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>--%>--%>
    <div class="addaccountbox">
    <img src="Images/addaccount.png" alt="Alternate Text" class="Account" />
    <h2>Add & Update Account</h2>
    <form id="Form1" runat="server">
            <asp:Label ID="Label1" Text="Id" CssClass="lablid" runat="server"/>
            <asp:TextBox ID="TextBox1" runat="server" CssClass="txt_id" placeholder= "Account Id"/>

            <asp:Label ID="Label2" text="Name" CssClass="lablname" runat="server"/>
            <asp:TextBox ID="TextBox2"  runat="server" CssClass="txt_Name" placeholder="Account Name"/>

            <asp:Label ID="Label3" Text="Contact No" CssClass="lablcontact" runat="server"/>
            <asp:TextBox ID="TextBox3" runat="server" CssClass="txt_contact" placeholder= "Account Contact"/>
            <%--<ajaxToolkit:MaskedEditExtender ID:"TextBox3_MaskedEditExtender" Mask="9999-9999999" MaskType="Number" runat="server" />--%>

            <asp:Label ID="Label4" text="Email" CssClass="lablemail" runat="server"/>
            <asp:TextBox ID="TextBox4"  runat="server" CssClass="txt_email" placeholder="Email"/>

            <asp:Label ID="Label5" text="Type" CssClass="labltype" runat="server"/>
            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="cmb_type" placeholder="Account Type"/>
            <center>
            <asp:Button ID="Button1" Text="Reset" CssClass="btnreset" runat="server" 
                    onclick="Button1_Click"/>
            <asp:Button ID="Button2" Text="Save" CssClass="btnsubmit" runat="server"/>
            <asp:Button ID="Button3" Text="Update" CssClass="btnupdate" runat="server"/>
            </center>
            <%--<asp:LinkButton ID="LinkButton1" Text="Forget Password" CssClass="btnforget" runat="server" />--%>
            <%--<asp:LinkButton Text="Forget Password" CssClass="btnforget" runat="server"/>--%>
        </form>
    </div>
</body>
</html>
