﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmLogin.aspx.cs" Inherits="WebApplication1.frmLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Log In</title>
    <link href="CSS/StyleLogin.css" rel="stylesheet" />
</head>
<body>
    <div class = "loginbox">
        <img src="Images/images.png" alt="Alternate Text" class="user"/>
        <h2> Log in Here</h2>
        <form runat="server">
            <asp:Label Text="Email" CssClass="lablemail" runat="server"/>
            <asp:TextBox ID="TextBox1" runat="server" CssClass="txtemail" placeholder= "Enter Email"/>
            <asp:Label text="Password" CssClass="lablpass" runat="server"/>
            <asp:TextBox ID="TextBox2" TextMode="Password" runat="server" CssClass="txtpass" placeholder="*********"/>
            <asp:Button Text="Sign In" CssClass="btnsubmit" runat="server"/>
            <asp:LinkButton Text="Forget Password" CssClass="btnforget" runat="server" />
            <%--<asp:LinkButton Text="Forget Password" CssClass="btnforget" runat="server"/>--%>
            
        </form>
    </div>
</body>
</html>
