﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmProduct.aspx.cs" Inherits="WebApplication1.frmProduct" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add & Update Product</title>
    <link href="CSS/StyleProduct.css" rel="stylesheet" />
</head>
<body>
    <div class="addaccountbox">
    <img src="Images/adprd.png" alt="Alternate Text" class="Account" />
    <h2>Add & Update Product</h2>
    <form id="Form1" runat="server">
            <asp:Label ID="Label1" Text="Id" CssClass="lablid" runat="server"/>
            <asp:TextBox ID="txt_id" runat="server" CssClass="txt_id" placeholder= "Product Id"/>

            <asp:Label ID="Label2" text="Name" CssClass="lablname" runat="server"/>
            <asp:TextBox ID="txt_name"  runat="server" CssClass="txt_Name" placeholder="Product Name"/>
            
            <asp:Label ID="Label5" text="Type" CssClass="labltype" runat="server"/>
            <asp:DropDownList ID="cmb_type" runat="server" CssClass="cmb_type" placeholder="Type or Company Name"/>

            <asp:Label ID="Label6" text="Size" CssClass="labltype" runat="server"/>
            <asp:DropDownList ID="cmb_size" runat="server" CssClass="cmb_size" placeholder="Product Size"/>
            
            <asp:Label ID="Label7" text="Category" CssClass="labltype" runat="server"/>
            <asp:DropDownList ID="cmb_category" runat="server" CssClass="cmb_category" placeholder="Product Category"/>

            <asp:Label ID="Label3" Text="Metter In Box" CssClass="lablcontact" runat="server"/>
            <asp:TextBox ID="txt_tilem" runat="server" CssClass="txtmetterb" placeholder= "Metter In Box"/>
            <%--<ajaxToolkit:MaskedEditExtender ID:"TextBox3_MaskedEditExtender" Mask="9999-9999999" MaskType="Number" runat="server" />--%>

            <asp:Label ID="Label4" text="Tiles in Box" CssClass="lablemail" runat="server"/>
            <asp:TextBox ID="txt_tileinb"  runat="server" CssClass="tileinb" 
                placeholder="Tiles In Box" ontextchanged="txt_tileinb_TextChanged"/>

            <asp:Label ID="Label8" text="Tiles in Metter" CssClass="lablemail" runat="server"/>
            <asp:TextBox ID="txt_tileinm"  runat="server" CssClass="txttileim" placeholder="Tiles In Metter"/>
            
            <asp:Label ID="Label9" text="Purchase Rate" CssClass="lablemail" runat="server"/>
            <asp:TextBox ID="txt_purrate"  runat="server" CssClass="txtpur" placeholder="Purchase Rate"/>

            <asp:Label ID="Label10" text="Sale Rate" CssClass="lablemail" runat="server"/>
            <asp:TextBox ID="txt_srate"  runat="server" CssClass="txtsrate" placeholder="Sale Rate"/>

            <center>
            <asp:Button ID="Button1" Text="Reset" CssClass="btnreset" runat="server"/>
            <asp:Button ID="Button2" Text="Save" CssClass="btnsubmit" runat="server"/>
            <asp:Button ID="Button3" Text="Update" CssClass="btnupdate" runat="server"/>
            </center>
            <%--<asp:LinkButton ID="LinkButton1" Text="Forget Password" CssClass="btnforget" runat="server" />--%>
            <%--<asp:LinkButton Text="Forget Password" CssClass="btnforget" runat="server"/>--%>
        </form>
    </div>
</body>
</html>
