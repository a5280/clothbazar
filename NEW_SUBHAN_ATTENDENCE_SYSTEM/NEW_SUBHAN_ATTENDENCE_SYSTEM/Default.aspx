﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="NEW_SUBHAN_ATTENDENCE_SYSTEM._Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

  
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">  
        <div class="jumbotron">
            <asp:ScriptManager ID="ScriptManager10" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <h1>Get Attendance Reports</h1>
                    <p class="lead">Date :   </p>
                    <table class="nav-justified">
                        <tr>
                            <td>&nbsp;</td>
                            <td style="width: 45px">
                                <asp:Label ID="Label1" runat="server" Text="From :" Width="70px"></asp:Label>
                            </td>
                            <td style="width: 243px">
                                <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Width="165px"></asp:TextBox>
                            </td>
                            <td style="width: 32px">
                                <asp:Label ID="Label2" runat="server" Width="50px">To :</asp:Label>
                            </td>

                            <td>
                                <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Width="165px"></asp:TextBox>
                                <asp:ImageButton ID="ImageButton1" runat="server" Font-Bold="True" Height="25px" Width="25px" ImageAlign="TextTop" ImageUrl="~/image/calendar_icon.png" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="yyyy-MM-dd" PopupButtonID="ImageButton1 " PopupPosition="BottomRight" TargetControlID="TextBox2"></ajaxToolkit:CalendarExtender>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                    <p><a href="ReportViewer.aspx" class="btn btn-primary btn-lg">Show &raquo;</a></p>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div class="row">
            <div class="col-md-4">
                <h2>Getting started</h2>
                <p>
                    ASP.NET Web Forms lets you build dynamic websites using a familiar drag-and-drop, event-driven model.
            A design surface and hundreds of controls and components let you rapidly build sophisticated, powerful UI-driven sites with data access.
                </p>
                <p>
                    <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301948">Learn more &raquo;</a>
                </p>
            </div>
            <div class="col-md-4">
                <h2>Get more libraries</h2>
                <p>
                    NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.
                </p>
                <p>
                    <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301949">Learn more &raquo;</a>
                </p>
            </div>
            <div class="col-md-4">
                <h2>Web Hosting</h2>
                <p>
                    You can easily find a web hosting company that offers the right mix of features and price for your applications.
                </p>
                <p>
                    <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301950">Learn more &raquo;</a>
                </p>
            </div>
        </div>

</asp:Content>
       