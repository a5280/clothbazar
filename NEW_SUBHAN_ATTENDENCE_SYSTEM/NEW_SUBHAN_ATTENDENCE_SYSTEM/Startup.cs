﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NEW_SUBHAN_ATTENDENCE_SYSTEM.Startup))]
namespace NEW_SUBHAN_ATTENDENCE_SYSTEM
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
