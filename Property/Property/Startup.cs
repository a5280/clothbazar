﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Property.Startup))]
namespace Property
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
