﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
          //  TempData["Var3"] = "first application";
            string[] games = { "cricket", "football", "hockey", "baseball" };
            TempData["gamesarray"] = games;

            //Emp Emply = new Emp()
            //{
            //    id=1,
            //    name="mehran",
            //    age=22
            //};
            //   TempData["meh"] = Emply;

            return View();
        }

        public ActionResult AboutUS()
        {
            return View();
        }
    }
}