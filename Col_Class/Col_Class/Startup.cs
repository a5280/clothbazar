﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Col_Class.Startup))]
namespace Col_Class
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
