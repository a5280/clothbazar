﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pass_Data_View_To_Controller.Models;

namespace Pass_Data_View_To_Controller.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string Setparameter(string firstname, string lastname)
        {
            return "First Name : " + firstname + " Second Name :" + lastname;
        }

        [HttpPost]
        public string SetRequest()
        {
            string first = Request["firstname"];
            string second = Request["lastname"];

            return "First Name : " + first + " Second Name :" + second;
        }

        [HttpPost]
        public string SetFormCollection(FormCollection from)
        {
            string first = from["firstname"];
            string second = from["lastname"];

            return "First Name : " + first + " Second Name :" + second;
        }

        [HttpPost]
        public string SetModel(Employee emp)
        {
            return "First Name : " + emp.first + " Second Name :" + emp.second;
        }
    }
}