﻿using STRONGLY_TYPED_HTML_HELPERS_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace STRONGLY_TYPED_HTML_HELPERS_MVC.Controllers
{
    public class LoginController : Controller
    {
        LoginDBEntities db = new LoginDBEntities();
        // GET: Login
        public ActionResult Index()
        {
            HttpCookie cookie =Request.Cookies["User"];
            if (cookie !=null)
            {

                ViewBag.username = cookie["username"].ToString();

                string Encryptpass = cookie["password"].ToString();
                byte[] b = Convert.FromBase64String(Encryptpass);
                string decryptpass = ASCIIEncoding.ASCII.GetString(b);

                ViewBag.password = decryptpass.ToString();

            }
            return View();
        }
        [HttpPost]
        public ActionResult Index(User u)
        {
            HttpCookie cookie = new HttpCookie("User");
            if (ModelState.IsValid == true)
            {
                if (u.RememberMe == true)
                {

                    cookie["username"] = u.Username;

                    byte[] b = ASCIIEncoding.ASCII.GetBytes(u.password);
                    string Encryptpass = Convert.ToBase64String(b);
                    cookie["password"] = Encryptpass;

                    cookie.Expires = DateTime.Now.AddDays(5);
                    HttpContext.Response.Cookies.Add(cookie);
                }
                else
                {
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    HttpContext.Response.Cookies.Add(cookie);
                }
                var row = db.Users.Where(model => model.Username == u.Username && model.password == u.password).FirstOrDefault();
                if (row != null)
                {
                    Session["Username"] = u.Username;
                    TempData["Message"] = "<script>alert('Login Successful')</script>";
                    return RedirectToAction("Index", "Dashboard");
                }
                else
                {
                    TempData["Message"] = "<script>alert('Login Failed')</script>";

                }
            }
            return View();
        }
    }
}