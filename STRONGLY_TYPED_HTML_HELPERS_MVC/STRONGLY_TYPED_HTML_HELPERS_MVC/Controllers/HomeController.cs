﻿using STRONGLY_TYPED_HTML_HELPERS_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace STRONGLY_TYPED_HTML_HELPERS_MVC.Controllers
{
    public class HomeController : Controller
    {
        LoginDBEntities db = new LoginDBEntities();
        // GET: Home
        // string emailpattern = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(User u)
        {
            if (ModelState.IsValid==true)
            {
                db.Users.Add(u);
                int a = db.SaveChanges();
                if (a>0)
                {
                    TempData["Register"] = "<script>alert('Sign Up Successfully')</script>";
                    ModelState.Clear();
                    return RedirectToAction("Index","Login");
                }
                else
                {
                    TempData["Register"] = "<script>alert('Sign Up Not Successfully')</script>";
                }
            }
            else
            {
                TempData["Register"] = "<script>alert('ModelState False')</script>";
            }
            
            return View();
        }
       
        //public ActionResult Index(string Name,string Password, string Gender,string Email,string Comment)
        //{
        //    if (Name.Equals("") == true)
        //    {
        //        ModelState.AddModelError("Name","Name is required !!");
        //    }
        //    if (Password.Equals("") == true)
        //    {
        //        ModelState.AddModelError("Password", "Password is required !!");
        //    }
        //    if (Gender.Equals("")==true)
        //    {
        //        ModelState.AddModelError("Gender","Gender is required !!");
        //    }
        //    if (Email.Equals("")==true)
        //    {
        //        ModelState.AddModelError("Email","Email is required !!");
        //    }
        //    else
        //    {
        //        if (Regex.IsMatch(Email,emailpattern)==false)
        //        {
        //            ModelState.AddModelError("Email", "Invalid Email !!");
        //        }
        //    }
        //    if (Comment.Equals("")==true)
        //    {
        //        ModelState.AddModelError("Comment","Comment is required !");
        //    }


        //    if (ModelState.IsValid==true)
        //    {
        //        ViewData["SuccessMessage"] = "<script>alert('Data has been Submitted')</script> ";
        //        ModelState.Clear();
        //    }
        //    return View();
        //}
    }
}