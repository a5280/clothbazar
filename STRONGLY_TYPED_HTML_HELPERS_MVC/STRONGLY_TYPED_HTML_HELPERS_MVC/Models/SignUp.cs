﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace STRONGLY_TYPED_HTML_HELPERS_MVC.Models
{
    public class SignUp
    {
        [Range(20,25)]
        public int ID { get; set; }
        [DisplayName("Name")]
        [Required (ErrorMessage = "Name is required !!")]
        [StringLength (20,MinimumLength =5,ErrorMessage ="Name should be in between 5 and 20 ")]
        public string Name{ get; set; }


        [Required(ErrorMessage = "UpperCase, LowerCase, Numbers, Symbols, 8 Characters !!")]
        [RegularExpression(@"^.*(?=.{10,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$",ErrorMessage ="Invalid Password")]
        [DisplayName("Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }


        [Required(ErrorMessage = "UpperCase, LowerCase, Numbers, Symbols, 8 Characters !!")]
        [Compare("Password",ErrorMessage ="Password is not match")]
        [DisplayName("Confirm Password")]
        [DataType(DataType.Password)]
        public string ConPassword { get; set; }


        [Required(ErrorMessage = "Gender is required !!")]
        [DisplayName("Gender")]
        public string Gender { get; set; }


        [Required(ErrorMessage = "Email is required !!")]
        [RegularExpression(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z",ErrorMessage = "Invalid Email")]
        [DisplayName("Email")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Comment is required !!")]
        [DisplayName("Address")]
        [DataType(DataType.MultilineText)] 
        public string Comment { get; set; }


        [ReadOnly(true)]
        public string Organization { get; set; }



        [Required(ErrorMessage = "Date is required !!")]
        [DisplayName("Date")]
        [DataType(DataType.Date)]
        public string Date { get; set; }



        [Required(ErrorMessage = "Time is required !!")]
        [DisplayName("Time")]
        [DataType(DataType.Time)]
        public string Time { get; set; }

    }
}