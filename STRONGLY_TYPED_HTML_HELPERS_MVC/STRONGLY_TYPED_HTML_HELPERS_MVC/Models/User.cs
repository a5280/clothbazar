//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace STRONGLY_TYPED_HTML_HELPERS_MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public partial class User
    {
 
        public int Id { get; set; }
        [DisplayName("Username")]
        
        public string Username { get; set; }
        [DisplayName("Password")]
        public string password { get; set; }
        [DisplayName("Remember Me")]
        public bool RememberMe { get; set; }
        public string confirm_password { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Org { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<System.DateTime> time { get; set; }
    }
}
