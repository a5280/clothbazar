﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Crytal_report
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TextBox1.Text = Session["date1"].ToString();
            TextBox2.Text = Session["date2"].ToString();

            Button1_Click(Button1, null);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            rpt_attendance sr = new rpt_attendance();

            //Report_Date sh = new Report_Date();
            ConnectionInfo ciReportConnection = new ConnectionInfo();
            ciReportConnection.ServerName = "SQL5102.site4now.net";
            ciReportConnection.DatabaseName = "db_a504e8_zkt";
            ciReportConnection.UserID = "db_a504e8_zkt_admin";
            ciReportConnection.Password = "admin12345";
            //ciReportConnection.IntegratedSecurity = true;
            Tables tables = sr.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
            {
                TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                tableLogonInfo.ConnectionInfo = ciReportConnection;
                table.ApplyLogOnInfo(tableLogonInfo);
            }

            TextObject too = (TextObject)sr.ReportDefinition.Sections["Section2"].ReportObjects["Text7"];
            too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
            CrystalReportViewer1.ReportSource = sr;
        }
    }
}