﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Crytal_report.Startup))]
namespace Crytal_report
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
