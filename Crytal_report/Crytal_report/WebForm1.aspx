﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Crytal_report.WebForm1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style3 {
            width: 160px;
        }
        .auto-style4 {
            width: 216px;
        }
        .auto-style5 {
            width: 86px;
        }
        .auto-style6 {
            width: 47px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <table class="auto-style1">
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td class="auto-style5">
                                <asp:Label ID="Label1" runat="server" Text="From :"></asp:Label>
                            </td>
                            <td class="auto-style3">
                                 <asp:TextBox ID="TextBox3" runat="server" Width="100px"></asp:TextBox>
                                 <asp:ImageButton ID="Button2" runat="server" CssClass="auto-style4" Font-Bold="True" Height="25px" ImageAlign="TextTop"  Text="" Width="25px"  />
                                 <ajaxtoolkit:calendarextender id="CalendarExtender1" runat="server" format="yyyy-MM-dd" popupbuttonid="Button2" popupposition="BottomRight" targetcontrolid="TextBox3" />
                            </td>
                            <td class="auto-style6">
                                <asp:Label ID="Label2" runat="server" Text="To :"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox1" runat="server" Width="100px"></asp:TextBox>
                                <asp:ImageButton ID="ImageButton1" runat="server" CssClass="auto-style4" Font-Bold="True" Height="25px" ImageAlign="TextTop"  Text="" Width="25px" />
                                 <ajaxtoolkit:calendarextender id="CalendarExtender2" runat="server" format="yyyy-MM-dd" popupbuttonid="ImageButton1" popupposition="BottomRight" targetcontrolid="TextBox1"  />
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="auto-style5">&nbsp;</td>
                            <td class="auto-style3">
                                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
                            </td>
                            <td class="auto-style6">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                    </table>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    <div>    
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />    
    </div>
    </form>
</body>
</html>
