﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Database_First_Approach.Controllers
{
    public class HomeController : Controller
    {
        TeacherEntities db = new TeacherEntities();
        // GET: Home
        public ActionResult Index()
        {
            var data = db.Teachers.ToList();
            return View(data);
        }
    }
}