﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HAFIZ_BUILDERS.matrix_admin_bt5_master.html.ltr
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        int a = 0;
        string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;
        DataTable dtt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    con_combo_list();
            //}
            if (!Page.IsPostBack)
            {
                if (ViewState["record"]==null)
                {
                    dtt.Columns.Add("name");
                    dtt.Columns.Add("age");
                    dtt.Columns.Add("add");
                    ViewState["record"]= dtt;
                }
            }
        }


        public void con_combo_list()
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {

                SqlCommand cmd = new SqlCommand("select Control_Description from Level_Control_Code", CON);
                

                CON.Open();
                DropDownList1.DataSource = cmd.ExecuteReader();
                DropDownList1.DataTextField = "Control_Description";
                DropDownList1.DataBind();
                DropDownList1.Items.Insert(0, "--Select--");
                CON.Close();
            }
        }
        string l1_id, l2_id, l3_id, ppid;

        protected void Button1_Click(object sender, EventArgs e)
        {
            dtt = (DataTable)ViewState["record"];
            dtt.Rows.Add(txt_level1_id.Text);
            dtt.Rows.Add(txt_level2_id.Text);
            dtt.Rows.Add(txt_level3_id.Text);
            GridView1.DataSource = dtt;
            GridView1.DataBind();
        }

        string l1_code, l2_code, l3_code, ppid_code;
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {
                CON.Open();
                SqlCommand cmd = new SqlCommand("select LSC.Main_Code,LSC.Main_Description,LCC.Sub_Code, LCC.Sub_Description ,LCC.Control_Code ,LCC.Control_Description from Level_Control_Code as LCC inner join Level_Sub_Code AS LSC on LCC.Sub_Code =LSC.Sub_Code where LCC.Control_Description='"+DropDownList1.Text+"' ", CON);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    l1_id = item["Main_Code"].ToString();
                    l2_id = item["Sub_Code"].ToString();
                    l3_id = item["Control_Code"].ToString();

                    //txt_level1_id.Text = l1_id;
                    //txt_level2_id.Text = l1_id + "-" + l2_id;
                    //txt_level3_id.Text = l1_id + "-" + l2_id + "-" + l3_id;

                    int length = l1_id.Length;
                    if (length >= 2)
                    {
                        l1_code = l1_id;
                    }
                    else
                    {
                        l1_code = "0" + l1_id;
                    }
                    int length2 = l2_id.Length;

                    if (length2 >= 2)
                    {
                        l2_code = l2_id;
                    }
                    else
                    {
                        l2_code = "0" + l2_id;
                    }
                    int length3 = l3_id.Length;

                    if (length3 >= 2)
                    {
                        l3_code = l3_id;
                    }
                    else
                    {
                        l3_code = "0" + l3_id;
                    }

                    txt_level1_id.Text = l1_code;
                    txt_level2_id.Text = l1_code + "-" + l2_code;
                    txt_level3_id.Text = l1_code + "-" + l2_code + "-" + l3_code;

                    C3.Text = item["Main_Description"].ToString();
                    C4.Text = item["Sub_Description"].ToString();
                    Label1.Text = C3.Text +"-->" + C4.Text +"-->"+DropDownList1.Text ;

                    SqlCommand cmd10 = new SqlCommand("select isnull(max(Account_#),0)+1 as id from Party_AC where Description = '" + C3.Text + "' and Sub_Description = '" + C4.Text + "' and Con_Description = '" + DropDownList1.Text + "'", CON);
                    SqlDataAdapter da10 = new SqlDataAdapter(cmd10);
                    DataTable dt10 = new DataTable();
                    da10.Fill(dt10);
                    foreach (DataRow item02 in dt10.Rows)
                    {
                        ppid = item02["id"].ToString();
                        txt_ppid.Text = ppid;
                        int ppid_string = ppid.Length;
                        if (length >= 4)
                        {
                            ppid_code = ppid;
                        }
                        else if (ppid_string >= 3)
                        {
                            ppid_code = "0" + ppid;
                        }
                        else if (ppid_string >= 2)
                        {
                            ppid_code = "00" + ppid;
                        }
                        else if (ppid_string >= 1)
                        {
                            ppid_code = "000" + ppid;
                        }
                        txt_party_id.Text = txt_level3_id.Text + "-" + ppid_code;
                    }

                }
                //  Label1.Text = "11";
            }
            }
        }
}