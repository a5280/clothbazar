﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Services;
using System.Web.Script.Services;

namespace HAFIZ_BUILDERS.matrix_admin_bt5_master.html.ltr
{
    public partial class Chart_of_account : System.Web.UI.Page
    {      
        string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                code_check();
                loadcombo_list();
                sub_combo_list();
                con_combo_list();
                sub_con_combo_list();
            }
        }
        protected void ShowMessage(object sender, EventArgs e)
        {
            //string message = "alert('Hello! Mudassar.')";
            //ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
        }
        public void code_check()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select count(Main_Code) from Level_Main_Code", con);
                int check_null_db = Convert.ToInt32(cmd.ExecuteScalar());


                if (check_null_db != Convert.ToInt32(null))
                {
                    SqlCommand cmd1 = new SqlCommand("select max(Main_Code + 1) from Level_Main_Code", con);
                    int main_code = Convert.ToInt32(cmd1.ExecuteScalar());
                    TextBox17.Text = main_code.ToString();
                }
                else
                {
                    TextBox17.Text = "1";
                }


                SqlCommand cmd11 = new SqlCommand("select count(Sub_Code) from Level_Sub_Code", con);
                int check_null_db1 = Convert.ToInt32(cmd11.ExecuteScalar());

                if (check_null_db1 != Convert.ToInt32(null))
                {
                    SqlCommand cmd111 = new SqlCommand("select max(Sub_Code + 1) from Level_Sub_Code", con);
                    int main_code = Convert.ToInt32(cmd111.ExecuteScalar());
                    TextBox1.Text = main_code.ToString();

                }
                else
                {
                    TextBox1.Text = "1";
                }



                SqlCommand cmd2 = new SqlCommand("select count(Control_Code) from Level_Control_Code", con);
                int check_null_db2 = Convert.ToInt32(cmd2.ExecuteScalar());

                if (check_null_db2 != Convert.ToInt32(null))
                {
                    SqlCommand cmd22 = new SqlCommand("select max(Control_Code + 1) from Level_Control_Code", con);
                    int main_code = Convert.ToInt32(cmd22.ExecuteScalar());
                    TextBox2.Text = main_code.ToString();

                }
                else
                {
                    TextBox2.Text = "1";
                }


                SqlCommand cmd23 = new SqlCommand("select count(Sub_Control) from Level_Sub_Control_Code", con);
                int check_null_db23 = Convert.ToInt32(cmd23.ExecuteScalar());

                if (check_null_db23 != Convert.ToInt32(null))
                {
                    SqlCommand cmd233 = new SqlCommand("select max(Sub_Control + 1) from Level_Sub_Control_Code", con);
                    int main_code = Convert.ToInt32(cmd233.ExecuteScalar());
                    TextBox3.Text = main_code.ToString();

                }
                else
                {
                    TextBox3.Text = "1";
                }


                SqlCommand cmd3 = new SqlCommand("select count(Account_#) from Party_AC", con);
                int check_null_db3 = Convert.ToInt32(cmd3.ExecuteScalar());

                if (check_null_db3 != Convert.ToInt32(null))
                {
                    SqlCommand cmd33 = new SqlCommand("select max(Account_# + 1) from Party_AC", con);
                    int main_code = Convert.ToInt32(cmd33.ExecuteScalar());
                    TextBox16.Text = main_code.ToString();

                }
                else
                {
                    TextBox16.Text = "1";
                }

           

                con.Close();
            }
        }
        public void loadcombo_list()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                SqlCommand cmd = new SqlCommand("select Description from Level_Main_Code", con);
                con.Open();
                ComboBox1.DataSource = cmd.ExecuteReader();
                ComboBox1.DataTextField = "Description";
                ComboBox1.DataBind();
                con.Close();

                con.Open();
                ComboBox6.DataSource = cmd.ExecuteReader();
                ComboBox6.DataTextField = "Description";
                ComboBox6.DataBind();
                con.Close();


                SqlCommand cmd1 = new SqlCommand("select distinct Project_Name from Project  ", con);
                con.Open();
                ComboBox8.DataSource = cmd1.ExecuteReader();
                ComboBox8.DataTextField = "Project_Name";
                ComboBox8.DataBind();
                ComboBox8.Items.Insert(0, "--Select--");
                ComboBox8.SelectedIndex = 0;
                con.Close();
            }


        }
        public void sub_combo_list()
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {

                SqlCommand cmd = new SqlCommand("select Sub_Description from Level_Sub_Code", CON);
                CON.Open();
                ComboBox2.DataSource = cmd.ExecuteReader();
                ComboBox2.DataTextField = "Sub_Description";
                ComboBox2.DataBind();
                CON.Close();

                CON.Open();
                ComboBox4.DataSource = cmd.ExecuteReader();
                ComboBox4.DataTextField = "Sub_Description";
                ComboBox4.DataBind();
                CON.Close();
            }
        }

        public void con_combo_list()
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {

                SqlCommand cmd = new SqlCommand("select Control_Description from Level_Control_Code", CON);
                CON.Open();
                ComboBox3.DataSource = cmd.ExecuteReader();
                ComboBox3.DataTextField = "Control_Description";
                ComboBox3.DataBind();
                CON.Close();

                CON.Open();
                ComboBox7.DataSource = cmd.ExecuteReader();
                ComboBox7.DataTextField = "Control_Description";
                ComboBox7.DataBind();
                CON.Close();
            }
        }

        public void sub_con_combo_list()
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {

                SqlCommand cmd = new SqlCommand("select Sub_Control_Description from Level_Sub_Control_Code", CON);
                CON.Open();
                ComboBox5.DataSource = cmd.ExecuteReader();
                ComboBox5.DataTextField = "Sub_Control_Description";
                ComboBox5.DataBind();
                CON.Close();

             

                CON.Open();
                DropDownList1.DataSource = cmd.ExecuteReader();
                DropDownList1.DataTextField = "Sub_Control_Description";
                DropDownList1.DataBind();
                DropDownList1.Items.Insert(0, "--Select--");
                CON.Close();
            }
        }


        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBox11_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBox15_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                if (TextBox17.Text != string.Empty && ComboBox1.Text != string.Empty)
                {
                    SqlCommand cmd = new SqlCommand("insert into Level_Main_Code (Main_Code,Description) values ('" + TextBox17.Text + "', '" + ComboBox1.Text + "')", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    TextBox17.Text = string.Empty;
                    ComboBox1.SelectedIndex = -1;

                    code_check();
                    loadcombo_list();
                    sub_combo_list();
                    con_combo_list();
                    sub_con_combo_list();
                }
                else
                {
                    Response.Write("<script> alert('Please enter main code Error...') </script>");
                }
            }
        }

        protected void Button12_Click(object sender, EventArgs e)
        {
            TextBox17.Text = string.Empty;
            ComboBox1.SelectedIndex = -1;
        }

        protected void Button10_Click(object sender, EventArgs e)
        {

        }

        protected void Button13_Click(object sender, EventArgs e)
        {

        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            TextBox17.Text = string.Empty;
            ComboBox1.SelectedIndex = -1;

            code_check();
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            TextBox1.Text = string.Empty;
            ComboBox2.SelectedIndex = -1;
            ComboBox6.SelectedIndex = -1;
            code_check();
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            TextBox2.Text = string.Empty;
            ComboBox3.SelectedIndex = -1;
            ComboBox4.SelectedIndex = -1;
            code_check();
        }

        protected void Button10_Click1(object sender, EventArgs e)
        {
            TextBox17.Text = string.Empty;
            ComboBox1.SelectedIndex = -1;
            TextBox1.Text = string.Empty;
            ComboBox2.SelectedIndex = -1;
            ComboBox6.SelectedIndex = -1;
            TextBox2.Text = string.Empty;
            ComboBox3.SelectedIndex = -1;
            ComboBox4.SelectedIndex = -1;
            code_check();
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                if (TextBox1.Text != string.Empty && ComboBox2.Text != string.Empty && ComboBox6.Text != string.Empty)
                {
                    con.Open();
                    SqlCommand cmd11 = new SqlCommand("select Main_Code from Level_Main_Code where Description='" + ComboBox6.Text + "' ", con);
                    int MAIN_CODE = Convert.ToInt32(cmd11.ExecuteScalar());

                    SqlCommand cmd = new SqlCommand("insert into Level_Sub_Code (Main_Code,Main_Description,Sub_Code,Sub_Description) values ('" + MAIN_CODE + "','" + ComboBox6.Text + "','" + TextBox1.Text + "','" + ComboBox2.Text + "')", con);

                    cmd.ExecuteNonQuery();
                    con.Close();
                    TextBox1.Text = string.Empty;
                    ComboBox2.SelectedIndex = -1;
                    ComboBox6.SelectedIndex = -1;

                    code_check();
                    loadcombo_list();
                    sub_combo_list();
                    con_combo_list();
                    sub_con_combo_list();
                }
                else
                {
                    Response.Write("<script> alert('Please enter main code Error...') </script>");
                }
            }
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                if (TextBox2.Text != string.Empty && ComboBox3.Text != string.Empty && ComboBox4.Text != string.Empty)
                {
                    con.Open();
                    SqlCommand cmd11 = new SqlCommand("select Sub_Code from Level_Sub_Code where Sub_Description='" + ComboBox4.Text + "' ", con);
                    int MAIN_CODE = Convert.ToInt32(cmd11.ExecuteScalar());

                    SqlCommand cmd = new SqlCommand("insert into Level_Control_Code (Sub_Code,Sub_Description,Control_Code,Control_Description) values ('" + MAIN_CODE + "','" + ComboBox4.Text + "','" + TextBox2.Text + "','" + ComboBox3.Text + "')", con);

                    cmd.ExecuteNonQuery();
                    con.Close();
                    TextBox2.Text = string.Empty;
                    ComboBox3.SelectedIndex = -1;
                    ComboBox4.SelectedIndex = -1;

                    code_check();
                    loadcombo_list();
                    sub_combo_list();
                    con_combo_list();
                    sub_con_combo_list();
                }
                else
                {
                    Response.Write("<script> alert('Please enter main code Error...') </script>");
                }
            }
        }
        SqlCommand cmd;
        protected void Button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {
                if (TextBox16.Text != string.Empty && DropDownList1.Text != "--Select--" && TextBox10.Text != string.Empty)
                {

                    CON.Open();

                    string QRY = "INSERT INTO Party_AC (Account_#, Account_Name, Email, Address,Contact_#,NTN_#,Account_Type,Main_Code,Description,Sub_Code,Sub_Description,Control_Code,Con_Description,Sub_Control,Sub_Control_Description,PARTY_ID,PPID,Date,Opening_Blc,Project_Name,P_AC_Amt) VALUES (@AC,@Name,@Email,@Add,@Con,@Ntn,@Ac_ty,@MC,@MD,@SC,@SD,@CC,@CD,@SCC,@SCD,@PID,@PPID,@Date,@Blc,@Proj_Name,@P_AC_At)";

                    cmd = new SqlCommand(QRY, CON);
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@AC", TextBox16.Text);
                    cmd.Parameters.AddWithValue("@Name", TextBox10.Text);
                    cmd.Parameters.AddWithValue("@Email", TextBox12.Text);
                    cmd.Parameters.AddWithValue("@Add", TextBox13.Text);
                    cmd.Parameters.AddWithValue("@Con", TextBox14.Text);
                    cmd.Parameters.AddWithValue("@Ntn", TextBox15.Text);
                    cmd.Parameters.AddWithValue("@Ac_ty", DropDownList1.Text);
                    cmd.Parameters.AddWithValue("@MC", txt_level1_id.Text);
                    cmd.Parameters.AddWithValue("@MD", C3.Text);
                    cmd.Parameters.AddWithValue("@SC", txt_level2_id.Text);
                    cmd.Parameters.AddWithValue("@SD", C4.Text);
                    cmd.Parameters.AddWithValue("@CC", txt_level3_id.Text);
                    cmd.Parameters.AddWithValue("@CD", C5.Text);
                    cmd.Parameters.AddWithValue("@SCC", txt_level4_id.Text);
                    cmd.Parameters.AddWithValue("@SCD", DropDownList1.Text);
                    cmd.Parameters.AddWithValue("@PID", txt_party_id.Text);
                    cmd.Parameters.AddWithValue("@PPID", txt_ppid.Text);
                    cmd.Parameters.AddWithValue("@Date", TextBox19.Text);
                    cmd.Parameters.AddWithValue("@Blc", TextBox18.Text);
                    cmd.Parameters.AddWithValue("@Proj_Name", ComboBox8.Text);
                    if (RadioButton1.Checked == true)
                    {
                        cmd.Parameters.AddWithValue("@P_AC_At", RadioButton1.Text);
                    }
                    if (RadioButton2.Checked == true)
                    {
                        cmd.Parameters.AddWithValue("@P_AC_At", RadioButton2.Text);
                    }

                    cmd.ExecuteNonQuery();
                    CON.Close();

                    if (TextBox18.Text != "0")
                    {                        
                        ////////////////////////////////////////////////////////// CASH CRADIT

                        CON.Open();
                        string QRY1 = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration1,Invoice,Debit,Credit,Party_Name,Project_Name) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name,@Proj_Name)";
                        SqlCommand cmd2 = new SqlCommand(QRY1, CON);
                        cmd2.CommandType = CommandType.Text;
                        cmd2.Parameters.AddWithValue("@Voc#", TextBox16.Text);
                        cmd2.Parameters.AddWithValue("@P_AC", TextBox16.Text);
                        cmd2.Parameters.AddWithValue("@DATE", TextBox19.Text);
                        cmd2.Parameters.AddWithValue("@V_TYPE", "Op_Blc");
                        cmd2.Parameters.AddWithValue("@Nara1", "Opening Balance");
                        cmd2.Parameters.AddWithValue("@INV", "0");
                        if (RadioButton1.Checked == true)
                        {
                            cmd2.Parameters.AddWithValue("@DEBIT", TextBox18.Text);
                            cmd2.Parameters.AddWithValue("@CREDIT", 0);
                        }
                        else if (RadioButton2.Checked == true)
                        {
                            cmd2.Parameters.AddWithValue("@DEBIT", 0);
                            cmd2.Parameters.AddWithValue("@CREDIT", TextBox18.Text);
                        }
                        cmd2.Parameters.AddWithValue("@P_Name", TextBox10.Text);
                        cmd2.Parameters.AddWithValue("@Proj_Name", ComboBox8.Text);
                        cmd2.ExecuteNonQuery();

                        ////////////////////////////////////////////////////////// CASH DEBIT

                        SqlCommand cmd_id = new SqlCommand("select account_# from party_ac where account_name= 'OPENING BALANCE'", CON);
                        SqlDataAdapter da = new SqlDataAdapter(cmd_id);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        foreach (DataRow item in dt.Rows)
                        {
                            string QRY3 = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration1,Invoice,Debit,Credit,Party_Name,Project_Name) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name,@Proj_Name)";
                            SqlCommand cmd1 = new SqlCommand(QRY3, CON);
                            cmd1.CommandType = CommandType.Text;
                            cmd1.Parameters.AddWithValue("@Voc#", TextBox16.Text);
                            cmd1.Parameters.AddWithValue("@P_AC", item["account_#"]);
                            cmd1.Parameters.AddWithValue("@DATE", TextBox19.Text);
                            cmd1.Parameters.AddWithValue("@V_TYPE", "Op_Blc");
                            cmd1.Parameters.AddWithValue("@Nara1", "Opening Balance");
                            cmd1.Parameters.AddWithValue("@INV", "0");
                            if (RadioButton2.Checked == true)
                            {
                                cmd1.Parameters.AddWithValue("@DEBIT", TextBox18.Text);
                                cmd1.Parameters.AddWithValue("@CREDIT", 0);
                            }
                            else if (RadioButton1.Checked == true)
                            {
                                cmd1.Parameters.AddWithValue("@DEBIT", 0);
                                cmd1.Parameters.AddWithValue("@CREDIT", TextBox18.Text);
                            }
                            cmd1.Parameters.AddWithValue("@P_Name", TextBox10.Text);
                            cmd1.Parameters.AddWithValue("@Proj_Name", ComboBox8.Text);
                            cmd1.ExecuteNonQuery();

                            CON.Close();
                        }
                    }


                    DropDownList1.SelectedIndex = -1;
                    txt_level1_id.Text = string.Empty;
                    txt_level2_id.Text = string.Empty;
                    txt_level3_id.Text = string.Empty;
                    txt_level4_id.Text = string.Empty;
                    txt_party_id.Text = string.Empty;
                    txt_ppid.Text = string.Empty;
                    txt_party_id.Text = string.Empty;
                    C3.Text = string.Empty;
                    C4.Text = string.Empty;
                    C5.Text = string.Empty;
                    TextBox10.Text = string.Empty;
                    TextBox12.Text = string.Empty;
                    TextBox13.Text = string.Empty;
                    TextBox14.Text = string.Empty;
                    TextBox15.Text = string.Empty;
                    TextBox16.Text = string.Empty;
                    TextBox19.Text = string.Empty;
                    TextBox18.Text = string.Empty;
                    Label100.Text = string.Empty;
                    ComboBox8.SelectedIndex = 0;
                    RadioButton1.Checked = false;
                    RadioButton2.Checked = false;

                    code_check();

                }
                else
                {
                    Response.Write("<script> alert('Please Fill the Empty Fields') </script>");
                }
            }

     

        }
        string l1_id, l2_id, l3_id, l4_id, ppid;
        string l1_code, l2_code, l3_code, l4_code, ppid_code;

        protected void txt_level2_id_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBox16_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                if (TextBox3.Text != string.Empty && ComboBox5.Text != string.Empty && ComboBox7.Text != string.Empty)
                {
                 
                    con.Open();
                    SqlCommand cmd11 = new SqlCommand("select Control_Code from Level_Control_Code where Control_Description='" + ComboBox7.Text + "' ", con);
                    int MAIN_CODE = Convert.ToInt32(cmd11.ExecuteScalar());

                    SqlCommand cmd = new SqlCommand("insert into Level_Sub_Control_Code (Control_Code,Control_Description,Sub_Control,Sub_Control_Description) values ('" + MAIN_CODE + "','" + ComboBox7.Text + "','" + TextBox3.Text + "','" + ComboBox5.Text + "')", con);

                    cmd.ExecuteNonQuery();
                    con.Close();
                    TextBox3.Text = string.Empty;
                    ComboBox7.SelectedIndex = -1;
                    ComboBox5.SelectedIndex = -1;

                    code_check();
                    loadcombo_list();
                    sub_combo_list();
                    con_combo_list();
                    sub_con_combo_list();
                }
                else
                {
                    Response.Write("<script> alert('Please enter main code Error...') </script>");
                }
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {      
            search();
            TextBox10.Focus();       
        }

        protected void TextBox19_TextChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel2, this.GetType(), "", "alert('" + TextBox18.Text + "')", true);
            TextBox10.Focus();
        }

        protected void Button13_Click2(object sender, EventArgs e)
        {
           
        }

        protected void Button13_Click3(object sender, EventArgs e)
        {
            search();
            TextBox10.Focus();
        }

        protected void DropDownList1_TextChanged(object sender, EventArgs e)
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {
                CON.Open();
                SqlCommand cmd = new SqlCommand("SELECT LSC.Main_Code,LSC.Main_Description, LCC.Sub_Code, LCC.Sub_Description ,LSCC.Control_Code ,LSCC.Control_Description,LSCC.Sub_Control,LSCC.Sub_Control_Description from Level_Sub_Control_Code as LSCC inner join Level_Control_Code AS LCC on LSCC.Sub_Control = LCC.Control_Code or LSCC.Sub_Control != LCC.Control_Code  inner join Level_Sub_Code as LSC on LSC.Sub_Code =LCC.Sub_Code   where LSCC.Sub_Control_Description='" + DropDownList1.Text + "' ", CON);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    l1_id = item["Main_Code"].ToString();
                    l2_id = item["Sub_Code"].ToString();
                    l3_id = item["Control_Code"].ToString();
                    l4_id = item["Sub_Control"].ToString();

                    int length = l1_id.Length;
                    if (length >= 2)
                    {
                        l1_code = l1_id;
                    }
                    else
                    {
                        l1_code = "0" + l1_id;
                    }
                    int length2 = l2_id.Length;

                    if (length2 >= 2)
                    {
                        l2_code = l2_id;
                    }
                    else
                    {
                        l2_code = "0" + l2_id;
                    }
                    int length3 = l3_id.Length;

                    if (length3 >= 2)
                    {
                        l3_code = l3_id;
                    }
                    else
                    {
                        l3_code = "0" + l3_id;
                    }

                    int length4 = l4_id.Length;
                    if (length4 >= 2)
                    {
                        l4_code = l4_id;
                    }
                    else
                    {
                        l4_code = "0" + l4_id;
                    }

                    txt_level1_id.Text = l1_code;
                    txt_level2_id.Text = l1_code + "-" + l2_code;
                    txt_level3_id.Text = l1_code + "-" + l2_code + "-" + l3_code;
                    txt_level4_id.Text = l1_code + "-" + l2_code + "-" + l3_code + "-" + l4_code;

                    C3.Text = item["Main_Description"].ToString();
                    C4.Text = item["Sub_Description"].ToString();
                    C5.Text = item["Control_Description"].ToString();
                    Label100.Text = C3.Text + "-->" + C4.Text + "-->" + C5.Text + "-->" + DropDownList1.Text;

                    SqlCommand cmd10 = new SqlCommand("select isnull(max(Account_#),0)+1 as id from Party_AC where Description = '" + C3.Text + "' and Sub_Description = '" + C4.Text + "' and Con_Description='" + C5.Text + "' and  Sub_Control_Description= '" + DropDownList1.Text + "'", CON);
                    SqlDataAdapter da10 = new SqlDataAdapter(cmd10);
                    DataTable dt10 = new DataTable();
                    da10.Fill(dt10);
                    foreach (DataRow item02 in dt10.Rows)
                    {
                        ppid = item02["id"].ToString();
                        txt_ppid.Text = ppid;
                        int ppid_string = ppid.Length;
                        if (length >= 5)
                        {
                            ppid_code = ppid;
                        }
                        else if (ppid_string >= 4)
                        {
                            ppid_code = "0" + ppid;
                        }
                        else if (ppid_string >= 3)
                        {
                            ppid_code = "00" + ppid;
                        }
                        else if (ppid_string >= 2)
                        {
                            ppid_code = "000" + ppid;
                        }
                        else if (ppid_string >= 1)
                        {
                            ppid_code = "0000" + ppid;
                        }
                        txt_party_id.Text = txt_level3_id.Text + "-" + ppid_code;
                    }

                }

            }
        }

        protected void Button13_Click1(object sender, EventArgs e)
        {
         
        }

        protected void But_Click(object sender, EventArgs e)
        {
            TextBox3.Text = string.Empty;
            ComboBox5.SelectedIndex = -1;
            ComboBox7.SelectedIndex = -1;
            code_check();
        }

        public void search()
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {
                cmd = new SqlCommand("select * from Party_AC where Account_#='" + TextBox16.Text + "' ", CON);
                SqlDataReader dr;
                CON.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {

                    TextBox10.Text = dr.GetValue(1).ToString();
                    TextBox12.Text = dr.GetValue(2).ToString();
                    TextBox13.Text = dr.GetValue(3).ToString();
                    TextBox14.Text = dr.GetValue(4).ToString();
                    TextBox15.Text = dr.GetValue(5).ToString();
                    DropDownList1.Text = dr.GetValue(6).ToString();
                    txt_level1_id.Text = dr.GetValue(7).ToString();
                    C3.Text = dr.GetValue(8).ToString();
                    txt_level2_id.Text = dr.GetValue(9).ToString();
                    C4.Text = dr.GetValue(10).ToString();
                    txt_level3_id.Text = dr.GetValue(11).ToString();
                    C5.Text = dr.GetValue(12).ToString();
                    txt_level4_id.Text = dr.GetValue(13).ToString();
                    DropDownList1.Text = dr.GetValue(14).ToString();
                    txt_party_id.Text = dr.GetValue(15).ToString();
                    txt_ppid.Text = dr.GetValue(16).ToString();
                    TextBox19.Text = dr.GetValue(18).ToString();
                    TextBox18.Text = dr.GetValue(19).ToString();
                    ComboBox8.Text = dr.GetValue(20).ToString();
                    string data=  dr.GetValue(21).ToString();

                    if (data=="DR")
                    {
                        RadioButton1.Checked = true;
                    }
                    if (data == "CR")
                    {
                        RadioButton2.Checked = true;
                    }

                }
                CON.Close();
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {
                CON.Open();
                string QRY = "update Party_AC set Account_Name=@Name,Email=@Email,Address=@Add,Contact_#=@Con,NTN_#=@Ntn,Account_Type=@Ac_ty,Main_Code=@MC,Description=@MD,Sub_Code=@SC,Sub_Description=@SD,Control_Code=@CC,Con_Description=@CD,PARTY_ID=@PID,PPID=@PPID,Date=@Date,Opening_Blc=@Blc,Project_Name=@Pro_Name,P_AC_Amt=@P_AC_At where Account_#=@AC ";
                cmd = new SqlCommand(QRY, CON);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@AC", TextBox16.Text);
                cmd.Parameters.AddWithValue("@Name", TextBox10.Text);
                cmd.Parameters.AddWithValue("@Email", TextBox12.Text);
                cmd.Parameters.AddWithValue("@Add", TextBox13.Text);
                cmd.Parameters.AddWithValue("@Con", TextBox14.Text);
                cmd.Parameters.AddWithValue("@Ntn", TextBox15.Text);
                cmd.Parameters.AddWithValue("@Ac_ty", DropDownList1.Text);
                cmd.Parameters.AddWithValue("@MC", txt_level1_id.Text);
                cmd.Parameters.AddWithValue("@MD", C3.Text);
                cmd.Parameters.AddWithValue("@SC", txt_level2_id.Text);
                cmd.Parameters.AddWithValue("@SD", C4.Text);
                cmd.Parameters.AddWithValue("@CC", txt_level3_id.Text);
                cmd.Parameters.AddWithValue("@CD", DropDownList1.Text);
                cmd.Parameters.AddWithValue("@PID", txt_party_id.Text);
                cmd.Parameters.AddWithValue("@PPID", txt_ppid.Text);
                cmd.Parameters.AddWithValue("@Date", TextBox19.Text); 
                cmd.Parameters.AddWithValue("@Blc", TextBox18.Text); 
                cmd.Parameters.AddWithValue("@Pro_Name",ComboBox8.Text);
                if (RadioButton1.Checked == true)
                {
                    cmd.Parameters.AddWithValue("@P_AC_At", RadioButton1.Text);
                }
                if (RadioButton2.Checked == true)
                {
                    cmd.Parameters.AddWithValue("@P_AC_At", RadioButton2.Text);
                }
                cmd.ExecuteNonQuery();
                CON.Close();
                if (TextBox18.Text != "0")
                {
                    ////////////////////////////////////////////////////////// CASH CRADIT

                    CON.Open();
                    string QRY1 = "update Party_Ledger set Voucher_#=@Voc#,P_Account_#=@P_AC,DATE=@DATE,Vou_TYPE=@V_TYPE,Naration1=@Nara1,Invoice=@INV,Debit=@DEBIT,Credit=@CREDIT,Party_Name=@P_Name,Project_Name=@Proj_Name  where Voucher_# =@Voc# and Vou_TYPE='Op_Blc' and P_Account_#=@P_AC  ";
                    SqlCommand cmd2 = new SqlCommand(QRY1, CON);
                    cmd2.CommandType = CommandType.Text;
                    cmd2.Parameters.AddWithValue("@Voc#", TextBox16.Text);
                    cmd2.Parameters.AddWithValue("@P_AC", TextBox16.Text);
                    cmd2.Parameters.AddWithValue("@DATE", TextBox19.Text);
                    cmd2.Parameters.AddWithValue("@V_TYPE", "Op_Blc");
                    cmd2.Parameters.AddWithValue("@Nara1", "Opening Balance");
                    cmd2.Parameters.AddWithValue("@INV", "0");
                    if (RadioButton1.Checked == true)
                    {
                        cmd2.Parameters.AddWithValue("@DEBIT", TextBox18.Text);
                        cmd2.Parameters.AddWithValue("@CREDIT", 0);
                    }
                    else if (RadioButton2.Checked == true)
                    {
                        cmd2.Parameters.AddWithValue("@DEBIT", 0);
                        cmd2.Parameters.AddWithValue("@CREDIT", TextBox18.Text);
                    }
                    cmd2.Parameters.AddWithValue("@P_Name", TextBox10.Text);
                    cmd2.Parameters.AddWithValue("@Proj_Name", ComboBox8.Text);
                    cmd2.ExecuteNonQuery();

                    ////////////////////////////////////////////////////////// CASH DEBIT
                    SqlCommand cmd_id = new SqlCommand("select account_# from party_ac where account_name= 'OPENING BALANCE'", CON);
                    SqlDataAdapter da = new SqlDataAdapter(cmd_id);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    foreach (DataRow item in dt.Rows)
                    {
                        string QRY3 = "update Party_Ledger set Voucher_#=@Voc#,P_Account_#=@P_AC,DATE=@DATE,Vou_TYPE=@V_TYPE,Naration1=@Nara1,Invoice=@INV,Debit=@DEBIT,Credit=@CREDIT,Party_Name=@P_Name,Project_Name=@Proj_Name  where Voucher_# =@Voc# and Vou_TYPE='Op_Blc' and P_Account_#=@P_AC  ";
                        SqlCommand cmd1 = new SqlCommand(QRY3, CON);
                        cmd1.CommandType = CommandType.Text;
                        cmd1.Parameters.AddWithValue("@Voc#", TextBox16.Text);
                        cmd1.Parameters.AddWithValue("@P_AC", item["account_#"]);
                        cmd1.Parameters.AddWithValue("@DATE", TextBox19.Text);
                        cmd1.Parameters.AddWithValue("@V_TYPE", "Op_Blc");
                        cmd1.Parameters.AddWithValue("@Nara1", "Opening Balance11");
                        cmd1.Parameters.AddWithValue("@INV", "0");
                        if (RadioButton2.Checked == true)
                        {
                            cmd1.Parameters.AddWithValue("@DEBIT", TextBox18.Text);
                            cmd1.Parameters.AddWithValue("@CREDIT", 0);
                        }
                        else if (RadioButton1.Checked == true)
                        {
                            cmd1.Parameters.AddWithValue("@DEBIT", 0);
                            cmd1.Parameters.AddWithValue("@CREDIT", TextBox18.Text);
                        }
                        cmd1.Parameters.AddWithValue("@P_Name", TextBox10.Text);
                        cmd1.Parameters.AddWithValue("@Proj_Name", ComboBox8.Text);
                        cmd1.ExecuteNonQuery();

                        CON.Close();
                    }
                }


                DropDownList1.SelectedIndex = -1;
                txt_level1_id.Text = string.Empty;
                txt_level2_id.Text = string.Empty;
                txt_party_id.Text = string.Empty;
                txt_ppid.Text = string.Empty;
                txt_party_id.Text = string.Empty;
                C3.Text = string.Empty;
                C4.Text = string.Empty;
                TextBox10.Text = string.Empty;
                TextBox12.Text = string.Empty;
                TextBox13.Text = string.Empty;
                TextBox14.Text = string.Empty;
                TextBox15.Text = string.Empty;
                TextBox16.Text = string.Empty;
                TextBox18.Text = string.Empty;
                TextBox19.Text = string.Empty;
                Label100.Text = string.Empty;
                ComboBox8.SelectedIndex = 0;
                RadioButton1.Checked = false;
                RadioButton2.Checked = false;

                code_check();
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            TextBox10.Text = string.Empty;
            TextBox12.Text = string.Empty;
            TextBox13.Text = string.Empty;
            TextBox14.Text = string.Empty;
            TextBox15.Text = string.Empty;
            TextBox16.Text = string.Empty;
            TextBox19.Text = string.Empty;
            TextBox18.Text = string.Empty;
            DropDownList1.SelectedIndex = 0;
            Label100.Text = string.Empty;
            ComboBox8.SelectedIndex = 0;
            RadioButton1.Checked = false;
            RadioButton2.Checked = false;
            code_check();
        }

        protected void DropDownList1_SelectedIndexChanged2(object sender, EventArgs e)
        {
           

        }

        protected void DropDownList1_SelectedIndexChanged1(object sender, EventArgs e)
        {

        }

        protected void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}