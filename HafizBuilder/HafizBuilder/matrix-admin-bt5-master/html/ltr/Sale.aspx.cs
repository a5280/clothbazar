﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace HafizBuilder.matrix_admin_bt5_master.html.ltr
{
    public partial class Sale : System.Web.UI.Page
    {
        string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;
        DataTable dtt = new DataTable();
        private string PID,PID2, PID1, Product_ID, Project_ID, Total,Total1,ty= "Sale";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (ViewState["Purchase"] == null)
                {
                    dtt.Columns.Add("Product_Name");
                    dtt.Columns.Add("Qty");
                    dtt.Columns.Add("Rate");
                    dtt.Columns.Add("Amount");

                    ViewState["Purchase"] = dtt;
                    dtt = (DataTable)ViewState["Purchase"];
                    dtt.Rows.Add(" No record available");
                    gridview.DataSource = dtt;
                    gridview.DataBind();

                    GlobalClass.id = 0;
                }
           
                code_check();
                loadcombo_list();
                ImageButton1.Focus();
            }
        }
        protected void Button4_Click(object sender, EventArgs e)
        {
            if (GlobalClass.id == 0)
            {
                dtt = (DataTable)ViewState["Purchase"];
                dtt.Rows.RemoveAt(0);
                gridview.DataSource = dtt;
                gridview.DataBind();
                GlobalClass.id = 1;
            }

            dtt = (DataTable)ViewState["Purchase"];

            dtt.Rows.Add(ComboBox2.Text, TextBox8.Text, TextBox3.Text, TextBox4.Text);
            gridview.DataSource = dtt;
            gridview.DataBind();
           

            TextBox8.Text = "";
            TextBox3.Text = "";
            TextBox4.Text = "";
            ComboBox2.SelectedIndex=-1;
            ComboBox2.Focus();
            Sum();

        }
        public void nullvalue()
        {
            gridview.DataSource = null;
            gridview.DataBind();
        }
        public void record()
        {
            dtt.Columns.Add("Product_Name");
            dtt.Columns.Add("Qty");
            dtt.Columns.Add("Rate");
            dtt.Columns.Add("Amount");

            ViewState["Purchase"] = dtt;
            dtt = (DataTable)ViewState["Purchase"];
            dtt.Rows.Add(" No record available");
            gridview.DataSource = dtt;
            gridview.DataBind();

        }
        Double AABC;
        protected void Button6_Click(object sender, EventArgs e)
        {

            using (SqlConnection con = new SqlConnection(cnString))
            {
                ViewState["voc_no"] = TextBox5.Text;
                ViewState["date"] = TextBox1.Text;
                for (int i = 0; i < gridview.Rows.Count; ++i)
                {
                    con.Open();
                    string QRY4 = "select Product_# FROM Product where Product_Name='" + gridview.Rows[i].Cells[1].Text + "' ";
                    SqlCommand cmd4 = new SqlCommand(QRY4, con);
                    SqlDataAdapter sa4 = new SqlDataAdapter(cmd4);
                    DataTable dt4 = new DataTable();
                    sa4.Fill(dt4);
                    foreach (DataRow item1 in dt4.Rows)
                    {
                        Product_ID = item1["Product_#"].ToString();
                    }

                    string QRY7 = "select Project_# FROM Project where Project_Name='" + ComboBox3.Text + "' ";
                    SqlCommand cmd7 = new SqlCommand(QRY7, con);
                    SqlDataAdapter saa7 = new SqlDataAdapter(cmd7);
                    DataTable dttt7 = new DataTable();
                    saa7.Fill(dttt7);
                    foreach (DataRow item1 in dttt7.Rows)
                    {
                        Project_ID = item1["Project_#"].ToString();
                    }

                    string QRY11 = "select Account_# FROM Party_AC where Account_Name='" + ComboBox1.Text + "' ";
                    SqlCommand cmd11 = new SqlCommand(QRY11, con);
                    SqlDataAdapter saa = new SqlDataAdapter(cmd11);
                    DataTable dttt = new DataTable();
                    saa.Fill(dttt);
                    foreach (DataRow item1 in dttt.Rows)
                    {
                        PID2 = item1["Account_#"].ToString();
                    }

                    string QRY = "INSERT INTO Stock (Voucher_#,Date,P_Account_#,Party_Name,Naration,Vou_TYPE,Product_#,Qty,Rate,Amount,Net_Total,Product_Name,Project_Name,Pre_Blc,Project_#) VALUES (@Voc#,@DATE,@P_AC,@P_Name,@Nara1,@V_TYPE,@Pro_#,@qty,@rate,@amt,@net_a,@Pro_Name,@Project_name,@Pre_Blc,@Project_id)";
                    SqlCommand cmd2 = new SqlCommand(QRY, con);
                    cmd2.CommandType = CommandType.Text;
                      
                    if (ty == "Sale")
                    {
                        AABC = Convert.ToDouble(gridview.Rows[i].Cells[2].Text) * -1;
                    }
              
                    cmd2.Parameters.AddWithValue("@Voc#", ViewState["voc_no"].ToString());
                    cmd2.Parameters.AddWithValue("@DATE", ViewState["date"].ToString());
                    cmd2.Parameters.AddWithValue("@P_AC", PID2);
                    cmd2.Parameters.AddWithValue("@P_Name", ComboBox1.Text);
                    cmd2.Parameters.AddWithValue("@Nara1", TextBox2.Text);
                    cmd2.Parameters.AddWithValue("@V_TYPE", "SV");
                    cmd2.Parameters.AddWithValue("@Pro_#", Product_ID);
                    cmd2.Parameters.AddWithValue("@qty", AABC);
                    cmd2.Parameters.AddWithValue("@rate", gridview.Rows[i].Cells[3].Text);
                    cmd2.Parameters.AddWithValue("@amt", gridview.Rows[i].Cells[4].Text);
                    cmd2.Parameters.AddWithValue("@net_a", gridview.FooterRow.Cells[4].Text);
                    cmd2.Parameters.AddWithValue("@Pro_Name", gridview.Rows[i].Cells[1].Text);
                    cmd2.Parameters.AddWithValue("@Project_name", ComboBox3.Text);
                    cmd2.Parameters.AddWithValue("@Pre_Blc", TextBox6.Text);
                    cmd2.Parameters.AddWithValue("@Project_id", Project_ID);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                 }

                    con.Open();
                    //////////////////////////////////////////////////////// SALE CREDIT
                    string QRY1 = "select Account_# FROM Party_AC where Account_Name='SALE'";
                    SqlCommand cmd3 = new SqlCommand(QRY1, con);
                    SqlDataAdapter sa = new SqlDataAdapter(cmd3);
                    DataTable dt = new DataTable();
                    sa.Fill(dt);
                    foreach (DataRow item in dt.Rows)
                    {
                        PID = item["Account_#"].ToString();
                    }
                    string QRY2 = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration,Naration1,Invoice,Debit,Credit,Party_Name) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name)";
                    SqlCommand cmd1 = new SqlCommand(QRY2, con);
                    cmd1.CommandType = CommandType.Text;
                    cmd1.Parameters.AddWithValue("@Voc#", ViewState["voc_no"].ToString());
                    cmd1.Parameters.AddWithValue("@P_AC", PID);
                    cmd1.Parameters.AddWithValue("@DATE", ViewState["date"].ToString());
                    cmd1.Parameters.AddWithValue("@V_TYPE", "SV");
                    cmd1.Parameters.AddWithValue("@Nara", gridview.Rows[0].Cells[1].Text + " " + gridview.Rows[0].Cells[2].Text + "@" + gridview.Rows[0].Cells[4].Text);
                    cmd1.Parameters.AddWithValue("@Nara1",TextBox2.Text);
                    cmd1.Parameters.AddWithValue("@INV", ViewState["voc_no"].ToString());
                    cmd1.Parameters.AddWithValue("@DEBIT", 0);
                    cmd1.Parameters.AddWithValue("@CREDIT", gridview.FooterRow.Cells[4].Text);
                    cmd1.Parameters.AddWithValue("@P_Name", "SALE");
                    cmd1.ExecuteNonQuery();

                    /////////////////////////////////////////////////////////////////////// PARTY DEBIT
                    string QRY6 = "select Account_# FROM Party_AC where Account_Name='"+ComboBox1.Text+"'";
                    SqlCommand cmd6 = new SqlCommand(QRY6, con);
                    SqlDataAdapter sa6 = new SqlDataAdapter(cmd6);
                    DataTable dt6 = new DataTable();
                    sa6.Fill(dt6);
                    foreach (DataRow item in dt6.Rows)
                    {
                        PID1 = item["Account_#"].ToString();
                    }

                    string QRY5 = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration,Naration1,Invoice,Debit,Credit,Party_Name) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name)";
                    SqlCommand cmd5 = new SqlCommand(QRY5, con);
                    cmd5.CommandType = CommandType.Text;
                    cmd5.Parameters.AddWithValue("@Voc#", ViewState["voc_no"].ToString());
                    cmd5.Parameters.AddWithValue("@P_AC", PID1);
                    cmd5.Parameters.AddWithValue("@DATE", ViewState["date"].ToString());
                    cmd5.Parameters.AddWithValue("@V_TYPE", "SV");
                    cmd5.Parameters.AddWithValue("@Nara", gridview.Rows[0].Cells[1].Text + " " + gridview.Rows[0].Cells[2].Text + "@" + gridview.Rows[0].Cells[4].Text);
                    cmd5.Parameters.AddWithValue("@Nara1", TextBox2.Text);
                    cmd5.Parameters.AddWithValue("@INV", ViewState["voc_no"].ToString());
                    cmd5.Parameters.AddWithValue("@DEBIT", gridview.FooterRow.Cells[4].Text);
                    cmd5.Parameters.AddWithValue("@CREDIT",0);
                    cmd5.Parameters.AddWithValue("@P_Name", ComboBox1.Text); 
                    cmd5.ExecuteNonQuery();

                    con.Close();
                    txtzero();
                    loadcombo_list();
                    code_check();
            }
            nullvalue();
            record();
            gridview.Rows[0].Cells[1].ColumnSpan = 2;
            gridview.Rows[0].Cells.RemoveAt(0);
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
            TextBox6.Text = "";
            TextBox4.Text = "";
            GlobalClass.id = 0;
    
        }
        protected void grid_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (!IsPostBack)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[1].ColumnSpan = 2;
                    //now make up for the colspan from cell2
                    e.Row.Cells.RemoveAt(0);
                }
            }

        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            int rowid = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            ComboBox2.Text = gridview.Rows[rowid].Cells[1].Text;
            TextBox8.Text = gridview.Rows[rowid].Cells[2].Text;
            TextBox3.Text = gridview.Rows[rowid].Cells[3].Text;
            TextBox4.Text = gridview.Rows[rowid].Cells[4].Text;

            dtt = (DataTable)ViewState["Purchase"];
            dtt.Rows.RemoveAt(rowid);
            gridview.DataSource = dtt;
            gridview.DataBind();
            Sum();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            dtt = (DataTable)ViewState["Purchase"];
            int rowid = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            dtt.Rows.RemoveAt(rowid);
            gridview.DataSource = dtt;
            gridview.DataBind();

            if (gridview.Rows.Count == 0)
            {
                ViewState["Purchase"] = dtt;
                dtt = (DataTable)ViewState["Purchase"];

                dtt.Rows.Add(" No record available");
                gridview.DataSource = dtt;
                gridview.DataBind();
                gridview.Rows[0].Cells[1].ColumnSpan = 2;
                gridview.Rows[0].Cells.RemoveAt(0);
                GlobalClass.id = 0;
            }
            for (int i = 0; i < gridview.Rows.Count; i++)
            {
                if (gridview.Rows.Count == 1)
                {
                    sum = 0;
                    qty = 0;
                }
                else
                {
                    sum += int.Parse(gridview.Rows[i].Cells[4].Text);
                    qty += int.Parse(gridview.Rows[i].Cells[2].Text);
                }
            }
            Total = sum.ToString();
            Total1 = qty.ToString();
            gridview.FooterRow.Cells[1].Text = "Total Amount";
            gridview.FooterRow.Cells[4].Text = Total;
            gridview.FooterRow.Cells[2].Text = Total1;
        }

        public void loadcombo_list()
        {
            SqlConnection con = new SqlConnection(cnString);
            using (con)
            {
                SqlCommand cmd = new SqlCommand("select distinct Account_Name from Party_AC where Account_Type!= 'BUILTEN' ", con);
                con.Open();
                ComboBox1.DataSource = cmd.ExecuteReader();
                ComboBox1.DataTextField = "Account_Name";
                ComboBox1.DataBind();
                con.Close();

                SqlCommand cmd1 = new SqlCommand("select distinct Project_Name from Project ", con);
                con.Open();
                ComboBox3.DataSource = cmd1.ExecuteReader();
                ComboBox3.DataTextField = "Project_Name";
                ComboBox3.DataBind();
                con.Close();

                SqlCommand cmd2 = new SqlCommand("select distinct Product_Name from Product ", con);
                con.Open();
                ComboBox2.DataSource = cmd2.ExecuteReader();
                ComboBox2.DataTextField = "Product_Name";
                ComboBox2.DataBind();
                con.Close();

            }
        }
        public void code_check()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select isnull(max(voucher_#),0)+1 as id from party_ledger where vou_type = 'SV'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    TextBox5.Text = item["id"].ToString();
                }
                //SqlCommand cmd = new SqlCommand("select count(Voucher_#) from Party_Ledger", con);
                //int check_null_db = Convert.ToInt32(cmd.ExecuteScalar());

                //if (check_null_db != Convert.ToInt32(null))
                //{
                //    SqlCommand cmd1 = new SqlCommand("select max(Voucher_# + 1) from Party_Ledger", con);
                //    int main_code = Convert.ToInt32(cmd1.ExecuteScalar());
                //    TextBox5.Text = main_code.ToString();
                //}
                //else
                //{
                //    TextBox5.Text = "1";
                //}
                con.Close();
            }

        }

        public void Pre_blc()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(" SELECT ISNULL(sum(Debit),0)- ISNULL(sum(Credit),0) FROM Party_Ledger WHERE P_Account_# in(select Account_# from Party_AC where Account_Name = '"+ComboBox1.Text+"' ) ", con);
                int total_ope_blc = Convert.ToInt32(cmd.ExecuteScalar());
                TextBox6.Text = total_ope_blc.ToString();
                con.Close();
            }
        }

      

        int sum = 0,qty=0;
        public void Sum()
        {
            for (int i = 0; i < gridview.Rows.Count; i++)
            {
                sum += int.Parse(gridview.Rows[i].Cells[4].Text);
                qty += int.Parse(gridview.Rows[i].Cells[2].Text);
            }
            Total = sum.ToString();
            Total1 = qty.ToString();

            gridview.FooterRow.Cells[1].Text = "Total";

            gridview.FooterRow.Cells[4].Text = Total;
            gridview.FooterRow.Cells[2].Text = Total1;

        }

        protected void TextBox3_TextChanged(object sender, EventArgs e)
        {
            try
            {

                int rate_amt = Convert.ToInt32(TextBox3.Text) * Convert.ToInt32(TextBox8.Text);

                TextBox4.Text = rate_amt.ToString();
                if (GlobalClass.post == 0)
                {
                    ViewState["voc_no"] = 0;
                    nullvalue();
                    record();
                    gridview.Rows[0].Cells[1].ColumnSpan = 2;
                    gridview.Rows[0].Cells.RemoveAt(0);

                    GlobalClass.id = 0;
                    GlobalClass.post = 1;
                }


            }
            catch
            {
            }
        }

        public void txtzero()
        {
            ComboBox1.SelectedIndex = -1;
            TextBox1.Text = string.Empty;
            TextBox5.Text = string.Empty;
            TextBox2.Text = string.Empty;
            TextBox3.Text = string.Empty;
            TextBox4.Text = string.Empty;

        }

        protected void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
                Pre_blc();
                record();
                gridview.Rows[0].Cells[1].ColumnSpan = 2;
                gridview.Rows[0].Cells.RemoveAt(0);
                GlobalClass.id = 0;    
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            nullvalue();
            record();
            gridview.Rows[0].Cells[1].ColumnSpan = 2;
            gridview.Rows[0].Cells.RemoveAt(0);
            txtzero();
            code_check();
            GlobalClass.id = 0;
            ComboBox3.SelectedIndex = -1;
            TextBox6.Text = "";
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                string QRYQ = "DELETE FROM Party_Ledger WHERE Vou_TYPE = 'SV' AND Voucher_# ='" + TextBox5.Text + "' ";
                SqlCommand cmd = new SqlCommand(QRYQ, con);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                con.Close();

                con.Open();
                string QRYQ8 = "DELETE FROM Stock WHERE Vou_TYPE = 'SV' AND Voucher_# ='" + TextBox5.Text + "' ";
                SqlCommand cmd8 = new SqlCommand(QRYQ8, con);
                cmd8.CommandType = CommandType.Text;
                cmd8.ExecuteNonQuery();
                con.Close();
            }
            Button1_Click(Button1, null);
            code_check();
            ComboBox3.SelectedIndex = -1;
            TextBox6.Text = "";
        }
        protected void Button7_Click(object sender, EventArgs e)
        {
            if (ComboBox1.Text !=" " && ComboBox2.Text !=" " && ComboBox3.Text!=" ")
            {

                using (SqlConnection con = new SqlConnection(cnString))
                {
                        ViewState["voc_no"] = TextBox5.Text;
                        ViewState["date"] = TextBox1.Text;

                        con.Open();
                        string QRYQ = "DELETE FROM Party_Ledger WHERE Vou_TYPE = 'SV' AND Voucher_# ='" + TextBox5.Text + "' ";
                        SqlCommand cmd = new SqlCommand(QRYQ, con);
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                        con.Close();

                        con.Open();
                        string QRYQ8= "DELETE FROM Stock WHERE Vou_TYPE = 'SV' AND Voucher_# ='" + TextBox5.Text + "' ";
                        SqlCommand cmd8 = new SqlCommand(QRYQ8, con);
                        cmd8.CommandType = CommandType.Text;
                        cmd8.ExecuteNonQuery();
                        con.Close();


                    for (int i = 0; i < gridview.Rows.Count; ++i)
                    {
                        con.Open();
                        string QRY4 = "select Product_# FROM Product where Product_Name='" + gridview.Rows[i].Cells[1].Text + "' ";
                        SqlCommand cmd4 = new SqlCommand(QRY4, con);
                        SqlDataAdapter sa4 = new SqlDataAdapter(cmd4);
                        DataTable dt4 = new DataTable();
                        sa4.Fill(dt4);
                        foreach (DataRow item1 in dt4.Rows)
                        {
                            Product_ID = item1["Product_#"].ToString();
                        }

                        string QRY7 = "select Project_# FROM Project where Project_Name='" + ComboBox3.Text + "' ";
                        SqlCommand cmd7 = new SqlCommand(QRY7, con);
                        SqlDataAdapter saa7 = new SqlDataAdapter(cmd7);
                        DataTable dttt7 = new DataTable();
                        saa7.Fill(dttt7);
                        foreach (DataRow item1 in dttt7.Rows)
                        {
                            Project_ID = item1["Project_#"].ToString();
                        }

                        string QRY11 = "select Account_# FROM Party_AC where Account_Name='" + ComboBox1.Text + "' ";
                        SqlCommand cmd11 = new SqlCommand(QRY11, con);
                        SqlDataAdapter saa = new SqlDataAdapter(cmd11);
                        DataTable dttt = new DataTable();
                        saa.Fill(dttt);
                        foreach (DataRow item1 in dttt.Rows)
                        {
                            PID2 = item1["Account_#"].ToString();
                        }

                        string QRY = "INSERT INTO Stock (Voucher_#,Date,P_Account_#,Party_Name,Naration,Vou_TYPE,Product_#,Qty,Rate,Amount,Net_Total,Product_Name,Project_Name,Pre_Blc,Project_#) VALUES (@Voc#,@DATE,@P_AC,@P_Name,@Nara1,@V_TYPE,@Pro_#,@qty,@rate,@amt,@net_a,@Pro_Name,@Project_name,@Pre_Blc,@Project_id)";
                        SqlCommand cmd2 = new SqlCommand(QRY, con);
                        cmd2.CommandType = CommandType.Text;

                        if (ty == "Sale")
                        {
                            AABC = Convert.ToDouble(gridview.Rows[i].Cells[2].Text) * -1;
                        }

                        cmd2.Parameters.AddWithValue("@Voc#", ViewState["voc_no"].ToString());
                        cmd2.Parameters.AddWithValue("@DATE", ViewState["date"].ToString());
                        cmd2.Parameters.AddWithValue("@P_AC", PID2);
                        cmd2.Parameters.AddWithValue("@P_Name", ComboBox1.Text);
                        cmd2.Parameters.AddWithValue("@Nara1", TextBox2.Text);
                        cmd2.Parameters.AddWithValue("@V_TYPE", "SV");
                        cmd2.Parameters.AddWithValue("@Pro_#", Product_ID);
                        cmd2.Parameters.AddWithValue("@qty", AABC);
                        cmd2.Parameters.AddWithValue("@rate", gridview.Rows[i].Cells[3].Text);
                        cmd2.Parameters.AddWithValue("@amt", gridview.Rows[i].Cells[4].Text);
                        cmd2.Parameters.AddWithValue("@net_a", gridview.FooterRow.Cells[4].Text);
                        cmd2.Parameters.AddWithValue("@Pro_Name", gridview.Rows[i].Cells[1].Text);
                        cmd2.Parameters.AddWithValue("@Project_name", ComboBox3.Text);
                        cmd2.Parameters.AddWithValue("@Pre_Blc", TextBox6.Text);
                        cmd2.Parameters.AddWithValue("@Project_id", Project_ID);
                        cmd2.ExecuteNonQuery();
                        con.Close();
                    }

                    con.Open();
                    //////////////////////////////////////////////////////// SALE CREDIT
                    string QRY1 = "select Account_# FROM Party_AC where Account_Name='SALE'";
                    SqlCommand cmd3 = new SqlCommand(QRY1, con);
                    SqlDataAdapter sa = new SqlDataAdapter(cmd3);
                    DataTable dt = new DataTable();
                    sa.Fill(dt);
                    foreach (DataRow item in dt.Rows)
                    {
                        PID = item["Account_#"].ToString();
                    }
                    string QRY2 = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration,Naration1,Invoice,Debit,Credit,Party_Name) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name)";
                    SqlCommand cmd1 = new SqlCommand(QRY2, con);
                    cmd1.CommandType = CommandType.Text;
                    cmd1.Parameters.AddWithValue("@Voc#", ViewState["voc_no"].ToString());
                    cmd1.Parameters.AddWithValue("@P_AC", PID);
                    cmd1.Parameters.AddWithValue("@DATE", ViewState["date"].ToString());
                    cmd1.Parameters.AddWithValue("@V_TYPE", "SV");
                    cmd1.Parameters.AddWithValue("@Nara", gridview.Rows[0].Cells[1].Text + " " + gridview.Rows[0].Cells[2].Text + "@" + gridview.Rows[0].Cells[4].Text);
                    cmd1.Parameters.AddWithValue("@Nara1", TextBox2.Text);
                    cmd1.Parameters.AddWithValue("@INV", ViewState["voc_no"].ToString());
                    cmd1.Parameters.AddWithValue("@DEBIT", 0);
                    cmd1.Parameters.AddWithValue("@CREDIT", gridview.FooterRow.Cells[4].Text);
                    cmd1.Parameters.AddWithValue("@P_Name", "SALE");
                    cmd1.ExecuteNonQuery();

                    /////////////////////////////////////////////////////////////////////// PARTY DEBIT
                    string QRY6 = "select Account_# FROM Party_AC where Account_Name='" + ComboBox1.Text + "'";
                    SqlCommand cmd6 = new SqlCommand(QRY6, con);
                    SqlDataAdapter sa6 = new SqlDataAdapter(cmd6);
                    DataTable dt6 = new DataTable();
                    sa6.Fill(dt6);
                    foreach (DataRow item in dt6.Rows)
                    {
                        PID1 = item["Account_#"].ToString();
                    }

                    string QRY5 = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration,Naration1,Invoice,Debit,Credit,Party_Name) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name)";
                    SqlCommand cmd5 = new SqlCommand(QRY5, con);
                    cmd5.CommandType = CommandType.Text;
                    cmd5.Parameters.AddWithValue("@Voc#", ViewState["voc_no"].ToString());
                    cmd5.Parameters.AddWithValue("@P_AC", PID1);
                    cmd5.Parameters.AddWithValue("@DATE", ViewState["date"].ToString());
                    cmd5.Parameters.AddWithValue("@V_TYPE", "SV");
                    cmd5.Parameters.AddWithValue("@Nara", gridview.Rows[0].Cells[1].Text + " " + gridview.Rows[0].Cells[2].Text + "@" + gridview.Rows[0].Cells[4].Text);
                    cmd5.Parameters.AddWithValue("@Nara1", TextBox2.Text);
                    cmd5.Parameters.AddWithValue("@INV", ViewState["voc_no"].ToString());
                    cmd5.Parameters.AddWithValue("@DEBIT", gridview.FooterRow.Cells[4].Text);
                    cmd5.Parameters.AddWithValue("@CREDIT", 0);
                    cmd5.Parameters.AddWithValue("@P_Name", ComboBox1.Text);
                    cmd5.ExecuteNonQuery();

                    con.Close();
                    txtzero();
                    loadcombo_list();
                    code_check();
                }

                ViewState["voc_no"] = 0;
                nullvalue();
                record();
                gridview.Rows[0].Cells[1].ColumnSpan = 2;
                gridview.Rows[0].Cells.RemoveAt(0);
                TextBox1.Text = "";
                TextBox2.Text = "";
                TextBox3.Text = "";
                TextBox4.Text = "";
                ComboBox3.SelectedIndex = -1;
                GlobalClass.id = 0;
           
   
            }
        }
        public void search()
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {
                ViewState["voc_no"] = TextBox5.Text;
                string QRY = " select Voucher_#,Date,Party_Name,Naration,Project_Name,Product_Name,Qty,Rate,Amount from Stock where Voucher_#= '"+TextBox5.Text+"' ";
                SqlCommand cmd = new SqlCommand(QRY, CON);
                SqlDataAdapter DA = new SqlDataAdapter(cmd);
                DataTable DT = new DataTable();
                DA.Fill(DT);

                dtt.Columns.Add("Product_Name");
                dtt.Columns.Add("Qty");
                dtt.Columns.Add("Rate");
                dtt.Columns.Add("Amount");

                ViewState["Purchase"] = dtt;
                dtt = (DataTable)ViewState["Purchase"];

                foreach (DataRow item in DT.Rows)
                {

                    TextBox1.Text = Convert.ToDateTime(item["DATE"]).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                    TextBox5.Text = item["Voucher_#"].ToString();
                    ComboBox1.Text = item["Party_Name"].ToString();
                    TextBox2.Text= item["Naration"].ToString();
                    ComboBox3.Text = item["Project_Name"].ToString();


                    dtt.Rows.Add(item["Product_Name"].ToString(), item["Qty"].ToString(), item["Rate"].ToString(), item["Amount"].ToString());
                    gridview.DataSource = dtt;
                    gridview.DataBind();
                    GlobalClass.id = 1;
                    Pre_blc();
                }
                if (dtt.Rows.Count > 0)
                {

                    Sum();
                }
            }
            
        }
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select count(Voucher_#) from Stock where Voucher_#='"+TextBox5.Text+"' ", con);
                int check_null_db = Convert.ToInt32(cmd.ExecuteScalar());

                if (check_null_db != Convert.ToInt32(null))
                {
                    search();
                }
                else
                {
                    Response.Write("<script>alert('No record found')</script>");
                }
            }
                    
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            Sum();
        }

    }
}