﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Bank_Cheque_Transaction.aspx.cs" Inherits="HAFIZ_BUILDERS.matrix_admin_bt5_master.html.ltr.Bank_Cheque_Transaction" %>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, Tech No Hub lite admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, Tech No Hub lite design, Tech No Hub lite dashboard bootstrap 5 dashboard template">
    <meta name="description"
        content="Tech No Hub Lite Free Version is powerful and clean admin dashboard template, inpired from Bootstrap Framework">
    <meta name="robots" content="noindex,nofollow">
   <title>Hafiz Builder</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <link href="../../assets/libs/toastr/build/toastr.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 27px
        }
        .auto-style3 {
            width: 124px;
        }
        .auto-style4 {
            width: 86px;
        }
    </style>
</head>

<body>
    
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
       <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
     <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b class="logo-icon ps-2">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="../../assets/images/logo-icon.png" alt="homepage" class="light-logo" />

                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text">
                            <!-- dark Logo text -->
                           
                            <span>Hafiz Builder</span>

                        </span>
                        <!-- Logo icon -->
                        <!-- <b class="logo-icon"> -->
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                        <!-- Dark Logo icon -->
                        <!-- <img src="../../assets/images/logo-text.png" alt="homepage" class="light-logo" /> -->

                        <!-- </b> -->
                        <!--End Logo icon -->
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-start me-auto">
                        <li class="nav-item d-none d-lg-block"><a
                                class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)"
                                data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- create new -->
                        <!-- ============================================================== -->
                       <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <span class="d-none d-md-block">Create New User <i class="fa fa-angle-down"></i></span>
                                <span class="d-block d-md-none"><i class="fa fa-plus"></i></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="Create_User.aspx">ADD User</a></li>
                              
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search position-absolute">
                                <input type="text" class="form-control" placeholder="Search &amp; enter"> <a
                                    class="srh-btn"><i class="ti-close"></i></a>
                            </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-end">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-bell font-24"></i>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" id="2" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                 <i class="font-24 mdi mdi-comment-processing"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end mailbox animated bounceInDown" aria-labelledby="2">
                                <ul class="list-style-none">
                                    <li>
                                        <div class="">
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-success btn-circle"><i
                                                            class="ti-calendar"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Event today</h5>
                                                        <span class="mail-desc">Just a reminder that event</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-info btn-circle"><i
                                                            class="ti-settings"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Settings</h5>
                                                        <span class="mail-desc">You can customize this template</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-primary btn-circle"><i
                                                            class="ti-user"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Pavan kumar</h5>
                                                        <span class="mail-desc">Just see the my admin!</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-danger btn-circle"><i
                                                            class="fa fa-link"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Luanch Admin</h5>
                                                        <span class="mail-desc">Just see the my new admin!</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="../../assets/images/users/1.jpg" alt="user" class="rounded-circle" width="31">
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end user-dd animated" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user me-1 ms-1"></i>
                                    My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet me-1 ms-1"></i>
                                    My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email me-1 ms-1"></i>
                                    Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i
                                        class="ti-settings me-1 ms-1"></i> Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i
                                        class="fa fa-power-off me-1 ms-1"></i> Logout</a>
                                <div class="dropdown-divider"></div>
                                <div class="ps-4 p-10"><a href="javascript:void(0)"
                                        class="btn btn-sm btn-success btn-rounded text-white">View Profile</a></div>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
            <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="pt-4">
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link"
                               href="index1.aspx" aria-expanded="false">
                                <i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-chart-line "></i><span class="hide-menu">Accounts</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="Chart_of_account.aspx" class="sidebar-link">
                                        <i class="far fa-user-circle"></i><span class="hide-menu">
                                            Chart of Account
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Cash_Receipt_Voucher.aspx" class="sidebar-link">
                                        <i class="mdi mdi-receipt"></i><span class="hide-menu">
                                            Cash Receipt Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Cash_Payment_Voucher.aspx" class="sidebar-link">
                                        <i class="far fa-credit-card"></i><span class="hide-menu">
                                            Cash Payment Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Bank_Receipt_Voucher.aspx" class="sidebar-link">
                                        <i class="mdi mdi-receipt"></i><span class="hide-menu">
                                            Bank Receipt Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Bankk_Payment_Voucher.aspx" class="sidebar-link">
                                        <i class="far fa-credit-card"></i><span class="hide-menu">
                                            Bank Payment Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Journall_Voucher.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Journal Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Account_Name_Update.aspx" class="sidebar-link">
                                        <i class="fas fa-edit"></i><span class="hide-menu">
                                            Account Name Update
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Bank_Cheque_Transaction.aspx" class="sidebar-link">
                                        <i class="fas fa-money-bill-alt"></i><span class="hide-menu">
                                            Bank Cheque Transaction
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="mdi mdi-chart-bar"></i><span class="hide-menu">Inventory</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="Project.aspx" class="sidebar-link">
                                        <i class="fas fa-building"></i><span class="hide-menu">
                                            Project Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Product.aspx" class="sidebar-link">
                                        <i class="fas fa-boxes"></i><span class="hide-menu">
                                            Product Voucher
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-shopping-cart"></i><span class="hide-menu">Purchase</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="Purchase.aspx" class="sidebar-link">
                                        <i class="fas fa-shopping-basket"></i><span class="hide-menu">
                                            Purchase Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Purchase_Return.aspx" class="sidebar-link">
                                        <i class="fas fa-undo-alt"></i><span class="hide-menu">
                                            Purchase Return Voucher
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-edit"></i><span class="hide-menu">Sale</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="Sale.aspx" class="sidebar-link">
                                        <i class="fab fa-sellsy"></i><span class="hide-menu">
                                            Sale Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Sale_Return.aspx" class="sidebar-link">
                                        <i class="fas fa-undo-alt"></i><span class="hide-menu">
                                            Sale Return Voucher
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                         <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="far fa-chart-bar"></i><span class="hide-menu">Reports</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">                                                                                           
                                <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Reports
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <%--<li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="far fa-chart-bar"></i><span class="hide-menu">Cash Reports</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item" >
                                    <a href="Date_form.aspx"  class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"  ></i><span class="hide-menu">
                                            Cash Receive Report
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item" >
                                    <a href="Date_form.aspx"  class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"  ></i><span class="hide-menu">
                                            Cash Payment Report
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item" >
                                    <a href="Date_form.aspx"  class="sidebar-link">
                                        <i class="fas fa-file-medical-alt" ></i><span class="hide-menu">
                                            Bank Receive Report
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Bank Payment Report
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Journal Voucher Report
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="far fa-chart-bar"></i><span class="hide-menu">Purchase Reports</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item" >
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt" ></i><span class="hide-menu">
                                            Purchase Party Wise
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Purchase Product Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Purchase Date Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Project Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Return Party Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Return Product WS
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Return Date Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Return Project Wise
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                         <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="far fa-chart-bar"></i><span class="hide-menu">Sale Reports</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item" >
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt" ></i><span class="hide-menu">
                                            Sale Party Wise
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Sale Product Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Sale Date Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Project Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Return Party Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Return Product WS
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Return Date Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Return Project Wise
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>--%>

                        

                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-wrench"></i><span class="hide-menu">Utilities</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="icon-material.aspx" class="sidebar-link">
                                        <i class="mdi mdi-emoticon"></i><span class="hide-menu">
                                            Material Icons
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="icon-fontawesome.aspx" class="sidebar-link">
                                        <i class="mdi mdi-emoticon-cool"></i><span class="hide-menu">
                                            Font Awesome
                                            Icons
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-cogs"></i><span class="hide-menu">SetUp</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="icon-material.aspx" class="sidebar-link">
                                        <i class="mdi mdi-emoticon"></i><span class="hide-menu">
                                            Material Icons
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="icon-fontawesome.aspx" class="sidebar-link">
                                        <i class="mdi mdi-emoticon-cool"></i><span class="hide-menu">
                                            Font Awesome
                                            Icons
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="mdi mdi-account-key"></i><span class="hide-menu">Authentication</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="authentication-login.aspx" class="sidebar-link">
                                        <i class="fas fa-user"></i><span class="hide-menu"> Login </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Create_User.aspx" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i><span class="hide-menu">
                                            Register
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Bank Transactions Posting</h4>
                        <div class="ms-auto text-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" style="margin-top:0px">
                <form id="form1" runat="server">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title mb-0">Bank Transactions Posting</h5>
                            </div>
                            <div>
                                
                                <table class="auto-style1">
                                    <tr>
                                        <td class="auto-style2">&nbsp;</td>
                                        <td class="auto-style3">&nbsp;</td>
                                        <td class="auto-style4">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style2">
                                            <asp:Label ID="Label1" runat="server" Text="Date" Width="55px"></asp:Label>
                                        </td>
                                        <td class="auto-style3">
                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>

                                        </td>
                                        <td class="auto-style4">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                                                               
                                <br />
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#336666" BorderStyle="Double" AutoGenerateColumns="False" BorderWidth="3px" CellPadding="4" CellSpacing="4"  >
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353"  />

                                    <Columns>
                                        <asp:BoundField DataField="DATE" HeaderText="Voc Date"/>
                                        <asp:BoundField DataField="Voucher_#" HeaderText="Voc #"/>
                                        <asp:BoundField DataField="Vou_TYPE" HeaderText="Voc Type"/>
                                        <asp:BoundField DataField="Party_Name" HeaderText="Party Name" />
                                        <asp:BoundField DataField="Naration1" HeaderText="Naration"/>
                                        <asp:BoundField DataField="Credit" HeaderText="Amount"/>
                                        <asp:BoundField DataField="Chq#" HeaderText="Chq #"/>
                                        <asp:BoundField DataField="Chq_Date" HeaderText="Chq_Date"/>

                                        <asp:TemplateField HeaderText="Chq Status">
                                            <ItemTemplate>
                                                <asp:Label ID ="lblName" runat="server" Text='<%#Eval("Un_Present") %>' Visible="false"></asp:Label>
                                                <asp:DropDownList ID="dd1" runat="server" AutoPostBack = "true" OnSelectedIndexChanged="OnSelectedIndexChanged">                                                  
                                                        <asp:ListItem Text="Un_Present" Value="1"/>
                                                        <asp:ListItem Text="Present" Value="2" />
                                                        <asp:ListItem Text="Suspend" Value="3" />
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField DataField="Un_Present" HeaderText="Chq Status"/>--%>
                                    </Columns>
                                </asp:GridView>
                                    </ContentTemplate>
                               </asp:UpdatePanel>
                            </div>                          
                        </div>
                    </div>
                </div>      </form>            
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Admin Designed and Developed by TECH<b>NO</b>HUB.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="Scripts/jquery-2.1.1.js"></script>
<script src="Scripts/bootstrap.js"></script>
<script src="Scripts/bootstrap.min.js"></script>
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <!-- this page js -->
    <script src="../../assets/libs/toastr/build/toastr.min.js"></script>
    <script>
        $(function () {
            // Success Type
            $('#ts-success').on('click', function () {
                toastr.success('Have fun storming the castle!', 'Miracle Max Says');
            });

            // Success Type
            $('#ts-info').on('click', function () {
                toastr.info('We do have the Kapua suite available.', 'Turtle Bay Resort');
            });

            // Success Type
            $('#ts-warning').on('click', function () {
                toastr.warning('My name is Inigo Montoya. You killed my father, prepare to die!');
            });

            // Success Type
            $('#ts-error').on('click', function () {
                toastr.error('I do not think that word means what you think it means.', 'Inconceivable!');
            });
        });
    </script>
  
</body>

</html>