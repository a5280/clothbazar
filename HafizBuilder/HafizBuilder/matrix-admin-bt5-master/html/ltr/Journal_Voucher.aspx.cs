﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HafizBuilder.matrix_admin_bt5_master.html.ltr
{
    public partial class Journal_Voucher : System.Web.UI.Page
    {
        string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;
        private string PID, Total, Total1;
        protected void Page_Load(object sender, EventArgs e)
        {
            string JQueryVer = "1.7.1";
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
            {
                Path = "~/Scripts/jquery-" + JQueryVer + ".min.js",
                DebugPath = "~/Scripts/jquery-" + JQueryVer + ".js",
                CdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + JQueryVer + ".min.js",
                CdnDebugPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + JQueryVer + ".js",
                CdnSupportsSecureConnection = true,
                LoadSuccessExpression = "window.jQuery"
            });

            if (!IsPostBack)
            {
                code_check();
                FillGridViewEmpty();
                loadcombo_list();
            }
        }
        //[WebMethod]
        //[ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        //public static List<string> GetAccount_Name(string pre)
        //{
        //    string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;

        //    List<string> allACname = new List<string>();
        //    using (Hafiz_BuildersEntities4 dc=new Hafiz_BuildersEntities4() )
        //    {
        //        allACname = (from a in dc.Party_AC
        //                     where a.Account_Name.StartsWith(pre)
        //                     select a.Account_Name).ToList();
        //    }
        //    return allACname;
        //}
        public void FillGridViewEmpty()
        {
            try
            {
                //  SqlConnection con = new SqlConnection(@"Data Source=(local);Initial Catalog=Hafiz_Builders;Integrated Security=True");
                SqlConnection con = new SqlConnection(cnString);
                con.Open();
                GlobalClass.adap = new SqlDataAdapter("select * from Party_Ledger where Sr_No=0", con);
                SqlCommandBuilder bui = new SqlCommandBuilder(GlobalClass.adap);
                GlobalClass.dt = new DataTable();
                GlobalClass.adap.Fill(GlobalClass.dt);
                GridView2.DataSource = GlobalClass.dt;
                GridView2.DataBind();
                con.Close();
            }
            catch
            {
                Response.Write("<script> alert('Connection String Error...') </script>");
            }

        }
        public void FillGridView()
        {
            try
            {
                SqlConnection con = new SqlConnection(cnString);
                GlobalClass.adap = new SqlDataAdapter("select Sr_No,Party_Name,Naration1,Invoice,Debit,Credit from Party_Ledger where Voucher_# = '" + TextBox5.Text + "'", con);
                SqlCommandBuilder bui = new SqlCommandBuilder(GlobalClass.adap);
                GlobalClass.dt = new DataTable();
                GlobalClass.adap.Fill(GlobalClass.dt);
                GridView2.DataSource = GlobalClass.dt;
                GridView2.DataBind();
            }
            catch
            {
                Response.Write("<script> alert('Connection String Error...') </script>");
            }
        }
        public void loadcombo_list()
        {
            SqlConnection con = new SqlConnection(cnString);
            using (con)
            {
                SqlCommand cmd = new SqlCommand("select distinct Account_Name from Party_AC where Account_Type!= 'BUILTEN' ", con);
                con.Open();
                ComboBox1.DataSource = cmd.ExecuteReader();
                ComboBox1.DataTextField = "Account_Name";
                ComboBox1.DataBind();
                con.Close();
            }
        }
        public void code_check()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select count(Voucher_#) from Party_Ledger", con);
                int check_null_db = Convert.ToInt32(cmd.ExecuteScalar());

                if (check_null_db != Convert.ToInt32(null))
                {
                    SqlCommand cmd1 = new SqlCommand("select max(Voucher_# + 1) from Party_Ledger", con);
                    int main_code = Convert.ToInt32(cmd1.ExecuteScalar());
                    TextBox5.Text = main_code.ToString();
                }
                else
                {
                    TextBox5.Text = "1";
                }
                con.Close();
            }

        }
        public void fill_gridview_after_adding()
        {
            ///////////////////////////////////Fill Gridview
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter("select Sr_No,Party_Name,Naration1,Invoice,Debit,Credit from Party_Ledger where Voucher_# = '" + TextBox5.Text + "' ", con);
                DataTable dtt = new DataTable();
                da.Fill(dtt);
                GridView2.DataSource = dtt;
                GridView2.DataBind();
                con.Close();

            }
        }
        public void Sum()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                string QRY = "select sum(Debit) FROM Party_Ledger where Voucher_# = '" + TextBox5.Text + "'  ";
                string QRY1 = "select sum(Credit) FROM Party_Ledger where Voucher_# = '" + TextBox5.Text + "'  ";

                SqlCommand cmd = new SqlCommand(QRY, con);
                SqlCommand cmd1 = new SqlCommand(QRY1, con);

                Total = Convert.ToString(cmd.ExecuteScalar());
                Total1 = Convert.ToString(cmd1.ExecuteScalar());

                GridView2.FooterRow.Cells[1].Text = "Total Amount";
                GridView2.FooterRow.Cells[4].Text = Total;
                GridView2.FooterRow.Cells[5].Text = Total1;
                con.Close();           
            }
        }
        protected void editRecord(object sender, GridViewEditEventArgs e)
        {
            // Get the current row index for edit record
            GridView2.EditIndex = e.NewEditIndex;
            FillGridView();
        }
        protected void cancelRecord(object sender, GridViewCancelEditEventArgs e)
        {
            GridView2.EditIndex = -1;
            /// FillGridView();
            fill_gridview_after_adding();
            Sum();
        }
        protected void AddNewRecord(object sender, EventArgs e)
        {
            try
            {
                if (GlobalClass.dt.Rows.Count > 0)
                {
                    GridView2.EditIndex = -1;
                    GridView2.ShowFooter = true;
                    FillGridView();
                }
                else
                {
                    GridView2.ShowFooter = true;
                    DataRow dr = GlobalClass.dt.NewRow();
                    dr["Party"] = "0";
                    dr["DISCRIPTION"] = "0";
                    dr["INVOICE"] = 0;
                    dr["Amount"] = "0";
                    GlobalClass.dt.Rows.Add(dr);
                    GridView2.DataSource = GlobalClass.dt;
                    GridView2.DataBind();
                    GridView2.Rows[0].Visible = false;
                }
            }
            catch
            {
                Response.Write("<script> alert('Row not added in DataTable...') </script>");
            }
        }
        protected void AddNewCancel(object sender, EventArgs e)
        {
            GridView2.ShowFooter = false;
            FillGridView();
        }
        protected void InsertNewRecord(object sender, EventArgs e)
        {
            try
            {
                string strName = GlobalClass.dt.Rows[0]["Party"].ToString();
                if (strName == "0")
                {
                    GlobalClass.dt.Rows[0].Delete();
                    GlobalClass.adap.Update(GlobalClass.dt);
                }
                TextBox txtName = GridView2.FooterRow.FindControl("txtNewName") as TextBox;
                TextBox txtCountry = GridView2.FooterRow.FindControl("txtNewCountry") as TextBox;
                TextBox txtSalary = GridView2.FooterRow.FindControl("txtNewSalary") as TextBox;
                TextBox txtAmt = GridView2.FooterRow.FindControl("txtNewAmt") as TextBox;

                Guid FileName = Guid.NewGuid();
                DataRow dr = GlobalClass.dt.NewRow();

                dr["Party"] = txtName.Text.Trim();
                dr["DISCRIPTION"] = txtCountry.Text.Trim();
                dr["INVOICE"] = txtSalary.Text.Trim();
                dr["Amount"] = txtAmt.Text.Trim();
                GlobalClass.dt.Rows.Add(dr);
                GlobalClass.adap.Update(GlobalClass.dt);
                GridView2.ShowFooter = false;
                FillGridView();
            }
            catch
            {
                Response.Write("<script> alert('Record not added...') </script>");
            }

        }
  
        protected void updateRecord(object sender, GridViewUpdateEventArgs e)
        {
            TextBox txtName = GridView2.Rows[e.RowIndex].FindControl("txtName") as TextBox;
            TextBox txtCountry = GridView2.Rows[e.RowIndex].FindControl("txtCountry") as TextBox;
            TextBox txtSalary = GridView2.Rows[e.RowIndex].FindControl("txtSalary") as TextBox;
            TextBox txtDebit = GridView2.Rows[e.RowIndex].FindControl("txtDebit") as TextBox;
            TextBox txtCredit = GridView2.Rows[e.RowIndex].FindControl("txtCredit") as TextBox;
            try
            {
                Guid FileName = Guid.NewGuid();
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Party_Name"] = txtName.Text.Trim();
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Naration1"] = txtCountry.Text.Trim();
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Invoice"] = Convert.ToInt32(txtSalary.Text.Trim());
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Debit"] = Convert.ToInt32(txtDebit.Text.Trim());
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Credit"] = Convert.ToInt32(txtCredit.Text.Trim());

                GlobalClass.adap.Update(GlobalClass.dt);
                GridView2.EditIndex = -1;
                FillGridView();
                Sum();
            }
            catch
            {
                Response.Write("<script> alert('Record updation fail...') </script>");
            }

            //using (SqlConnection con = new SqlConnection(cnString))
            //{
            //    con.Open();
            //    SqlCommand cmd;
            //    string QRY = "update Party_Ledger set Party_Name=@Name,Naration1=@nar,Invoice=@inv,Credit=@Cre where Voucher_#=@Voc and Party_Name=@Name and Debit='0' ";
            //    cmd = new SqlCommand(QRY, con);
            //    cmd.CommandType = CommandType.Text;
            //    cmd.Parameters.AddWithValue("@Voc", TextBox5.Text);
            //    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
            //    cmd.Parameters.AddWithValue("@nar", txtCountry.Text.Trim());
            //    cmd.Parameters.AddWithValue("@inv", Convert.ToInt32(txtSalary.Text.Trim()));
            //    cmd.Parameters.AddWithValue("@Cre", Convert.ToInt32(txtAmt.Text.Trim()));
            //    cmd.ExecuteNonQuery();
            //    con.Close();
            //}
        }
        protected void RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                FillGridView();
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex].Delete();
                GlobalClass.adap.Update(GlobalClass.dt);
                FillGridView();
                Sum();
            }
            catch
            {
                Response.Write("<script> alert('Record not deleted...') </script>");
            }
        }
        public void txtzero()
        {
            ComboBox1.SelectedIndex = -1;
            //  TextBox5.Text = string.Empty;
            TextBox2.Text = string.Empty;
            TextBox3.Text = string.Empty;
            TextBox4.Text = string.Empty;
            TextBox7.Text = string.Empty;
            TextBox8.Text = string.Empty;
        }
        protected void btninsert_Click(object sender, EventArgs e)
        {
        
            using (SqlConnection con = new SqlConnection(cnString))
            {

                ////////////////////////////////////////////////////////////// PARTY CREDIT
                con.Open();
                string QRY = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration1,Invoice,Debit,Credit,Party_Name) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name)";
                SqlCommand cmd = new SqlCommand(QRY, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@Voc#", TextBox5.Text);
                cmd.Parameters.AddWithValue("@P_AC", TextBox7.Text);
                cmd.Parameters.AddWithValue("@DATE", TextBox1.Text);
                cmd.Parameters.AddWithValue("@V_TYPE", "JV");
                cmd.Parameters.AddWithValue("@Nara1", TextBox2.Text);
                cmd.Parameters.AddWithValue("@INV", TextBox3.Text);
                cmd.Parameters.AddWithValue("@DEBIT", TextBox4.Text);
                cmd.Parameters.AddWithValue("@CREDIT", TextBox8.Text);
                cmd.Parameters.AddWithValue("@P_Name", ComboBox1.Text);
                cmd.ExecuteNonQuery();

      
                con.Close();
                fill_gridview_after_adding();
                txtzero();
                loadcombo_list();
                Sum();
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                string QRY = "delete  FROM Party_Ledger where Voucher_# = '" + TextBox5.Text + "'  ";
                SqlCommand cmd = new SqlCommand(QRY, con);
                cmd.ExecuteNonQuery();
                con.Close();
                txtzero();
                TextBox1.Text = string.Empty;
                code_check();
                FillGridViewEmpty();
            }

        }
        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }
        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            // TextBox5.Focus();
        }
        protected void TextBox5_TextChanged(object sender, EventArgs e)
        {
            //  ComboBox1.Focus();
        }
        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //GridViewRow gr = GridView2.SelectedRow;
            ////  TextBox4.Text = GridView2.Rows[GridView2.SelectedIndex].Cells[0].Text;
            ////    ClientScript.RegisterStartupScript(this.GetType(),"alert","alert('no record')",true);
            //  TextBox4.Text = gr.Cells[0].Text;
            //GridView GridView2 = Page.FindControl("GridView2") as GridView;
            //GridViewRow row = GridView2.SelectedRow;
            //TextBox4.Text = row.Cells[1].Text;

        }
        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
        protected void ComboBox1_Disposed(object sender, EventArgs e)
        {

        }
        protected void ComboBox1_TextChanged(object sender, EventArgs e)
        {


            if (ComboBox1.Text != "")
            {
                SqlConnection con = new SqlConnection(cnString);
                string QRY = "select Account_# from Party_AC where Account_Name='" + ComboBox1.Text + "'";
                SqlCommand cmd = new SqlCommand(QRY, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow abc in dt.Rows)
                {
                    TextBox7.Text = abc["Account_#"].ToString();
                    TextBox2.Focus();
                }
            }
            else
            {
                Response.Write("<script> alert('No Record Found...') </script>");
                ComboBox1.Text = "";
            }

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            //int rowind = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            //  TextBox4.Text = GridView2.Rows[0].Cells[0].Text;
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                string QRY = "select sum(Debit) FROM Party_Ledger where Voucher_# = '" + TextBox5.Text + "'  ";
                string QRY1 = "select sum(Credit) FROM Party_Ledger where Voucher_# = '" + TextBox5.Text + "'  ";

                SqlCommand cmd = new SqlCommand(QRY, con);
                SqlCommand cmd1 = new SqlCommand(QRY1, con);

                Total = Convert.ToString(cmd.ExecuteScalar());
                Total1 = Convert.ToString(cmd1.ExecuteScalar());

                con.Close();
            }

            if (Total !=Total1)
            {
                //  Response.Write("<script> alert('Debit, Credit Should be Equal') </script>");
                //   ClientScript.RegisterStartupScript(GetType(),"alert","alert('Debit, Credit Should be Equal')",true);
            }
            else
            {
                TextBox1.Text = string.Empty;
                TextBox5.Text = string.Empty;
                txtzero();
                code_check();
                FillGridViewEmpty();
            }
           
        }
    }
}