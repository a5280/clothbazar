﻿using HafizBuilder;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HAFIZ_BUILDERS.matrix_admin_bt5_master.html.ltr
{
    public partial class widgets : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string JQueryVer = "1.7.1";
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
            {
                Path = "~/Scripts/jquery-" + JQueryVer + ".min.js",
                DebugPath = "~/Scripts/jquery-" + JQueryVer + ".js",
                CdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + JQueryVer + ".min.js",
                CdnDebugPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + JQueryVer + ".js",
                CdnSupportsSecureConnection = true,
                LoadSuccessExpression = "window.jQuery"
            });
          
            if (!IsPostBack)
            {
                FillGridView();
            }
        }
   
        public void FillGridViewEmpty()
        {
            try
            {
                SqlConnection con=new SqlConnection(@"Data Source=DESKTOP-MSO8RDH;Initial Catalog=DEMO;Integrated Security=True");
                SqlCommand comd = new SqlCommand("select * from cash ",con);
                SqlDataAdapter d = new SqlDataAdapter(comd);
                DataTable dt = new DataTable();

                d.Fill(dt);
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            catch
            {
                Response.Write("<script> alert('Connection String Error...') </script>");
            }
        }
        public void FillGridView()
        {
            try
            {

                string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;
                SqlConnection con = new SqlConnection(cnString);
                GlobalClass.adap = new SqlDataAdapter("select Voc,datetime,Party,DISCRIPTION,INVOICE,Amount from cash ", con);
                SqlCommandBuilder bui = new SqlCommandBuilder(GlobalClass.adap);
                GlobalClass.dt = new DataTable();
                GlobalClass.adap.Fill(GlobalClass.dt);
                GridView1.DataSource = GlobalClass.dt;
                GridView1.DataBind();
                TextBox6.Text = GlobalClass.dt.Compute("Sum(Amount)","").ToString();

            }
            catch
            {
                Response.Write("<script> alert('Connection String Error...') </script>");
            }
        }
        protected void editRecord(object sender, GridViewEditEventArgs e)
        {
            // Get the current row index for edit record
            GridView1.EditIndex = e.NewEditIndex;
            FillGridView();
        }
        protected void cancelRecord(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillGridView();
        }
        protected void AddNewRecord(object sender, EventArgs e)
        {
            try
            {
                if (GlobalClass.dt.Rows.Count > 0)
                {
                    GridView1.EditIndex = -1;
                    GridView1.ShowFooter = true;
                    FillGridView();
                }
                else
                {
                    GridView1.ShowFooter = true;
                    DataRow dr = GlobalClass.dt.NewRow();
                    dr["Party"] = "0";
                    dr["DISCRIPTION"] = "0";
                    dr["INVOICE"] = 0;
                    dr["Amount"] = "0";
                    GlobalClass.dt.Rows.Add(dr);
                    GridView1.DataSource = GlobalClass.dt;
                    GridView1.DataBind();
                    GridView1.Rows[0].Visible = false;
                }
            }
            catch
            {
                Response.Write("<script> alert('Row not added in DataTable...') </script>");
            }
        }

        protected void AddNewCancel(object sender, EventArgs e)
        {
            GridView1.ShowFooter = false;
            FillGridView();
        }

        protected void InsertNewRecord(object sender, EventArgs e)
        {
            try
            {
                string strName = GlobalClass.dt.Rows[0]["Party"].ToString();
                if (strName == "0")
                {
                    GlobalClass.dt.Rows[0].Delete();
                    GlobalClass.adap.Update(GlobalClass.dt);
                }
                TextBox txtName = GridView1.FooterRow.FindControl("txtNewName") as TextBox;
                TextBox txtCountry = GridView1.FooterRow.FindControl("txtNewCountry") as TextBox;
                TextBox txtSalary = GridView1.FooterRow.FindControl("txtNewSalary") as TextBox;
                TextBox txtAmt = GridView1.FooterRow.FindControl("txtNewAmt") as TextBox;

                Guid FileName = Guid.NewGuid();
                DataRow dr = GlobalClass.dt.NewRow();
               
                dr["Party"] = txtName.Text.Trim();
                dr["DISCRIPTION"] = txtCountry.Text.Trim();
                dr["INVOICE"] = txtSalary.Text.Trim();
                dr["Amount"] = txtAmt.Text.Trim();
                GlobalClass.dt.Rows.Add(dr);
                GlobalClass.adap.Update(GlobalClass.dt);
                GridView1.ShowFooter = false;
                FillGridView();
            }
            catch
            {
                Response.Write("<script> alert('Record not added...') </script>");
            }

        }
        protected void updateRecord(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                TextBox txtName = GridView1.Rows[e.RowIndex].FindControl("txtName") as TextBox;
                TextBox txtCountry = GridView1.Rows[e.RowIndex].FindControl("txtCountry") as TextBox;
                TextBox txtSalary = GridView1.Rows[e.RowIndex].FindControl("txtSalary") as TextBox;
                TextBox txtAmt = GridView1.Rows[e.RowIndex].FindControl("txtAmt") as TextBox;
                Guid FileName = Guid.NewGuid();

                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Party"] = txtName.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["DISCRIPTION"] = txtCountry.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["INVOICE"] = Convert.ToInt32(txtSalary.Text.Trim());
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Amount"] = Convert.ToInt32(txtAmt.Text.Trim());
                GlobalClass.adap.Update(GlobalClass.dt);
                GridView1.EditIndex = -1;
                FillGridView();
            }
            catch
            {
                Response.Write("<script> alert('Record updation fail...') </script>");
            }
        }
        protected void RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex].Delete();
                GlobalClass.adap.Update(GlobalClass.dt);
                FillGridView();
            }
            catch
            {
                Response.Write("<script> alert('Record not deleted...') </script>");
            }
        }
        public void txtzero()
        {
            ComboBox1.Text = string.Empty;
            TextBox5.Text = string.Empty;
            TextBox2.Text = string.Empty;
            TextBox3.Text = string.Empty;
            TextBox4.Text = string.Empty;
        }
      
        protected void btninsert_Click(object sender, EventArgs e)
        {
            string query = "insert into cash (datetime,Party,DISCRIPTION,INVOICE,Amount) values('" + TextBox1.Text + "','" + ComboBox1.Text + "','" + TextBox2.Text + "','" + TextBox3.Text + "','" + TextBox4.Text + "')";
            SqlConnection con = new SqlConnection("Data Source=(local);Initial Catalog=DEMO;Integrated Security=True");

            con.Open();
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            con.Close();

         //   Response.Write("<script> alert('Row save...') </script>");
            FillGridView();
            txtzero();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            txtzero();
            TextBox1.Text = string.Empty;
          
        }
        
        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
           // TextBox5.Focus();
        }

        protected void TextBox5_TextChanged(object sender, EventArgs e)
        {
          //  ComboBox1.Focus();
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
    }
}