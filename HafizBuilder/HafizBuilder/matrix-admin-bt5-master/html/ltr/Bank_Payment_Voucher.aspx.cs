﻿using HafizBuilder;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HAFIZ_BUILDERS.matrix_admin_bt5_master.html.ltr
{
    
    public partial class Bank_Payment_Voucher : System.Web.UI.Page
    {
        string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;
        private string PID, Total;
        protected void Page_Load(object sender, EventArgs e)
        {
            string JQueryVer = "1.7.1";
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
            {
                Path = "~/Scripts/jquery-" + JQueryVer + ".min.js",
                DebugPath = "~/Scripts/jquery-" + JQueryVer + ".js",
                CdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + JQueryVer + ".min.js",
                CdnDebugPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + JQueryVer + ".js",
                CdnSupportsSecureConnection = true,
                LoadSuccessExpression = "window.jQuery"
            });

            if (!IsPostBack)
            {
                code_check();
                FillGridViewEmpty();
                loadcombo_list();
                
            }
        }

        public void FillGridViewEmpty()
        {
            try
            {
                //  SqlConnection con = new SqlConnection(@"Data Source=(local);Initial Catalog=Hafiz_Builders;Integrated Security=True");
                SqlConnection con = new SqlConnection(cnString);
                con.Open();
                GlobalClass.adap = new SqlDataAdapter("select * from Party_Ledger where Sr_No=0", con);
                SqlCommandBuilder bui = new SqlCommandBuilder(GlobalClass.adap);
                GlobalClass.dt = new DataTable();
                GlobalClass.adap.Fill(GlobalClass.dt);
                GridView1.DataSource = GlobalClass.dt;
                GridView1.DataBind();
                con.Close();
            }
            catch
            {
                Response.Write("<script> alert('Connection String Error...') </script>");
            }

        }
        public void FillGridView()
        {
            try
            {
                SqlConnection con = new SqlConnection(cnString);
                GlobalClass.adap = new SqlDataAdapter("select Sr_No,Party_Name,Naration1,Invoice,Credit from Party_Ledger where Voucher_# = '" + TextBox5.Text + "' and Credit !=0", con);
                SqlCommandBuilder bui = new SqlCommandBuilder(GlobalClass.adap);
                GlobalClass.dt = new DataTable();
                GlobalClass.adap.Fill(GlobalClass.dt);
                GridView1.DataSource = GlobalClass.dt;
                GridView1.DataBind();
            }
            catch
            {
                Response.Write("<script> alert('Connection String Error...') </script>");
            }
        }
        public void loadcombo_list()
        {
            SqlConnection con = new SqlConnection(cnString);
            using (con)
            {
                SqlCommand cmd = new SqlCommand("select distinct Account_Name from Party_AC where Account_Type!= 'BUILTEN' ", con);
                con.Open();
                ComboBox1.DataSource = cmd.ExecuteReader();
                ComboBox1.DataTextField = "Account_Name";
                ComboBox1.DataBind();
                con.Close();
            }
        }
        public void code_check()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select count(Voucher_#) from Party_Ledger", con);
                int check_null_db = Convert.ToInt32(cmd.ExecuteScalar());

                if (check_null_db != Convert.ToInt32(null))
                {
                    SqlCommand cmd1 = new SqlCommand("select max(Voucher_# + 1) from Party_Ledger", con);
                    int main_code = Convert.ToInt32(cmd1.ExecuteScalar());
                    TextBox5.Text = main_code.ToString();
                }
                else
                {
                    TextBox5.Text = "1";
                }
                con.Close();
            }

        }
        public void fill_gridview_after_adding()
        {
            ///////////////////////////////////Fill Gridview
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter("select Sr_No,Party_Name,Naration1,Invoice,Credit from Party_Ledger where Voucher_# = '" + TextBox5.Text + "' and Credit !=0 ", con);
                DataTable dtt = new DataTable();
                da.Fill(dtt);
                GridView1.DataSource = dtt;
                GridView1.DataBind();
                con.Close();

            }
        }
        public void Sum()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                string QRY = "select sum(Credit) FROM Party_Ledger where Voucher_# = '" + TextBox5.Text + "' and Credit !=0 ";
                SqlCommand cmd = new SqlCommand(QRY, con);
                Total = Convert.ToString(cmd.ExecuteScalar());
                GridView1.FooterRow.Cells[1].Text = "Total Amount";
                GridView1.FooterRow.Cells[4].Text = Total;
                con.Close();
            }
        }
        protected void editRecord(object sender, GridViewEditEventArgs e)
        {
            // Get the current row index for edit record
            GridView1.EditIndex = e.NewEditIndex;
            FillGridView();
        }
        protected void cancelRecord(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            /// FillGridView();
            fill_gridview_after_adding();
            Sum();
        }
        protected void AddNewRecord(object sender, EventArgs e)
        {
            try
            {
                if (GlobalClass.dt.Rows.Count > 0)
                {
                    GridView1.EditIndex = -1;
                    GridView1.ShowFooter = true;
                    FillGridView();
                }
                else
                {
                    GridView1.ShowFooter = true;
                    DataRow dr = GlobalClass.dt.NewRow();
                    dr["Party"] = "0";
                    dr["DISCRIPTION"] = "0";
                    dr["INVOICE"] = 0;
                    dr["Amount"] = "0";
                    GlobalClass.dt.Rows.Add(dr);
                    GridView1.DataSource = GlobalClass.dt;
                    GridView1.DataBind();
                    GridView1.Rows[0].Visible = false;
                }
            }
            catch
            {
                Response.Write("<script> alert('Row not added in DataTable...') </script>");
            }
        }
        protected void AddNewCancel(object sender, EventArgs e)
        {
            GridView1.ShowFooter = false;
            FillGridView();

        }
        protected void InsertNewRecord(object sender, EventArgs e)
        {
            try
            {
                string strName = GlobalClass.dt.Rows[0]["Party"].ToString();
                if (strName == "0")
                {
                    GlobalClass.dt.Rows[0].Delete();
                    GlobalClass.adap.Update(GlobalClass.dt);
                }
                TextBox txtName = GridView1.FooterRow.FindControl("txtNewName") as TextBox;
                TextBox txtCountry = GridView1.FooterRow.FindControl("txtNewCountry") as TextBox;
                TextBox txtSalary = GridView1.FooterRow.FindControl("txtNewSalary") as TextBox;
                TextBox txtAmt = GridView1.FooterRow.FindControl("txtNewAmt") as TextBox;

                Guid FileName = Guid.NewGuid();
                DataRow dr = GlobalClass.dt.NewRow();

                dr["Party"] = txtName.Text.Trim();
                dr["DISCRIPTION"] = txtCountry.Text.Trim();
                dr["INVOICE"] = txtSalary.Text.Trim();
                dr["Amount"] = txtAmt.Text.Trim();
                GlobalClass.dt.Rows.Add(dr);
                GlobalClass.adap.Update(GlobalClass.dt);
                GridView1.ShowFooter = false;
                FillGridView();
            }
            catch
            {
                Response.Write("<script> alert('Record not added...') </script>");
            }
        }
        protected void updateRecord(object sender, GridViewUpdateEventArgs e)
        {
            TextBox txtName = GridView1.Rows[e.RowIndex].FindControl("txtName") as TextBox;
            TextBox txtCountry = GridView1.Rows[e.RowIndex].FindControl("txtCountry") as TextBox;
            TextBox txtSalary = GridView1.Rows[e.RowIndex].FindControl("txtSalary") as TextBox;
            TextBox txtAmt = GridView1.Rows[e.RowIndex].FindControl("txtAmt") as TextBox;
            try
            {
                Guid FileName = Guid.NewGuid();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Party_Name"] = txtName.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Naration1"] = txtCountry.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Invoice"] = Convert.ToInt32(txtSalary.Text.Trim());
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Credit"] = Convert.ToInt32(txtAmt.Text.Trim());
                GlobalClass.adap.Update(GlobalClass.dt);
                GridView1.EditIndex = -1;
                FillGridView();
                Sum();
            }
            catch
            {
                Response.Write("<script> alert('Record updation fail...') </script>");
            }

            using (SqlConnection con = new SqlConnection(cnString))
            {

                con.Open();
                SqlCommand cmd;
                string QRY = "update Party_Ledger set Party_Name=@Name,Naration1=@nar,Invoice=@inv,Debit=@Cre where Voucher_#=@Voc and Party_Name=@Name and Credit='0' ";
                cmd = new SqlCommand(QRY, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@Voc", TextBox5.Text);
                cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                cmd.Parameters.AddWithValue("@nar", txtCountry.Text.Trim());
                cmd.Parameters.AddWithValue("@inv", Convert.ToInt32(txtSalary.Text.Trim()));
                cmd.Parameters.AddWithValue("@Cre", Convert.ToInt32(txtAmt.Text.Trim()));
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        protected void RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                FillGridView();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex].Delete();
                GlobalClass.adap.Update(GlobalClass.dt);
                FillGridView();
                Sum();


            }
            catch
            {
                Response.Write("<script> alert('Record not deleted...') </script>");
            }
        }
        public void txtzero()
        {
            ComboBox1.SelectedIndex = -1;
            //  TextBox5.Text = string.Empty;
            TextBox2.Text = string.Empty;
            TextBox3.Text = string.Empty;
            TextBox4.Text = string.Empty;
            TextBox7.Text = string.Empty;

        }
        protected void btninsert_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                if (RadioButton2.Checked == true)
                {


                    ////////////////////////////////////////////////////////////// PARTY CREDIT
                    con.Open();
                    string QRY = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration1,Invoice,Debit,Credit,Party_Name) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name)";
                    SqlCommand cmd = new SqlCommand(QRY, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@Voc#", TextBox5.Text);
                    cmd.Parameters.AddWithValue("@P_AC", TextBox7.Text);
                    cmd.Parameters.AddWithValue("@DATE", TextBox1.Text);
                    cmd.Parameters.AddWithValue("@V_TYPE", "CPV");
                    cmd.Parameters.AddWithValue("@Nara1", TextBox2.Text);
                    cmd.Parameters.AddWithValue("@INV", TextBox3.Text);
                    cmd.Parameters.AddWithValue("@DEBIT", TextBox4.Text);
                    cmd.Parameters.AddWithValue("@CREDIT", 0);
                    cmd.Parameters.AddWithValue("@P_Name", ComboBox1.Text);
                    cmd.ExecuteNonQuery();

                    ////////////////////////////////////////////////////////// CASH DEBIT
                    string QRY1 = "select Account_# FROM Party_AC where Account_Name='CASH'";
                    cmd = new SqlCommand(QRY1, con);
                    SqlDataAdapter sa = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    sa.Fill(dt);
                    foreach (DataRow item in dt.Rows)
                    {
                        PID = item["Account_#"].ToString();
                    }
                    string QRY2 = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration1,Invoice,Debit,Credit,Party_Name) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name)";
                    SqlCommand cmd1 = new SqlCommand(QRY2, con);
                    cmd1.CommandType = CommandType.Text;
                    cmd1.Parameters.AddWithValue("@Voc#", TextBox5.Text);
                    cmd1.Parameters.AddWithValue("@P_AC", PID);
                    cmd1.Parameters.AddWithValue("@DATE", TextBox1.Text);
                    cmd1.Parameters.AddWithValue("@V_TYPE", "CPV");
                    cmd1.Parameters.AddWithValue("@Nara1", TextBox2.Text);
                    cmd1.Parameters.AddWithValue("@INV", TextBox3.Text);
                    cmd1.Parameters.AddWithValue("@DEBIT", 0);
                    cmd1.Parameters.AddWithValue("@CREDIT", TextBox4.Text);
                    cmd1.Parameters.AddWithValue("@P_Name", ComboBox1.Text);
                    cmd1.ExecuteNonQuery();



                    con.Close();
                    fill_gridview_after_adding();
                    txtzero();
                    loadcombo_list();
                    Sum();
                }
                else if (RadioButton1.Checked == true)
                {
                    {
                        ////////////////////////////////////////////////////////////// PARTY CREDIT
                        con.Open();
                        string QRY = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration1,Invoice,Debit,Credit,Party_Name,Chq#,Chq_Date,Un_Present) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name,@Chq#,@Chq_Date,@Pre)";
                        SqlCommand cmd = new SqlCommand(QRY, con);
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@Voc#", TextBox5.Text);
                        cmd.Parameters.AddWithValue("@P_AC", TextBox7.Text);
                        cmd.Parameters.AddWithValue("@DATE", TextBox1.Text);
                        cmd.Parameters.AddWithValue("@V_TYPE", "CPV_Chq");
                        cmd.Parameters.AddWithValue("@Nara1", TextBox2.Text);
                        cmd.Parameters.AddWithValue("@INV", TextBox3.Text);
                        cmd.Parameters.AddWithValue("@DEBIT", TextBox4.Text);
                        cmd.Parameters.AddWithValue("@CREDIT", 0);
                        cmd.Parameters.AddWithValue("@P_Name", ComboBox1.Text);
                        cmd.Parameters.AddWithValue("@Chq#", TextBox8.Text);
                        cmd.Parameters.AddWithValue("@Chq_Date", TextBox9.Text);
                        cmd.Parameters.AddWithValue("@Pre", "Un Present Cheque");
                        cmd.ExecuteNonQuery();

                        ////////////////////////////////////////////////////////// CASH DEBIT
                        string QRY1 = "select Account_# FROM Party_AC where Account_Name='CASH'";
                        cmd = new SqlCommand(QRY1, con);
                        SqlDataAdapter sa = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        sa.Fill(dt);
                        foreach (DataRow item in dt.Rows)
                        {
                            PID = item["Account_#"].ToString();
                        }
                        string QRY2 = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration1,Invoice,Debit,Credit,Party_Name,Chq#,Chq_Date,Un_Present) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name,@Chq#,@Chq_Date,@Pre)";
                        SqlCommand cmd1 = new SqlCommand(QRY2, con);
                        cmd1.CommandType = CommandType.Text;
                        cmd1.Parameters.AddWithValue("@Voc#", TextBox5.Text);
                        cmd1.Parameters.AddWithValue("@P_AC", PID);
                        cmd1.Parameters.AddWithValue("@DATE", TextBox1.Text);
                        cmd1.Parameters.AddWithValue("@V_TYPE", "CPV_Chq");
                        cmd1.Parameters.AddWithValue("@Nara1", TextBox2.Text);
                        cmd1.Parameters.AddWithValue("@INV", TextBox3.Text);
                        cmd1.Parameters.AddWithValue("@DEBIT", 0);
                        cmd1.Parameters.AddWithValue("@CREDIT", TextBox4.Text);
                        cmd1.Parameters.AddWithValue("@P_Name", ComboBox1.Text);
                        cmd1.Parameters.AddWithValue("@Chq#", TextBox8.Text);
                        cmd1.Parameters.AddWithValue("@Chq_Date", TextBox9.Text);
                        cmd1.Parameters.AddWithValue("@Pre", "Un Present Cheque");
                        cmd1.ExecuteNonQuery();

                        con.Close();
                        fill_gridview_after_adding();
                        txtzero();
                        loadcombo_list();
                        Sum();
                        TextBox8.Text = string.Empty;
                        TextBox9.Text = string.Empty;

                    }
                }

            }
        }
        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }
        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            // TextBox5.Focus();
        }
        protected void TextBox5_TextChanged(object sender, EventArgs e)
        {
            //  ComboBox1.Focus();
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //GridViewRow gr = GridView1.SelectedRow;
            ////  TextBox4.Text = GridView1.Rows[GridView1.SelectedIndex].Cells[0].Text;
            ////    ClientScript.RegisterStartupScript(this.GetType(),"alert","alert('no record')",true);
            //  TextBox4.Text = gr.Cells[0].Text;
            //GridView GridView1 = Page.FindControl("GridView1") as GridView;
            //GridViewRow row = GridView1.SelectedRow;
            //TextBox4.Text = row.Cells[1].Text;

        }
        protected void ComboBox1_TextChanged(object sender, EventArgs e)
        {
            if (ComboBox1.Text != "")
            {
                SqlConnection con = new SqlConnection(cnString);
                string QRY = "select Account_# from Party_AC where Account_Name='" + ComboBox1.Text + "'";
                SqlCommand cmd = new SqlCommand(QRY, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow abc in dt.Rows)
                {
                    TextBox7.Text = abc["Account_#"].ToString();
                    TextBox2.Focus();
                }
            }
            else
            {
                Response.Write("<script> alert('No Record Found...') </script>");
                ComboBox1.Text = "";
            }

        }
        protected void Button3_Click(object sender, EventArgs e)
        {
            //int rowind = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            //  TextBox4.Text = GridView1.Rows[0].Cells[0].Text;
        }
        protected void Button5_Click(object sender, EventArgs e)
        {
            TextBox1.Text = string.Empty;
            TextBox5.Text = string.Empty;
            txtzero();
            code_check();
            FillGridViewEmpty();
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                string QRY = "delete  FROM Party_Ledger where Voucher_# = '" + TextBox5.Text + "'  ";
                SqlCommand cmd = new SqlCommand(QRY, con);
                cmd.ExecuteNonQuery();
                con.Close();
                txtzero();
                TextBox1.Text = string.Empty;
                code_check();
                FillGridViewEmpty();
            }

        }

        protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            //if (RadioButton1.Enabled == true)
            //{
            //    TextBox9.Visible = true;
            //    TextBox8.Visible = true;
            //    Label9.Visible = true;
            //    Label10.Visible = true;
            //    ImageButton1.Visible = true;
            //    RadioButton2.Checked = false;
            //}


        }

        protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            //if (RadioButton2.Enabled == true)
            //{
            //    TextBox9.Visible = false;
            //    TextBox8.Visible = false;
            //    Label9.Visible = false;
            //    Label10.Visible = false;
            //    ImageButton1.Visible = false;
            //    RadioButton1.Checked = false;
            //}
        }

        protected void TextBox8_TextChanged(object sender, EventArgs e)
        {

        }
    }
}