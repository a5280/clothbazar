﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace HafizBuilder.matrix_admin_bt5_master.html.ltr
{
    public partial class Project : System.Web.UI.Page
    {
        string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;
        DataTable dtt = new DataTable();
        private string PID,PID2, Total;
        int inc = 0, Total2=0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (ViewState["Cash"] == null)
                {
                    dtt.Columns.Add("Project_#");
                    dtt.Columns.Add("Project Name");
                    dtt.Columns.Add("Naration");
                    
                  
                    ViewState["Cash"] = dtt;
                    dtt = (DataTable)ViewState["Cash"];
                    dtt.Rows.Add(" No record available");
                    gridview.DataSource = dtt;
                    gridview.DataBind();

                    GlobalClass.id = 0;
                }
              
                code_check();
                loadcombo_list();
             
            }
        }
        protected void Button4_Click(object sender, EventArgs e)
        {
            inc++;
            Total2 = inc + Convert.ToInt32(TextBox5.Text);

            if (GlobalClass.id == 0)
            {
                dtt = (DataTable)ViewState["Cash"];
                dtt.Rows.RemoveAt(0);
                gridview.DataSource = dtt;
                gridview.DataBind();
                GlobalClass.id = 1;
            }

            dtt = (DataTable)ViewState["Cash"];

            dtt.Rows.Add(TextBox5.Text,TextBox2.Text, TextBox6.Text);
            gridview.DataSource = dtt;
            gridview.DataBind();
            TextBox6.Text = string.Empty;

            TextBox2.Text = "";
            TextBox5.Text = "";
            TextBox6.Text = string.Empty;
            TextBox2.Focus();

            TextBox5.Text = Total2.ToString();
        }
        public void nullvalue()
        {
            gridview.DataSource = null;
            gridview.DataBind();
        }
        public void record()
        {
            dtt.Columns.Add("Project_#");
            dtt.Columns.Add("Project Name");
            dtt.Columns.Add("Naration");
            ViewState["Cash"] = dtt;
            dtt = (DataTable)ViewState["Cash"];
            dtt.Rows.Add(" No record available");
            gridview.DataSource = dtt;
            gridview.DataBind();

        }
        protected void Button6_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                for (int i = 0; i < gridview.Rows.Count; i++)
                {
                    con.Open();
                    string QRY = "INSERT INTO Project (Project_#,Project_Name,Naration) VALUES (@Pro#,@Pro_Na,@Cata)";
                    SqlCommand cmd2 = new SqlCommand(QRY, con);
                    cmd2.CommandType = CommandType.Text;
                    cmd2.Parameters.AddWithValue("@Pro#",gridview.Rows[i].Cells[1].Text);
                    cmd2.Parameters.AddWithValue("@Pro_Na", gridview.Rows[i].Cells[2].Text);
                    cmd2.Parameters.AddWithValue("@Cata", gridview.Rows[i].Cells[3].Text);      
                              
                    cmd2.ExecuteNonQuery();

                    con.Close();               
                }
                loadcombo_list();
                code_check();
            }
            nullvalue();
            record();
            gridview.Rows[0].Cells[1].ColumnSpan = 2;
            gridview.Rows[0].Cells.RemoveAt(0);
            TextBox6.Text = string.Empty;
            TextBox2.Text = "";
            GlobalClass.id = 0;

        }
        protected void grid_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (!IsPostBack)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[1].ColumnSpan = 2;
                    //now make up for the colspan from cell2
                    e.Row.Cells.RemoveAt(0);
                }
            }

        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            int rowid = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            TextBox5.Text = gridview.Rows[rowid].Cells[1].Text;
            TextBox2.Text = gridview.Rows[rowid].Cells[2].Text;
            TextBox6.Text = gridview.Rows[rowid].Cells[3].Text;
          

            dtt = (DataTable)ViewState["Cash"];
            dtt.Rows.RemoveAt(rowid);
            gridview.DataSource = dtt;
            gridview.DataBind();
       
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            dtt = (DataTable)ViewState["Cash"];
            int rowid = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            dtt.Rows.RemoveAt(rowid);
            gridview.DataSource = dtt;
            gridview.DataBind();

            if (gridview.Rows.Count == 0)
            {
                ViewState["Cash"] = dtt;
                dtt = (DataTable)ViewState["Cash"];

                dtt.Rows.Add(" No record available");
                gridview.DataSource = dtt;
                gridview.DataBind();
                gridview.Rows[0].Cells[1].ColumnSpan = 2;
                gridview.Rows[0].Cells.RemoveAt(0);
                GlobalClass.id = 0;
            }
          
        }

        public void loadcombo_list()
        {
            //SqlConnection con = new SqlConnection(cnString);
            //using (con)
            //{
            //    SqlCommand cmd = new SqlCommand("select distinct Catagory from Product ", con);
            //    con.Open();
            //    TextBox6.DataSource = cmd.ExecuteReader();
            //    TextBox6.DataTextField = "Catagory";
            //    TextBox6.DataBind();
            //    con.Close();
            //}
        }
        public void code_check()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select count(Project_#) from Project", con);
                int check_null_db = Convert.ToInt32(cmd.ExecuteScalar());

                if (check_null_db != Convert.ToInt32(null))
                {
                    SqlCommand cmd1 = new SqlCommand("select max(Project_# + 1) from Project", con);
                    int main_code = Convert.ToInt32(cmd1.ExecuteScalar());
                    TextBox5.Text = main_code.ToString();
                }
                else
                {
                    TextBox5.Text = "1";
                }
                con.Close();
            }

        }    
        public void txtzero()
        {
            TextBox6.Text = string.Empty;
            TextBox5.Text = string.Empty;
            TextBox2.Text = string.Empty;
          

        }
        protected void Button1_Click(object sender, EventArgs e)
        {

            nullvalue();
            record();
            gridview.Rows[0].Cells[1].ColumnSpan = 2;
            gridview.Rows[0].Cells.RemoveAt(0);
            txtzero();
            code_check();
            GlobalClass.id = 0;

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                string QRY = "DELETE FROM Project WHERE  Project_#='" + TextBox5.Text + "' ";
                SqlCommand cmd = new SqlCommand(QRY, con);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            Button1_Click(Button1, null);
            code_check();
           
        }
        protected void Button7_Click(object sender, EventArgs e)
        {
            if ( TextBox5.Text !="")
            {

                using (SqlConnection con = new SqlConnection(cnString))
                {
                   

                    ViewState["voc_no"] = TextBox5.Text;
                    for (int i = 0; i < gridview.Rows.Count; ++i)
                    {
                        con.Open();
                        string QRYQ = "DELETE FROM Project WHERE Project_# ='" + gridview.Rows[i].Cells[1].Text + "' ";
                        SqlCommand cmd = new SqlCommand(QRYQ, con);
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                        con.Close();

                        con.Open();
                        string QRY = "INSERT INTO Project (Project_#,Project_Name,Naration) VALUES (@Pro#,@Pro_Na,@Cata)";
                        SqlCommand cmd2 = new SqlCommand(QRY, con);
                        cmd2.CommandType = CommandType.Text;
                        cmd2.Parameters.AddWithValue("@Pro#", gridview.Rows[i].Cells[1].Text);
                        cmd2.Parameters.AddWithValue("@Pro_Na", gridview.Rows[i].Cells[2].Text);
                        cmd2.Parameters.AddWithValue("@Cata", gridview.Rows[i].Cells[3].Text);
                        cmd2.ExecuteNonQuery();

                        con.Close();
                      

                    }
                  
                    nullvalue();
                    record();
                    gridview.Rows[0].Cells[1].ColumnSpan = 2;
                    gridview.Rows[0].Cells.RemoveAt(0);
                    txtzero();
                    code_check();
                    GlobalClass.id = 0;
                    loadcombo_list();
                    code_check();
                }
         
            }
        }
        public void search()
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {
                ViewState["voc_no"] = TextBox5.Text;
                string QRY = "SELECT Project_#,Project_Name,Naration from Project  WHERE Project_#= '" + TextBox5.Text + "' ";
                SqlCommand cmd = new SqlCommand(QRY, CON);
                SqlDataAdapter DA = new SqlDataAdapter(cmd);
                DataTable DT = new DataTable();
                DA.Fill(DT);

                dtt.Columns.Add("Project_#");
                dtt.Columns.Add("Project Name");
                dtt.Columns.Add("Naration");
                ViewState["Cash"] = dtt;
                dtt = (DataTable)ViewState["Cash"];

                foreach (DataRow item in DT.Rows)
                {
                    
                    dtt.Rows.Add(item["Project_#"].ToString(), item["Project_Name"].ToString(), item["Naration"].ToString());
                    gridview.DataSource = dtt;
                    gridview.DataBind();
                    GlobalClass.id = 1;
                }
              
            }
            
        }
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            search();
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            
        }

    }
}