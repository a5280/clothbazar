﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace HAFIZ_BUILDERS.matrix_admin_bt5_master.html.ltr
{
    public partial class Account_Name_Update : System.Web.UI.Page
    {
        string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                   loadcombo_list();
            }
        }

        public void loadcombo_list()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                SqlCommand cmd = new SqlCommand("select Account_Name from Party_AC", con);
                con.Open();

                ComboBox1.DataSource = cmd.ExecuteReader();
                ComboBox1.DataTextField = "Account_Name";
                ComboBox1.DataBind();
                con.Close();

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {
                SqlCommand cmd,cmd1;
                CON.Open();
                string QRY = "update Party_AC set Account_Name=@Name where Account_Name=@AN ";
                string QRY1 = "update Party_Ledger set Party_Name=@Name where Party_Name=@AN ";

                cmd = new SqlCommand(QRY, CON);
                cmd1 = new SqlCommand(QRY1, CON);

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@Name", TextBox1.Text);
                cmd.Parameters.AddWithValue("@AN", ComboBox1.Text);

                cmd1.CommandType = CommandType.Text;
                cmd1.Parameters.AddWithValue("@Name", TextBox1.Text);
                cmd1.Parameters.AddWithValue("@AN", ComboBox1.Text);

                cmd.ExecuteNonQuery();
                cmd1.ExecuteNonQuery();
                CON.Close();

                TextBox1.Text = string.Empty;
                ComboBox1.SelectedIndex = -1;
            }
        }
    }
}