﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace HafizBuilder.matrix_admin_bt5_master.html.ltr
{
    public partial class Date_form : System.Web.UI.Page
    {
        string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;
        DataTable dtt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                loadcombo_list();
            }
        }
        public void loadcombo_list()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                SqlCommand cmd = new SqlCommand("select distinct Account_Name from Party_AC where Account_Type!= 'BUILTEN' ", con);
                con.Open();
                ComboBox2.DataSource = cmd.ExecuteReader();
                ComboBox2.DataTextField = "Account_Name";
                ComboBox2.DataBind();
                con.Close();
            }
        }
        protected void Button4_Click(object sender, EventArgs e)
        {
          
        }
    
        protected void Button6_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                /////////////////////////////////////////// Cashbook Reprots
                if (ComboBox1.Text == "Cash Book")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','CASH')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();

                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "SS";
                    Response.Redirect("ReportView.aspx");
                }

                /////////////////////////////////////////// Ledger Reprots
                if (ComboBox1.Text == "Party Ledger")
                {
                    // ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('" +ad  + "');", true);
                    //  Response.Write("<script>alert('Data inserted successfully')</script>");

                    //opbal();
                    //con.Open();
                    //SqlCommand cmd = new SqlCommand("select isnull(sum(debit),0)-isnull(sum(credit),0) as balance from party_ledger where party_name = '" + ComboBox2.Text + "' and date < '" + TextBox2.Text + "'", con);
                    //SqlDataAdapter da = new SqlDataAdapter(cmd);
                    //DataTable dt = new DataTable();
                    //da.Fill(dt);

                    //string QRY11 = "select Account_# FROM Party_AC where Account_Name='" + ComboBox2.Text + "' ";
                    //SqlCommand cmd11 = new SqlCommand(QRY11, con);
                    //SqlDataAdapter saa = new SqlDataAdapter(cmd11);
                    //DataTable dttt = new DataTable();
                    //saa.Fill(dttt);
                    //foreach (DataRow item1 in dttt.Rows)
                    //{
                    //    PID2 = item1["Account_#"].ToString();
                    //}

                    //foreach (DataRow item in dt.Rows)
                    //{
                    //    if (item["balance"].ToString() != "0")
                    //    {
                    //        if (Convert.ToDouble(item["balance"]) > 0)
                    //        {
                    //            SqlCommand cmdins = new SqlCommand("Insert into party_ledger(voucher_#,p_account_#,date,naration1,debit,credit,party_name,rose)values(0,'" + PID2 + "','" + TextBox2.Text + "','Opening Balance','" + item["balance"] + "',0,'" + ComboBox2.Text + "',1)", con);
                    //            cmdins.ExecuteNonQuery();
                    //        }
                    //        else if (Convert.ToDouble(item["balance"]) < 0)
                    //        {
                    //            SqlCommand cmdins1 = new SqlCommand("Insert into party_ledger(voucher_#,p_account_#,date,naration1,debit,credit,party_name,rose)values(0,'" + PID2 + "','" + TextBox2.Text + "','Opening Balance',0,'" + Math.Abs(Convert.ToDouble(item["balance"])) + "','" + ComboBox2.Text + "',1)", con);
                    //            cmdins1.ExecuteNonQuery();
                    //        }
                    //    }
                    //}
                    //con.Close();


                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();

                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox2.Text;
                    Session["etype2"] = ComboBox1.Text;
                    Response.Redirect("ReportView.aspx");
                }

                /////////////////////////////////////////// JV Reprots
                else if (ComboBox1.Text == "Journal Voucher")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','JV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();

                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }

                /////////////////////////////////////////// Cash Reprots
                else if (ComboBox1.Text == "Cash Receipt Voucher")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','CRV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();

                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Cash Payment Voucher")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','CPV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Bank Receipt Voucher")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','BRV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Bank Payment Voucher")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','BPV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }

                /////////////////////////////////////////// Purchase Reprots
                else if (ComboBox1.Text == "Purchase Party")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','PV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                     
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Purchase Product")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','PV')", con);
                    cmd2.ExecuteNonQuery(); 
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Purchase Date")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','PV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Purchase Project")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','PV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }

                /////////////////////////////////////////// Purchase Return Reprots

                else if (ComboBox1.Text == "Purchase Return Party")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','PRV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();

                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Purchase Return Product")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','PRV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Purchase Return Date")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','PRV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Purchase Return Project")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','PRV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }

                /////////////////////////////////////////// Sale Reprots
               else if (ComboBox1.Text == "Sale Party")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','SV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();

                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Sale Product")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','SV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Sale Date")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','SV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Sale Project")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','SV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }

                /////////////////////////////////////////// Sale Return Reprots

                else if (ComboBox1.Text == "Sale Return Party")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','SRV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();

                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Sale Return Product")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','SRV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Sale Return Date")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','SRV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }
                else if (ComboBox1.Text == "Sale Return Project")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','SRV')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }

                else if (ComboBox1.Text == "Trial_Balance")
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete from t_date", con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd2 = new SqlCommand("Insert into t_Date(date,date2,etype)values('" + TextBox2.Text + "','" + TextBox3.Text + "','Trial_Balance')", con);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    Session["date1"] = TextBox2.Text;
                    Session["date2"] = TextBox3.Text;
                    Session["etype"] = ComboBox1.Text;
                    Session["etype2"] = "ss";
                    Response.Redirect("ReportView.aspx");
                }

            }
               
         }
        public void DoMyOnClickCall(object sender, EventArgs e)
        {
           
        }
        protected void grid_RowCreated(object sender, GridViewRowEventArgs e)
        {
           

        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            
       
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
           
          
        }

        public void code_check()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select count(User_ID) from User_AC", con);
                int check_null_db = Convert.ToInt32(cmd.ExecuteScalar());

                if (check_null_db != Convert.ToInt32(null))
                {
                    SqlCommand cmd1 = new SqlCommand("select max(User_ID + 1) from User_AC", con);
                    int main_code = Convert.ToInt32(cmd1.ExecuteScalar());
                    TextBox1.Text = main_code.ToString();
                }
                else
                {
                    TextBox1.Text = "1";
                }
                con.Close();
            }

        }    
        public void txtzero()
        {
            TextBox1.Text = string.Empty;
            TextBox2.Text = string.Empty;
            TextBox3.Text = string.Empty;
            ComboBox1.SelectedIndex = -1;


        }
        protected void Button1_Click(object sender, EventArgs e)
        {

            txtzero();
            code_check();

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                string QRY = "DELETE FROM User_AC WHERE User_ID='" + TextBox1.Text + "' ";
                SqlCommand cmd = new SqlCommand(QRY, con);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            txtzero();
            code_check();
        }
        protected void Button7_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text != "")
            {
                using (SqlConnection con = new SqlConnection(cnString))
                {
                    con.Open();
                    string QRY1 = "DELETE FROM User_AC WHERE User_ID='" + TextBox1.Text + "' ";
                    SqlCommand cmd = new SqlCommand(QRY1, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    con.Close();

                    con.Open();
                    string QRY = "INSERT INTO User_AC (User_ID,Username,Password,Role) VALUES (@User_Id,@name,@pass,@role)";
                    SqlCommand cmd2 = new SqlCommand(QRY, con);
                    cmd2.CommandType = CommandType.Text;
                    cmd2.Parameters.AddWithValue("@User_Id", TextBox1.Text);
                    cmd2.Parameters.AddWithValue("@name", TextBox2.Text);
                    cmd2.Parameters.AddWithValue("@pass", TextBox3.Text);
                    cmd2.Parameters.AddWithValue("@role", ComboBox1.Text);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    txtzero();
                }
            }
            txtzero();
            code_check();
        }
        string ab;
        public void search()
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {
                string QRY = "SELECT User_ID,Username,Password,Role from User_AC  WHERE User_ID = '" + TextBox1.Text + "' ";
                SqlCommand cmd = new SqlCommand(QRY, CON);
                SqlDataAdapter DA = new SqlDataAdapter(cmd);
                DataTable DT = new DataTable();
                DA.Fill(DT);
                
                foreach (DataRow item in DT.Rows)
                {
                    TextBox2.Text = item["Username"].ToString();
                    TextBox3.Text = item["Password"].ToString();
                     ab= item["Role"].ToString();
                    if (ab=="Admin")
                    {
                        ComboBox1.Text = "Admin";
                    }
                    else
                    {
                        ComboBox1.Text = "User";
                    }
                }
               
            }
        }
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            search();
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            
        }
        string PID2;
        private void opbal()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {

                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('" +ComboBox2.Text + "');", true);

                con.Open();
                SqlCommand cmd = new SqlCommand("select isnull(sum(debit),0)-isnull(sum(credit),0) as balance from party_ledger where party_name = '" + ComboBox2.Text + "' and date < '" + TextBox2.Text + "'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                string QRY11 = "select Account_# FROM Party_AC where Account_Name='" + ComboBox2.Text + "' ";
                SqlCommand cmd11 = new SqlCommand(QRY11, con);
                SqlDataAdapter saa = new SqlDataAdapter(cmd11);
                DataTable dttt = new DataTable();
                saa.Fill(dttt);
                foreach (DataRow item1 in dttt.Rows)
                {
                    PID2 = item1["Account_#"].ToString();
                }

                foreach (DataRow item in dt.Rows)
                {
                    if (item["balance"].ToString() != "0")
                    {
                        if (Convert.ToDouble(item["balance"]) > 0)
                        {
                            SqlCommand cmdins = new SqlCommand("Insert into party_ledger(voucher_#,p_account_#,date,naration1,debit,credit,party_name,rose)values(0,'" + PID2 + "','" + TextBox2.Text + "','Opening Balance','" + item["balance"] + "',0,'" + ComboBox2.Text + "',1)", con);
                            cmdins.ExecuteNonQuery();
                        }
                        else if (Convert.ToDouble(item["balance"]) < 0)
                        {
                            SqlCommand cmdins1 = new SqlCommand("Insert into party_ledger(voucher_#,p_account_#,date,naration1,debit,credit,party_name,rose)values(0,'" + PID2 + "','" + TextBox2.Text + "','Opening Balance',0,'" + Math.Abs(Convert.ToDouble(item["balance"])) + "','" + ComboBox2.Text + "',1)", con);
                            cmdins1.ExecuteNonQuery();
                        }
                    }
                }
                con.Close();
            }
        }

      

        protected void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBox1.SelectedIndex == 0)
            {
                ComboBox2.Enabled = true;                
            }
            else
            {
                ComboBox2.Enabled = false;
            }
        }

        protected void ComboBox1_TextChanged(object sender, EventArgs e)
        {
           
        }
    }
}