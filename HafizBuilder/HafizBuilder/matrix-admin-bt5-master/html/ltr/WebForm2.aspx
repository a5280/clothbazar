﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="HAFIZ_BUILDERS.matrix_admin_bt5_master.html.ltr.WebForm2" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                 <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
        <br />
        <asp:GridView ID="GridView1" runat="server" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="12pt" OnRowCreated="grid_RowCreated" CellPadding="4" GridLines="None" ForeColor="#333333" CausesValidation="False" ShowHeaderWhenEmpty="True" Width="80%" >
           
            
            

             <EditRowStyle BackColor="#7C6F57" HorizontalAlign="Center" />
             <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
             <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
             <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
           
             <RowStyle BackColor="#E3EAEB" HorizontalAlign="Center" />
             <AlternatingRowStyle BackColor="White"/>
           
             <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
             <SortedAscendingCellStyle BackColor="#F8FAFA" />
             <SortedAscendingHeaderStyle BackColor="#246B61" />
             <SortedDescendingCellStyle BackColor="#D4DFE1" />
             <SortedDescendingHeaderStyle BackColor="#15524A" />
          

              <Columns >
               <asp:TemplateField HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Right"  >
                <HeaderTemplate>Operation</HeaderTemplate>
                <ItemTemplate>
                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Images/edit-regular.png"  OnClick="btnEdit_Click" Width="20px" Height="20px" CssClass="m-l-0" />
                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/Images/trash-alt-regular.png"  OnClick="btnDelete_Click" Width="20px" Height="20px" />                          
                </ItemTemplate>                   

                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
            </asp:TemplateField>      
            </Columns>
        </asp:GridView>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Add Row" CausesValidation="False" />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Save" />
    
                 <asp:Button ID="Button3" runat="server" CausesValidation="False" OnClick="Button3_Click" Text="Add Row" />
    
            </ContentTemplate>
        </asp:UpdatePanel>
       
    </div>
    </form>
</body>
</html>
