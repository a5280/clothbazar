﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cash_Payment_Voucher.aspx.cs" EnableEventValidation="false" Inherits="HafizBuilder.matrix_admin_bt5_master.html.ltr.Cash_Payment_Voucher" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html>

<html dir="ltr" lang="en">

<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, Tech No Hub lite admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, Tech No Hub lite design, Tech No Hub lite dashboard bootstrap 5 dashboard template">
    <meta name="description"
        content="Tech No Hub Lite Free Version is powerful and clean admin dashboard template, inpired from Bootstrap Framework">
    <meta name="robots" content="noindex,nofollow">
     <title>Hafiz Builder</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">

    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        
           <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>


<![endif]-->
  
    <style>

         @media screen and (min-width:0px) and (max-width: 540px) {
           .txt {
                width:auto;
                height: 29px;
                margin: 0 0 0 20px;
            }
            #GridView1{
                width:10%;
            }
            .smtxtsize{
                width:100%;
                height:auto;
            }
        }
        @media screen and (min-width:541px) and (max-width: 1366px) {
            .txt {
                width: 15%;
                height: 29px;
                margin: 0 0 0 20px;
            }
            #GridView1{
                width:95%;
            }
        }
        
        .auto-style4 {
            margin-bottom: 0px;
        }
                
        .auto-style88 {
            height: 31px;
            width: 4%;
        }
                
        .auto-style91 {
            width: 100%;
        }
        .auto-style101 {
            width: 4%;
            height: 23px;
        }
        .auto-style102 {
            width: 164px;
        }
        .auto-style107 {
            width: 56px;
        }
        .auto-style108 {
            width: 98px;
        }
        
        .auto-style111 {
            width: 62px;
        }
        .auto-style112 {
            width: 217px;
        }
        .auto-style113 {
            width: 52px;
        }
        .auto-style114 {
            width: 152px;
        }
        .auto-style115 {
            width: 124px;
        }
        .auto-style116 {
            width: 58px;
        }
        
        .auto-style121 {
            width: 118px;
        }
        .auto-style122 {
            width: 121px;
        }
        .auto-style124 {
            width: 104px;
        }
        .auto-style126 {
            width: 55px;
        }
        .auto-style128 {
            width: 145px;
        }
        
        .auto-style129 {
            width: 64px;
        }
        .auto-style132 {
            width: 135px;
        }
        .auto-style133 {
            width: 130px;
        }
        
    </style>

   <%--   <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="<%= Page.ResolveUrl("~/js/jquery-1.11.1.js") %>"></script>--%>


</head>

<body >
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
     <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b class="logo-icon ps-2">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="../../assets/images/logo-icon.png" alt="homepage" class="light-logo" />

                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text">
                            <!-- dark Logo text -->
                           
                            <span>Hafiz Builder</span>

                        </span>
                        <!-- Logo icon -->
                        <!-- <b class="logo-icon"> -->
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                        <!-- Dark Logo icon -->
                        <!-- <img src="../../assets/images/logo-text.png" alt="homepage" class="light-logo" /> -->

                        <!-- </b> -->
                        <!--End Logo icon -->
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-start me-auto">
                        <li class="nav-item d-none d-lg-block"><a
                                class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)"
                                data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- create new -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <span class="d-none d-md-block">Create New User <i class="fa fa-angle-down"></i></span>
                                <span class="d-block d-md-none"><i class="fa fa-plus"></i></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="Create_User.aspx">ADD User</a></li>
                              
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search position-absolute">
                                <input type="text" class="form-control" placeholder="Search &amp; enter"> <a
                                    class="srh-btn"><i class="ti-close"></i></a>
                            </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-end">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-bell font-24"></i>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" id="2" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                 <i class="font-24 mdi mdi-comment-processing"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end mailbox animated bounceInDown" aria-labelledby="2">
                                <ul class="list-style-none">
                                    <li>
                                        <div class="">
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-success btn-circle"><i
                                                            class="ti-calendar"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Event today</h5>
                                                        <span class="mail-desc">Just a reminder that event</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-info btn-circle"><i
                                                            class="ti-settings"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Settings</h5>
                                                        <span class="mail-desc">You can customize this template</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-primary btn-circle"><i
                                                            class="ti-user"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Pavan kumar</h5>
                                                        <span class="mail-desc">Just see the my admin!</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-danger btn-circle"><i
                                                            class="fa fa-link"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Luanch Admin</h5>
                                                        <span class="mail-desc">Just see the my new admin!</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="../../assets/images/users/1.jpg" alt="user" class="rounded-circle" width="31">
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end user-dd animated" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user me-1 ms-1"></i>
                                    My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet me-1 ms-1"></i>
                                    My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email me-1 ms-1"></i>
                                    Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i
                                        class="ti-settings me-1 ms-1"></i> Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i
                                        class="fa fa-power-off me-1 ms-1"></i> Logout</a>
                                <div class="dropdown-divider"></div>
                                <div class="ps-4 p-10"><a href="javascript:void(0)"
                                        class="btn btn-sm btn-success btn-rounded text-white">View Profile</a></div>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
            <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="pt-4">
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link"
                               href="index1.aspx" aria-expanded="false">
                                <i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-chart-line "></i><span class="hide-menu">Accounts</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="Chart_of_account.aspx" class="sidebar-link">
                                        <i class="far fa-user-circle"></i><span class="hide-menu">
                                            Chart of Account
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Cash_Receipt_Voucher.aspx" class="sidebar-link">
                                        <i class="mdi mdi-receipt"></i><span class="hide-menu">
                                            Cash Receipt Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Cash_Payment_Voucher.aspx" class="sidebar-link">
                                        <i class="far fa-credit-card"></i><span class="hide-menu">
                                            Cash Payment Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Bank_Receipt_Voucher.aspx" class="sidebar-link">
                                        <i class="mdi mdi-receipt"></i><span class="hide-menu">
                                            Bank Receipt Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Bankk_Payment_Voucher.aspx" class="sidebar-link">
                                        <i class="far fa-credit-card"></i><span class="hide-menu">
                                            Bank Payment Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Journall_Voucher.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Journal Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Account_Name_Update.aspx" class="sidebar-link">
                                        <i class="fas fa-edit"></i><span class="hide-menu">
                                            Account Name Update
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Bank_Cheque_Transaction.aspx" class="sidebar-link">
                                        <i class="fas fa-money-bill-alt"></i><span class="hide-menu">
                                            Bank Cheque Transaction
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="mdi mdi-chart-bar"></i><span class="hide-menu">Inventory</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="Project.aspx" class="sidebar-link">
                                        <i class="fas fa-building"></i><span class="hide-menu">
                                            Project Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Product.aspx" class="sidebar-link">
                                        <i class="fas fa-boxes"></i><span class="hide-menu">
                                            Product Voucher
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-shopping-cart"></i><span class="hide-menu">Purchase</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="Purchase.aspx" class="sidebar-link">
                                        <i class="fas fa-shopping-basket"></i><span class="hide-menu">
                                            Purchase Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Purchase_Return.aspx" class="sidebar-link">
                                        <i class="fas fa-undo-alt"></i><span class="hide-menu">
                                            Purchase Return Voucher
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-edit"></i><span class="hide-menu">Sale</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="Sale.aspx" class="sidebar-link">
                                        <i class="fab fa-sellsy"></i><span class="hide-menu">
                                            Sale Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Sale_Return.aspx" class="sidebar-link">
                                        <i class="fas fa-undo-alt"></i><span class="hide-menu">
                                            Sale Return Voucher
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                         <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="far fa-chart-bar"></i><span class="hide-menu">Reports</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">                                                                                           
                                <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Reports
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>


                       <%-- <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="far fa-chart-bar"></i><span class="hide-menu">Cash Reports</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item" >
                                    <a href="Date_form.aspx"  class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"  ></i><span class="hide-menu">
                                            Cash Receive Report
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item" >
                                    <a href="Date_form.aspx"  class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"  ></i><span class="hide-menu">
                                            Cash Payment Report
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item" >
                                    <a href="Date_form.aspx"  class="sidebar-link">
                                        <i class="fas fa-file-medical-alt" ></i><span class="hide-menu">
                                            Bank Receive Report
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Bank Payment Report
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Journal Voucher Report
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="far fa-chart-bar"></i><span class="hide-menu">Purchase Reports</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item" >
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt" ></i><span class="hide-menu">
                                            Purchase Party Wise
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Purchase Product Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Purchase Date Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Project Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Return Party Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Return Product WS
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Return Date Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Return Project Wise
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                         <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="far fa-chart-bar"></i><span class="hide-menu">Sale Reports</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item" >
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt" ></i><span class="hide-menu">
                                            Sale Party Wise
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Sale Product Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Sale Date Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Project Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Return Party Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Return Product WS
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Return Date Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Return Project Wise
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>--%>

                        

                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-wrench"></i><span class="hide-menu">Utilities</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="icon-material.aspx" class="sidebar-link">
                                        <i class="mdi mdi-emoticon"></i><span class="hide-menu">
                                            Material Icons
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="icon-fontawesome.aspx" class="sidebar-link">
                                        <i class="mdi mdi-emoticon-cool"></i><span class="hide-menu">
                                            Font Awesome
                                            Icons
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-cogs"></i><span class="hide-menu">SetUp</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="icon-material.aspx" class="sidebar-link">
                                        <i class="mdi mdi-emoticon"></i><span class="hide-menu">
                                            Material Icons
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="icon-fontawesome.aspx" class="sidebar-link">
                                        <i class="mdi mdi-emoticon-cool"></i><span class="hide-menu">
                                            Font Awesome
                                            Icons
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="mdi mdi-account-key"></i><span class="hide-menu">Authentication</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="authentication-login.aspx" class="sidebar-link">
                                        <i class="fas fa-user"></i><span class="hide-menu"> Login </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Create_User.aspx" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i><span class="hide-menu">
                                            Register
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Cash Payment Voucher</h4>
                        <div class="ms-auto text-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
            <div class="container-fluid" style="margin-top:0px">
                <form id="frm" runat="server">
                    <div class="row ">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title" style="font-family: Arial; font-size: large; margin-left: 18px;">Cash Payment</h4>
                                </div>
                                <div class="comment-widgets scrollable table-responsive-sm">
                                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>                    
                            
                                            <table class="auto-style91">
                                                <tr>
                                                    <td class="auto-style88">&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="Label8" runat="server" Font-Bold="False" Font-Italic="False" Text="Date" Width="85px" Font-Names="Arial" />
                                                    </td>
                                                    <td class="auto-style102" colspan="2">
                                                        <asp:TextBox ID="TextBox1" runat="server" onkeypress="pressEnter()" />
                                                        <asp:ImageButton ID="Button2" runat="server" CssClass="auto-style4" Font-Bold="True" Height="25px" ImageAlign="TextTop" ImageUrl="~/Images/—Pngtree—calendar icon in trendy style_5003062.png" Text="" Width="25px" />
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="yyyy-MM-dd" PopupButtonID="Button2" PopupPosition="BottomRight" TargetControlID="TextBox1" />

                                                    </td>
                                                    <td class="auto-style114">&nbsp;</td>
                                                    <td class="auto-style113">&nbsp;</td>
                                                    <td class="auto-style115">&nbsp;</td>
                                                    <td class="auto-style116">
                                                        <asp:Label ID="Label11" runat="server" Font-Bold="False" Font-Names="Arial" style="text-align: right" Text="Opn Blc" Width="60px" />
                                                    </td>
                                                    <td class="auto-style107">
                                                        <asp:TextBox ID="TextBox6" runat="server" OnKeyDown="TextBox4_KeyDown" onkeypress="pressEnter5()" Width="100px" />
                                                    </td>
                                                    <td class="auto-style108">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style88">&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="Label9" runat="server" Font-Bold="False" Text="Voc#" Width="85px" Font-Names="Arial" />
                                                    </td>
                                                    <td class="auto-style112">
                                                        <asp:TextBox ID="TextBox5" runat="server" onkeypress="pressEnter1()" />
                                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="18px" ImageAlign="AbsMiddle" ImageUrl="~/Images/search-512.png" OnClick="ImageButton1_Click" Width="22px" />
                                                    </td>
                                                    <td class="auto-style111">&nbsp;</td>
                                                    <td class="auto-style114">&nbsp;</td>
                                                    <td class="auto-style113">&nbsp;</td>
                                                    <td class="auto-style115">&nbsp;</td>
                                                    <td class="auto-style116">&nbsp;</td>
                                                    <td class="auto-style108">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style88">&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="Label13" runat="server" Font-Bold="False" Font-Names="Arial" style="text-align: right" Text="Project" />
                                                    </td>
                                                    <td class="auto-style112">
                                                        <ajaxToolkit:ComboBox ID="ComboBox2" runat="server" AutoCompleteMode="SuggestAppend" onkeypress="pressEnter2()">
                                                        </ajaxToolkit:ComboBox>
                                                    </td>
                                                    <td class="auto-style111">&nbsp;</td>
                                                    <td class="auto-style114">&nbsp;</td>
                                                    <td class="auto-style113">&nbsp;</td>
                                                    <td class="auto-style115">&nbsp;</td>
                                                    <td class="auto-style116">&nbsp;</td>
                                                    <td class="auto-style108">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style101">&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="Label10" runat="server" Font-Bold="False" Text="Party Name" Width="85px" Font-Names="Arial" />
                                                    </td>
                                                    <td class="auto-style112">
                                                        <ajaxToolkit:ComboBox ID="ComboBox1" runat="server" AutoCompleteMode="SuggestAppend" onkeypress="pressEnter2()">
                                                        </ajaxToolkit:ComboBox>
                                                    </td>
                                                    <td class="auto-style111"><span class="hide-menu">
                                                        <asp:Label ID="Label2" runat="server" Font-Bold="False" style="text-align: justify" Text="Naration" Font-Names="Arial" />
                                                        </span></td>
                                                    <td class="auto-style114">
                                                        <asp:TextBox ID="TextBox2" runat="server" onkeypress="pressEnter3()" style="width: 128px" />
                                                    </td>
                                                    <td class="auto-style113">
                                                        <asp:Label ID="Label3" runat="server" Font-Bold="False" Text="Invoice" style="text-align: right" Font-Names="Arial" />
                                                    </td>
                                                    <td class="auto-style115">
                                                        <asp:TextBox ID="TextBox3" runat="server" onkeypress="pressEnter4()" Width="100px" />
                                                    </td>
                                                    <td class="auto-style116">
                                                        <asp:Label ID="Label4" runat="server" Font-Bold="False" Text="Amount" style="text-align: right" Font-Names="Arial" />
                                                    </td>
                                                    <td class="auto-style108">
                                                        <asp:TextBox ID="TextBox4" runat="server" OnKeyDown="TextBox4_KeyDown" onkeypress="pressEnter5()" Width="100px" />
                                                    </td>
                                                    <td><span class="hide-menu">
                                                        <asp:Button ID="Button4" runat="server" BackColor="#1C5E55" CausesValidation="False" ForeColor="White" Height="25px" OnClick="Button4_Click" Text="⇩" Width="25px" />
                                                        </span></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                             
                                            </table>
                                            
                                            <br />

                                             <asp:GridView ID="gridview" runat="server"  Font-Bold="False" Font-Names="Arial" Font-Size="12pt" OnRowCreated="grid_RowCreated" CellPadding="4" GridLines="None" ForeColor="#333333" CausesValidation="False" ShowHeaderWhenEmpty="True" Width="90%" CssClass="m-l-40" ShowFooter="True" >
                                   
                                                <EditRowStyle BackColor="#7C6F57" HorizontalAlign="Center" Font-Names="Arial" Font-Size="12pt" />
                                                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
           
                                                <RowStyle BackColor="#E3EAEB" HorizontalAlign="Center" />
                                                <AlternatingRowStyle BackColor="White"/>
           
                                                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                                                <SortedAscendingHeaderStyle BackColor="#246B61" />
                                                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                                                <SortedDescendingHeaderStyle BackColor="#15524A" />
          

                                                <Columns >
                                                <asp:TemplateField HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Right"  >
                                                <HeaderTemplate>Operation</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Images/edit-regular.png"  OnClick="btnEdit_Click" Width="20px" Height="20px" CssClass="m-l-0" />
                                                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/Images/trash-alt-regular.png"  OnClick="btnDelete_Click" Width="20px" Height="20px" />                          
                                                </ItemTemplate>                   

                                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                            </asp:TemplateField>      
                                            </Columns>
                                           </asp:GridView>
                                            <table class="auto-style91">
                                                <tr>
                                                    <td class="auto-style124">&nbsp;</td>
                                                    <td class="auto-style126">&nbsp;</td>
                                                    <td class="auto-style128">&nbsp;</td>
                                                    <td class="auto-style121">&nbsp;</td>
                                                    <td class="auto-style122">&nbsp;</td>
                                                    <td class="auto-style133">&nbsp;</td>
                                                    <td class="auto-style129">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style124">&nbsp;</td>
                                                    <td class="auto-style126">&nbsp;</td>
                                                    <td class="auto-style128">&nbsp;</td>
                                                    <td class="auto-style121">&nbsp;</td>
                                                    <td class="auto-style122">&nbsp;</td>
                                                    <td class="auto-style133">&nbsp;</td>
                                                    <td class="auto-style129">
                                                        <asp:Label ID="Label12" runat="server" Font-Bold="False" Font-Names="Arial" style="text-align: right" Text="Clos Blc" Width="60px" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox7" runat="server" OnKeyDown="TextBox4_KeyDown" onkeypress="pressEnter5()" Width="100px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style124">&nbsp;</td>
                                                    <td class="auto-style126">&nbsp;</td>
                                                    <td class="auto-style128">
                                                        <asp:Button ID="Button7" runat="server" BackColor="#1C5E55" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt" Font-Strikeout="False" Font-Underline="False" ForeColor="White" Height="30px" OnClick="Button7_Click" style="margin-left: 10px" Text="Update" Width="118px" />
                                                    </td>
                                                    <td class="auto-style121">
                                                        <asp:Button ID="Button6" runat="server" BackColor="#1C5E55" Font-Names="PMingLiU-ExtB" Font-Size="13pt" ForeColor="White" Height="30px" OnClick="Button6_Click" Text="Save" Width="120px" style="font-weight: 700" />
                                                    </td>
                                                    <td class="auto-style122">
                                                        <asp:Button ID="Button1" runat="server" BackColor="#1C5E55" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt" Font-Strikeout="False" Font-Underline="False" ForeColor="White" Height="30px" OnClick="Button1_Click" style="margin-left: 11px" Text="Reset" Width="120px" />
                                                    </td>
                                                    <td class="auto-style133">
                                                        <asp:Button ID="Button3" runat="server" BackColor="#1C5E55" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt" Font-Strikeout="False" Font-Underline="False" ForeColor="White" Height="30px" OnClick="Button3_Click" style="margin-left: 10px" Text="Delete" Width="120px" />
                                                    </td>
                                                    <td class="auto-style129">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                            <br />
                           
                                         
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div> 
                        </div>
                    </div>
                </form>
            </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
           
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
             <footer class="footer text-center">
                All Rights Reserved by Admin Designed and Developed by TECH<b>NO</b>HUB.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
        <script type="text/javascript">
    function pressEnter() {
      // Key Code for ENTER = 13
      if ((event.keyCode == 13)) {
        document.getElementById("TextBox5").focus({preventScroll:false});
      }
    }
    function pressEnter1() {
        if ((event.keyCode == 13)) {
            document.getElementById("ComboBox1").focus({ preventScroll: false });
        }
    }
    function pressEnter2() {
        if ((event.keyCode == 13)) {
            document.getElementById("TextBox2").focus({ preventScroll: false });
        }
    }
    function pressEnter3() {
        if ((event.keyCode == 13)) {
            document.getElementById("TextBox3").focus({ preventScroll: false });
        }
    }
    function pressEnter4() {
      if ((event.keyCode == 13)) {
          document.getElementById("TextBox4").focus({ preventScroll: false });
      }
    }
    function pressEnter5() {
        if ((event.keyCode == 13)) {
            document.getElementById("btninsert").focus({ preventScroll: false });
        }
    }
    </script>

    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
</body>

</html>
