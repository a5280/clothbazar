﻿using HafizBuilder;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HAFIZ_BUILDERS.matrix_admin_bt5_master.html.ltr
{
    public partial class Bank_Cheque_Transaction : System.Web.UI.Page
    {
        string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                filldata();           
            }
        }
        public void filldata()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            { 
                con.Open();
                GlobalClass.adap = new SqlDataAdapter("select DATE,Voucher_#,Vou_TYPE,Party_Name,Naration1,Credit,Chq#,Chq_Date,Un_Present from Party_Ledger where Un_Present='Un Present Cheque' and Debit='0' ", con);
                SqlCommandBuilder bui = new SqlCommandBuilder(GlobalClass.adap);
                GlobalClass.dt = new DataTable();
                GlobalClass.adap.Fill(GlobalClass.dt);
                GridView1.DataSource = GlobalClass.dt;
                GridView1.DataBind();
                TextBox1.Text = DateTime.Now.Date.ToString();
                con.Close();                 
            }
        }
     
        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            //Reference the DropDownList.
            DropDownList dropDownList = sender as DropDownList;
            //Get the ID of the DropDownList.
            string id = dropDownList.ID;
            //Display the Selected Text of DropDownList.

            using (SqlConnection con = new SqlConnection(cnString))
            {              
                if (dropDownList.SelectedItem.Text == "Present")
                {
                    int rowid = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
                    con.Open();
                    string QRY = "update Party_Ledger set Un_Present='" + dropDownList.SelectedItem.Text + "' WHERE   Chq#='"+ GridView1.Rows[rowid].Cells[6].Text + "'  ";
                    SqlCommand cmd = new SqlCommand(QRY, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    con.Close();

                    filldata();
                    //  ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('" + dropDownList.SelectedItem.Text + "');", true);
                }
                if (dropDownList.SelectedItem.Text == "Suspend")
                {
                    int rowid = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
                    con.Open();
                    string QRY = "DELETE FROM Party_Ledger WHERE Chq#='" + GridView1.Rows[rowid].Cells[6].Text + "' ";
                    SqlCommand cmd = new SqlCommand(QRY, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    con.Close();

                    filldata();
                }
            }
        }

      

    }
}