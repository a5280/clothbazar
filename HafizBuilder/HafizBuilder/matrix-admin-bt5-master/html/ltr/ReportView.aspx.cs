﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace HafizBuilder.matrix_admin_bt5_master.html.ltr
{
    public partial class ReportView : System.Web.UI.Page
    {
        string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;

        public void Page_Load(object sender, EventArgs e)
        {
            if (Session["date1"] !=null)
            {
                TextBox4.Text = "";
                TextBox1.Text = Session["date1"].ToString();
                TextBox2.Text = Session["date2"].ToString();
                TextBox3.Text = Session["etype"].ToString();
                TextBox4.Text = Session["etype2"].ToString();

                Button1_Click(Button1, null);
            }
          
        }

        public void CRV1_Init(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ////////////////////////////////////////// JV Reports
            if (TextBox3.Text == "Journal Voucher")
            {

                Reports.RPT_JV sr = new Reports.RPT_JV();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }

                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text6"];
                to.Text = "JOURNAL VOUCHER REPORT";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text12"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }

            /////////////////////////////////////////// CASH BOOK
            if (TextBox3.Text == "Cash Book")
            {
                Reports.rptLedger sr = new Reports.rptLedger();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }

                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section2"].ReportObjects["Text12"];
                to.Text = "CASH BOOK";

                sr.RecordSelectionFormula = "{ command.date}>= datetime('" + TextBox1.Text + "') and { command.date}<= datetime('" + TextBox2.Text + "') and { command.party_name} = 'CASH' ";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section2"].ReportObjects["Text14"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }

            ////////////////////////////////////////// Trial_Balance Reports
            if (TextBox3.Text == "Trial_Balance")
            {
                Reports.rptAlltoAllProject sr = new Reports.rptAlltoAllProject();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }

                //    TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                //   to.Text = "JOURNAL VOUCHER REPORT";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text14"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }

            ////////////////////////////////////////// Party_Ledger Reports
            if (TextBox4.Text == "Party Ledger")
            {
                using (SqlConnection con = new SqlConnection(cnString))
                {
                    string QRY11 = "select Account_# FROM Party_AC where Account_Name='" + TextBox3.Text + "' ";
                    SqlCommand cmd11 = new SqlCommand(QRY11, con);
                    SqlDataAdapter saa = new SqlDataAdapter(cmd11);
                    DataTable dttt = new DataTable();
                    saa.Fill(dttt);
                    foreach (DataRow item1 in dttt.Rows)
                    {
                        PID2 = item1["Account_#"].ToString();
                    }
                }
               

                Reports.CrystalReport1 sr = new Reports.CrystalReport1();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }

                sr.RecordSelectionFormula = "{command.P_Account_#} =" + PID2 + " and ({command.date}>= datetime('" + TextBox1.Text + "') and {command.date}<= datetime('" + TextBox2.Text + "'))  ";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text14"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
                TextBox4.Text = string.Empty;
            }


            //////////////////////////////////////////Cash Reports
            if (TextBox3.Text == "Cash Receipt Voucher")
            {
                Reports.RPT_CRV sr = new Reports.RPT_CRV();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }

                //TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                //to.Text = "CASH RECEIPT VOUCHER REPORT";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text12"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }

             if (TextBox3.Text == "Cash Payment Voucher")
            {
                Reports.RPT_CRV sr = new Reports.RPT_CRV();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }

                //TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                //to.Text = "CASH PAYMENT VOUCHER REPORT";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text12"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }
             if (TextBox3.Text == "Bank Receipt Voucher")
            {
                Reports.RPT_CRV sr = new Reports.RPT_CRV();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }

                //TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                //to.Text = "BANK RECEIPT REPORT";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text12"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }
             if (TextBox3.Text == "Bank Payment Voucher")
            {
                Reports.RPT_CRV sr = new Reports.RPT_CRV();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }

                //TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                //to.Text = "BANK PAYMENT REPORT";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text12"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }

            //////////////////////////////////////////Purchase Reports
            if (TextBox3.Text == "Purchase Party")
            {
                Reports.RPT_Sale sr = new Reports.RPT_Sale();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "PURCHASE REPORT PARTY WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }

             if (TextBox3.Text == "Purchase Product")
            {
                Reports.RPT_Saleproduct sr = new Reports.RPT_Saleproduct();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "PURCHASE REPORT PRODUCT WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }
             if (TextBox3.Text == "Purchase Date")
            {
                Reports.RPT_SaleDate sr = new Reports.RPT_SaleDate();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "PURCHASE REPORT DATE WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }
             if (TextBox3.Text == "Purchase Project")
            {
                Reports.RPT_Sale sr = new Reports.RPT_Sale();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "PURCHASE REPORT PRPJECT WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }

            //////////////////////////////////////////Purchase Return Reports
             if(TextBox3.Text == "Purchase Return Party")
            {
                Reports.RPT_Sale sr = new Reports.RPT_Sale();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "PURCHASE RETURN REPORT PARTY WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }

             if (TextBox3.Text == "Purchase Return Product")
            {
                Reports.RPT_Saleproduct sr = new Reports.RPT_Saleproduct();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "PURCHASE RETURN REPORT PRODUCT WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }
             if (TextBox3.Text == "Purchase Return Date")
            {
                Reports.RPT_SaleDate sr = new Reports.RPT_SaleDate();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "PURCHASE RETURN REPORT DATE WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }
             if (TextBox3.Text == "Purchase Return Project")
            {
                Reports.RPT_Sale sr = new Reports.RPT_Sale();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "PURCHASE RETURN REPORT PRPJECT WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }


            ////////////////////////////////////////// Sale Reports
              if (TextBox3.Text == "Sale Party")
            {
                Reports.RPT_Sale sr = new Reports.RPT_Sale();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "SALE REPORT PARTY WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }

             if (TextBox3.Text == "Sale Product")
            {
                Reports.RPT_Saleproduct sr = new Reports.RPT_Saleproduct();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "SALE REPORT PRODUCT WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }
             if (TextBox3.Text == "Sale Date")
            {
                Reports.RPT_SaleDate sr = new Reports.RPT_SaleDate();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "SALE REPORT DATE WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }
             if (TextBox3.Text == "Sale Project")
            {
                Reports.RPT_Sale sr = new Reports.RPT_Sale();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "SALE REPORT PRPJECT WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }


            //////////////////////////////////////////Sale Return Reports
             if (TextBox3.Text == "Sale Return Party")
            {
                Reports.RPT_Sale sr = new Reports.RPT_Sale();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "SALE RETURN REPORT PARTY WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }

             if (TextBox3.Text == "Sale Return Product")
            {
                Reports.RPT_Saleproduct sr = new Reports.RPT_Saleproduct();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "SALE RETURN REPORT PRODUCT WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }
             if (TextBox3.Text == "Sale Return Date")
            {
                Reports.RPT_SaleDate sr = new Reports.RPT_SaleDate();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "SALE RETURN REPORT DATE WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }
             if (TextBox3.Text == "Sale Return Project")
            {
                Reports.RPT_Sale sr = new Reports.RPT_Sale();

                ConnectionInfo ciReportConnection = new ConnectionInfo();
                ciReportConnection.ServerName = "SQL5064.site4now.net";
                ciReportConnection.DatabaseName = "db_a504e8_hbdb";
                ciReportConnection.UserID = "db_a504e8_hbdb_admin";
                ciReportConnection.Password = "Admin12@";
                //ciReportConnection.IntegratedSecurity = true;
                Tables tables = sr.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = ciReportConnection;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }


                TextObject to = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                to.Text = "SALE RETURN REPORT PRPJECT WISE";
                TextObject too = (TextObject)sr.ReportDefinition.Sections["Section1"].ReportObjects["Text3"];
                too.Text = "From:-" + TextBox1.Text + " To " + TextBox2.Text;
                CrystalReportViewer1.ReportSource = sr;
            }
            //ReportDocument cryRpt = new ReportDocument();
            //cryRpt.Load(Server.MapPath("~/Reports/RPT_CRV.rpt"));
            //CrystalReportViewer1.ReportSource = cryRpt;

        }
        string PID2;
        protected void CrystalReportViewer1_Disposed(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                string QRY11 = "select Account_# FROM Party_AC where Account_Name='" + TextBox3.Text + "' ";
                SqlCommand cmd11 = new SqlCommand(QRY11, con);
                SqlDataAdapter saa = new SqlDataAdapter(cmd11);
                DataTable dttt = new DataTable();
                saa.Fill(dttt);
                foreach (DataRow item1 in dttt.Rows)
                {
                    PID2 = item1["Account_#"].ToString();
                }

                SqlCommand cmd = new SqlCommand("delete from party_ledger where p_account_# = " +PID2 +" and naration1 = 'OPENING BALANCE' AND ROSE = 1", con);
                cmd.ExecuteNonQuery();
                con.Close();
            }

        }
    }
}