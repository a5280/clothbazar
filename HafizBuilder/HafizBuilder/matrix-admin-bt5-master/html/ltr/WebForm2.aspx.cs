﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Text;
using HafizBuilder;

namespace HAFIZ_BUILDERS.matrix_admin_bt5_master.html.ltr
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;
        DataTable dtt = new DataTable();
        
        protected void Page_Load(object sender, EventArgs e)
        {          
            if (!Page.IsPostBack)
            {
                
                if (ViewState["record"] == null)
                {
                    dtt.Columns.Add("name");
                    dtt.Columns.Add("age");
                    dtt.Columns.Add("add");
                    dtt.Columns.Add("ino");
                    dtt.Columns.Add("amt");
                    ViewState["record"] = dtt;
                    dtt = (DataTable)ViewState["record"];
                    dtt.Rows.Add(" No record available");
                    GridView1.DataSource = dtt;
                    GridView1.DataBind();
                    GlobalClass.id = 0;
                }               
            }

        
        }
       
        protected void Button1_Click(object sender, EventArgs e)
        {
          
            if (GlobalClass.id == 0)
            {
                dtt = (DataTable)ViewState["record"];
                dtt.Rows.RemoveAt(0);
                GridView1.DataSource = dtt;
                GridView1.DataBind();
                GlobalClass.id=1;
              }

            dtt = (DataTable)ViewState["record"];
            dtt.Rows.Add(TextBox1.Text, TextBox2.Text, TextBox3.Text, TextBox2.Text, TextBox2.Text);
            GridView1.DataSource = dtt;
            GridView1.DataBind();
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
        }
        protected void Button3_Click(object sender, EventArgs e)
        {
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";

            dtt = (DataTable)ViewState["record"];
            dtt.Rows.RemoveAt(0);
            GridView1.DataSource = dtt;
            GridView1.DataBind();
            GlobalClass.id = 0;

            ViewState["record"] = dtt;
            dtt = (DataTable)ViewState["record"];
            dtt.Rows.Add(" No record available");
            GridView1.DataSource = dtt;
            GridView1.DataBind();
        }

        public void nullvalue()
        {
            GridView1.DataSource = null;
            GridView1.DataBind();          
        }
        public void record()
        {
                dtt.Columns.Add("name");
                dtt.Columns.Add("age");
                dtt.Columns.Add("add");
                dtt.Columns.Add("ino");
                dtt.Columns.Add("amt");
                ViewState["record"] = dtt;
                dtt = (DataTable)ViewState["record"];
                dtt.Rows.Add(" No record available");
                GridView1.DataSource = dtt;
                GridView1.DataBind();        
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {

                for (int i = 0; i < GridView1.Rows.Count; ++i)
                {
                    string QRY = "INSERT INTO test (name,age,Address) VALUES (@name,@age,@add)";
                    SqlCommand cmd = new SqlCommand(QRY, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@name", GridView1.Rows[i].Cells[1].Text);
                    cmd.Parameters.AddWithValue("@age", GridView1.Rows[i].Cells[2].Text);
                    cmd.Parameters.AddWithValue("@add", GridView1.Rows[i].Cells[3].Text);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                  //  Response.Write("<script> alert('Save Record') </script>");
                }

            }

            nullvalue();
            record();
            GridView1.Rows[0].Cells[1].ColumnSpan = 2;
            GridView1.Rows[0].Cells.RemoveAt(0);
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
            GlobalClass.id = 0;
        }

        protected void grid_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (!IsPostBack)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[1].ColumnSpan = 2;
                    //now make up for the colspan from cell2
                    e.Row.Cells.RemoveAt(0);

                }
            }
      
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            int rowid = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            TextBox1.Text = GridView1.Rows[rowid].Cells[1].Text;
            TextBox2.Text = GridView1.Rows[rowid].Cells[2].Text;
            TextBox3.Text = GridView1.Rows[rowid].Cells[3].Text;

            dtt = (DataTable)ViewState["record"];
            dtt.Rows.RemoveAt(rowid);
            GridView1.DataSource = dtt;
            GridView1.DataBind();


        }

    
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            dtt = (DataTable)ViewState["record"];
            int rowid = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            dtt.Rows.RemoveAt(rowid);
            GridView1.DataSource = dtt;
            GridView1.DataBind();

            if (GridView1.Rows.Count==0)
            {
                
                ViewState["record"] = dtt;
                dtt = (DataTable)ViewState["record"];
                
                dtt.Rows.Add(" No record available");
                GridView1.DataSource = dtt;
                GridView1.DataBind();
                GridView1.Rows[0].Cells[1].ColumnSpan = 2;
                GridView1.Rows[0].Cells.RemoveAt(0);
                GlobalClass.id = 0;
            }
        }

      
    }
}