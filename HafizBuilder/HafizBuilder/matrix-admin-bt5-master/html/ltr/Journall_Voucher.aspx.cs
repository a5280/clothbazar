﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace HafizBuilder.matrix_admin_bt5_master.html.ltr
{
    public partial class Journall_Voucher : System.Web.UI.Page
    {
        string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;
        DataTable dtt = new DataTable();
        private string PID,PID2, Total,Total1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (ViewState["Cash"] == null)
                {
                    dtt.Columns.Add("Party_Name");
                    dtt.Columns.Add("Naration");
                    dtt.Columns.Add("Invoice");
       
                    dtt.Columns.Add("Debit");
                    dtt.Columns.Add("Credit");
                    ViewState["Cash"] = dtt;
                    dtt = (DataTable)ViewState["Cash"];
                    dtt.Rows.Add(" No record available");
                    gridview.DataSource = dtt;
                    gridview.DataBind();

                    GlobalClass.id = 0;
                }
                opn_blc();
                clo_blc();
                code_check();
                loadcombo_list();
                Button2.Focus();
            }
        }
        protected void Button4_Click(object sender, EventArgs e)
        {         
            if (GlobalClass.id == 0)
            {
                dtt = (DataTable)ViewState["Cash"];
                dtt.Rows.RemoveAt(0);
                gridview.DataSource = dtt;
                gridview.DataBind();
                GlobalClass.id = 1;
            }

            if (TextBox9.Text == "")
            {
                TextBox9.Text = "0";
            }
            if (TextBox10.Text == "")
            {
                TextBox10.Text = "0";
            }
            dtt = (DataTable)ViewState["Cash"];
            dtt.Rows.Add(ComboBox1.Text, TextBox2.Text, TextBox3.Text, TextBox9.Text, TextBox10.Text);
            gridview.DataSource = dtt;
            gridview.DataBind();
            ComboBox1.SelectedIndex = -1;
            TextBox2.Text = "";
            TextBox3.Text = "";
            TextBox9.Text = "";
            TextBox10.Text = "";
            ComboBox1.Focus();
            Sum();
        }
        public void nullvalue()
        {
            gridview.DataSource = null;
            gridview.DataBind();
        }
        public void record()
        {
            dtt.Columns.Add("Party_Name");
            dtt.Columns.Add("Naration");
            dtt.Columns.Add("Invoice");
          
            dtt.Columns.Add("Debit");
            dtt.Columns.Add("Credit");
            ViewState["Cash"] = dtt;
            dtt = (DataTable)ViewState["Cash"];
            dtt.Rows.Add(" No record available");
            gridview.DataSource = dtt;
            gridview.DataBind();

        }
        protected void Button6_Click(object sender, EventArgs e)
        {

            using (SqlConnection con = new SqlConnection(cnString))
            {
                ViewState["voc_no"] = TextBox5.Text;
                ViewState["date"] = TextBox1.Text;

                for (int i = 0; i < gridview.Rows.Count; ++i)
                {
                    con.Open();
                    string QRY11 = "select Account_# FROM Party_AC where Account_Name='" + gridview.Rows[i].Cells[1].Text + "' ";
                    SqlCommand cmd11 = new SqlCommand(QRY11, con);
                    SqlDataAdapter saa = new SqlDataAdapter(cmd11);
                    DataTable dttt = new DataTable();
                    saa.Fill(dttt);
                    foreach (DataRow item1 in dttt.Rows)
                    {
                        PID2 = item1["Account_#"].ToString();
                    }

                    string QRY = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration1,Invoice,Debit,Credit,Party_Name,Project_Name) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name,@Proj_Name)";
                    SqlCommand cmd2 = new SqlCommand(QRY, con);
                    cmd2.CommandType = CommandType.Text;
                    cmd2.Parameters.AddWithValue("@Voc#", ViewState["voc_no"].ToString());
                    cmd2.Parameters.AddWithValue("@P_AC", PID2);
                    cmd2.Parameters.AddWithValue("@DATE",Convert.ToDateTime(ViewState["date"]).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture));
                    cmd2.Parameters.AddWithValue("@V_TYPE", "JV");
                    cmd2.Parameters.AddWithValue("@Nara1", gridview.Rows[i].Cells[2].Text);
                    cmd2.Parameters.AddWithValue("@INV", gridview.Rows[i].Cells[3].Text);
                    cmd2.Parameters.AddWithValue("@DEBIT", gridview.Rows[i].Cells[4].Text);
                    cmd2.Parameters.AddWithValue("@CREDIT", gridview.Rows[i].Cells[5].Text);
                    cmd2.Parameters.AddWithValue("@P_Name", gridview.Rows[i].Cells[1].Text);
                    cmd2.Parameters.AddWithValue("@Proj_Name", ComboBox2.Text);
                    string ab;
                    if (RadioButton1.Checked==true)
                    {
                         ab = "Cheque";
                    }
                    else
                    {
                        ab = "Cash";
                    }
                    cmd2.Parameters.AddWithValue("@Voc_cash_chq_type", ab);
                    cmd2.ExecuteNonQuery();
                    ////////////////////////////////////////////////////////// CASH DEBIT
                    //string QRY1 = "select Account_# FROM Party_AC where Account_Name='CASH'";
                    //SqlCommand cmd3 = new SqlCommand(QRY1, con);
                    //SqlDataAdapter sa = new SqlDataAdapter(cmd3);
                    //DataTable dt = new DataTable();
                    //sa.Fill(dt);
                    //foreach (DataRow item in dt.Rows)
                    //{
                    //    PID = item["Account_#"].ToString();
                    //}
                    //string QRY2 = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration1,Invoice,Debit,Credit,Party_Name,Project_Name) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name,@Proj_Name)";
                    //SqlCommand cmd1 = new SqlCommand(QRY2, con);
                    //cmd1.CommandType = CommandType.Text;
                    //cmd1.Parameters.AddWithValue("@Voc#", ViewState["voc_no"].ToString());
                    //cmd1.Parameters.AddWithValue("@P_AC", PID);
                    //cmd1.Parameters.AddWithValue("@DATE", Convert.ToDateTime(ViewState["date"]).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture));
                    //cmd1.Parameters.AddWithValue("@V_TYPE", "JV");
                    //cmd1.Parameters.AddWithValue("@Nara1", gridview.Rows[i].Cells[2].Text);
                    //cmd1.Parameters.AddWithValue("@INV", gridview.Rows[i].Cells[3].Text);
                    //cmd1.Parameters.AddWithValue("@DEBIT", gridview.Rows[i].Cells[4].Text);
                    //cmd1.Parameters.AddWithValue("@CREDIT", gridview.Rows[i].Cells[5].Text );
                    //cmd1.Parameters.AddWithValue("@P_Name", gridview.Rows[i].Cells[1].Text);
                    //cmd1.Parameters.AddWithValue("@Proj_Name", ComboBox2.Text);
                    //if (RadioButton1.Checked == true)
                    //{
                    //    ab = "Cheque";
                    //}
                    //else
                    //{
                    //    ab = "Cash";
                    //}
                    //cmd1.Parameters.AddWithValue("@Voc_cash_chq_type", ab);
                    //cmd1.ExecuteNonQuery();
                    con.Close();               
                }
            }
            txtzero();
            loadcombo_list();
            code_check();

            nullvalue();
            record();
            gridview.Rows[0].Cells[1].ColumnSpan = 2;
            gridview.Rows[0].Cells.RemoveAt(0);
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
     
            GlobalClass.id = 0;
            opn_blc();
            clo_blc();
           
        }
        protected void grid_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (!IsPostBack)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[1].ColumnSpan = 2;
                    //now make up for the colspan from cell2
                    e.Row.Cells.RemoveAt(0);
                }
            }

        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            int rowid = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            ComboBox1.Text = gridview.Rows[rowid].Cells[1].Text;
            TextBox2.Text = gridview.Rows[rowid].Cells[2].Text;
            TextBox3.Text = gridview.Rows[rowid].Cells[3].Text;       
            TextBox9.Text = gridview.Rows[rowid].Cells[4].Text;
            TextBox9.Text = gridview.Rows[rowid].Cells[5].Text;

            dtt = (DataTable)ViewState["Cash"];
            dtt.Rows.RemoveAt(rowid);
            gridview.DataSource = dtt;
            gridview.DataBind();
            Sum();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            dtt = (DataTable)ViewState["Cash"];
            int rowid = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            dtt.Rows.RemoveAt(rowid);
            gridview.DataSource = dtt;
            gridview.DataBind();

            if (gridview.Rows.Count == 0)
            {
                ViewState["Cash"] = dtt;
                dtt = (DataTable)ViewState["Cash"];

                dtt.Rows.Add(" No record available");
                gridview.DataSource = dtt;
                gridview.DataBind();
                gridview.Rows[0].Cells[1].ColumnSpan = 2;
                gridview.Rows[0].Cells.RemoveAt(0);
                GlobalClass.id = 0;
            }
            for (int i = 0; i < gridview.Rows.Count; i++)
            {
                if (gridview.Rows.Count == 1)
                {
                    sum = 0;
                    credit = 0;
                }
                else
                {
                    sum += int.Parse(gridview.Rows[i].Cells[4].Text);
                    credit += int.Parse(gridview.Rows[i].Cells[5].Text);
                }
            }
            Total = sum.ToString();
            gridview.FooterRow.Cells[1].Text = "Total Amount";
            gridview.FooterRow.Cells[4].Text = Total;
            gridview.FooterRow.Cells[5].Text = Total1;
        }

        public void loadcombo_list()
        {
            SqlConnection con = new SqlConnection(cnString);
            using (con)
            {
                SqlCommand cmd = new SqlCommand("select distinct Account_Name from Party_AC where Account_Type!= 'BUILTEN' ", con);
                con.Open();
                ComboBox1.DataSource = cmd.ExecuteReader();
                ComboBox1.DataTextField = "Account_Name";
                ComboBox1.DataBind();
                con.Close();

                //SqlCommand cmd2 = new SqlCommand("select distinct Account_Name from Party_AC where Account_Type!= 'BUILTEN' ", con);
                //con.Open();
                //ComboBox3.DataSource = cmd2.ExecuteReader();
                //ComboBox3.DataTextField = "Account_Name";
                //ComboBox3.DataBind();
                //con.Close();

                SqlCommand cmd1 = new SqlCommand("select distinct Project_Name from Project  ", con);
                con.Open();
                ComboBox2.DataSource = cmd1.ExecuteReader();
                ComboBox2.DataTextField = "Project_Name";
                ComboBox2.DataBind();
                con.Close();

            }
        }
        public void code_check()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select isnull(max(voucher_#),0)+1 as id from party_ledger where vou_type = 'JV'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    TextBox5.Text = item["id"].ToString();
                }
                //////SqlCommand cmd = new SqlCommand("select count(Voucher_#) from Party_Ledger", con);
                //////int check_null_db = Convert.ToInt32(cmd.ExecuteScalar());

                //////if (check_null_db != Convert.ToInt32(null))
                //////{
                //////    SqlCommand cmd1 = new SqlCommand("select max(Voucher_# + 1) from Party_Ledger", con);
                //////    int main_code = Convert.ToInt32(cmd1.ExecuteScalar());
                //////    TextBox5.Text = main_code.ToString();
                //////}
                //////else
                //////{
                //////    TextBox5.Text = "1";
                //////}
                con.Close();
            }

        }

        public void opn_blc()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select ISNULL(SUM(Debit),0) - ISNULL(SUM(Credit),0)  from Party_Ledger where P_Account_#  in (select Account_# from Party_AC where Account_Name ='CASH' ) and DATE < '" + DateTime.Now.AddDays(-1) + "' ", con);
                int total_ope_blc = Convert.ToInt32(cmd.ExecuteScalar());
                TextBox6.Text = total_ope_blc.ToString();
                con.Close();
            }
        }

        public void clo_blc()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(" select ISNULL(SUM(Debit),0)-ISNULL(SUM(Credit),0)  from Party_Ledger where P_Account_# in (select Account_# from Party_AC where Account_Name ='CASH' ) ", con);
                int total_ope_blc = Convert.ToInt32(cmd.ExecuteScalar());
                TextBox7.Text = total_ope_blc.ToString();
                con.Close();
            }
        }

        int sum = 0,credit=0;
        public void Sum()
        {
            for (int i = 0; i < gridview.Rows.Count; i++)
            {
                sum += int.Parse(gridview.Rows[i].Cells[4].Text);
                credit += int.Parse(gridview.Rows[i].Cells[5].Text);

            }
            Total = sum.ToString();
            Total1 = credit.ToString();
            gridview.FooterRow.Cells[1].Text = "Total Amount";
            gridview.FooterRow.Cells[4].Text = Total;
            gridview.FooterRow.Cells[5].Text = Total1;

        }

        //protected void updateRecord(object sender, GridViewUpdateEventArgs e)
        //{
        //    TextBox txtName = GridView1.Rows[e.RowIndex].FindControl("txtName") as TextBox;
        //    TextBox txtCountry = GridView1.Rows[e.RowIndex].FindControl("txtCountry") as TextBox;
        //    TextBox txtSalary = GridView1.Rows[e.RowIndex].FindControl("txtSalary") as TextBox;
        //    TextBox txtAmt = GridView1.Rows[e.RowIndex].FindControl("txtAmt") as TextBox;
        //    try
        //    {
        //        Guid FileName = Guid.NewGuid();
        //        GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Party_Name"] = txtName.Text.Trim();
        //        GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Naration1"] = txtCountry.Text.Trim();
        //        GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Invoice"] = Convert.ToInt32(txtSalary.Text.Trim());
        //        GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Debit"] = Convert.ToInt32(txtAmt.Text.Trim());
        //        GlobalClass.adap.Update(GlobalClass.dt);
        //        GridView1.EditIndex = -1;
        //        FillGridView();
        //        Sum();
        //    }
        //    catch
        //    {
        //        Response.Write("<script> alert('Record updation fail...') </script>");
        //    }

        //    using (SqlConnection con = new SqlConnection(cnString))
        //    {
        //        con.Open();
        //        SqlCommand cmd;
        //        string QRY = "update Party_Ledger set Party_Name=@Name,Naration1=@nar,Invoice=@inv,Credit=@Cre where Voucher_#=@Voc and Party_Name=@Name and Debit='0' ";
        //        cmd = new SqlCommand(QRY, con);
        //        cmd.CommandType = CommandType.Text;
        //        cmd.Parameters.AddWithValue("@Voc", TextBox5.Text);
        //        cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
        //        cmd.Parameters.AddWithValue("@nar", txtCountry.Text.Trim());
        //        cmd.Parameters.AddWithValue("@inv", Convert.ToInt32(txtSalary.Text.Trim()));
        //        cmd.Parameters.AddWithValue("@Cre", Convert.ToInt32(txtAmt.Text.Trim()));
        //        cmd.ExecuteNonQuery();
        //        con.Close();
        //    }
        //}
        public void txtzero()
        {
            ComboBox1.SelectedIndex = -1;
            ComboBox2.SelectedIndex = -1;
            TextBox1.Text = string.Empty;
            TextBox5.Text = string.Empty;
            TextBox2.Text = string.Empty;
            TextBox3.Text = string.Empty;       
            TextBox9.Text = string.Empty;
            TextBox9.Text = string.Empty;

        }
        protected void Button1_Click(object sender, EventArgs e)
        {

            nullvalue();
            record();
            gridview.Rows[0].Cells[1].ColumnSpan = 2;
            gridview.Rows[0].Cells.RemoveAt(0);
            txtzero();
            code_check();
            GlobalClass.id = 0;
            ComboBox2.SelectedIndex = -1;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                string QRY = "DELETE FROM Party_Ledger WHERE Vou_TYPE = 'JV' AND Voucher_# ='" + TextBox5.Text + "' ";
                SqlCommand cmd = new SqlCommand(QRY, con);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            Button1_Click(Button1, null);
            code_check();
            opn_blc();
            clo_blc();
            ComboBox2.SelectedIndex = -1;
        }
        protected void Button7_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text !="" && TextBox5.Text !="")
            {

            
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                string QRYQ = "DELETE FROM Party_Ledger WHERE Vou_TYPE = 'JV' AND Voucher_# ='" + TextBox5.Text + "' ";
                SqlCommand cmd = new SqlCommand(QRYQ, con);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                con.Close();

                    ViewState["voc_no"] = TextBox5.Text;
                    ViewState["date"] = TextBox1.Text;
                    for (int i = 0; i < gridview.Rows.Count; ++i)
                    {
                        con.Open();
                        string QRY11 = "select Account_# FROM Party_AC where Account_Name='" + gridview.Rows[i].Cells[1].Text + "' ";
                        SqlCommand cmd11 = new SqlCommand(QRY11, con);
                        SqlDataAdapter saa = new SqlDataAdapter(cmd11);
                        DataTable dttt = new DataTable();
                        saa.Fill(dttt);
                        foreach (DataRow item1 in dttt.Rows)
                        {
                            PID2 = item1["Account_#"].ToString();
                        }

                        string QRY = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration1,Invoice,Debit,Credit,Party_Name,Project_Name) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name,@Proj_Name)";
                        SqlCommand cmd2 = new SqlCommand(QRY, con);
                        cmd2.CommandType = CommandType.Text;
                        cmd2.Parameters.AddWithValue("@Voc#", ViewState["voc_no"].ToString());
                        cmd2.Parameters.AddWithValue("@P_AC", PID2);
                        cmd2.Parameters.AddWithValue("@DATE", Convert.ToDateTime(ViewState["date"]).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture));
                        cmd2.Parameters.AddWithValue("@V_TYPE", "JV");
                        cmd2.Parameters.AddWithValue("@Nara1", gridview.Rows[i].Cells[2].Text);
                        cmd2.Parameters.AddWithValue("@INV", gridview.Rows[i].Cells[3].Text);
                        cmd2.Parameters.AddWithValue("@DEBIT", gridview.Rows[i].Cells[4].Text);
                        cmd2.Parameters.AddWithValue("@CREDIT", gridview.Rows[i].Cells[5].Text);
                        cmd2.Parameters.AddWithValue("@P_Name", gridview.Rows[i].Cells[1].Text);
                        cmd2.Parameters.AddWithValue("@Proj_Name", ComboBox2.Text);
                        string ab;
                        if (RadioButton1.Checked == true)
                        {
                            ab = "Cheque";
                        }
                        else
                        {
                            ab = "Cash";
                        }

                        cmd2.Parameters.AddWithValue("@Voc_cash_chq_type", ab);
                        cmd2.ExecuteNonQuery();

                        ////////////////////////////////////////////////////////// CASH DEBIT
                        string QRY1 = "select Account_# FROM Party_AC where Account_Name='CASH'";
                        SqlCommand cmd3 = new SqlCommand(QRY1, con);
                        SqlDataAdapter sa = new SqlDataAdapter(cmd3);
                        DataTable dt = new DataTable();
                        sa.Fill(dt);
                        foreach (DataRow item in dt.Rows)
                        {
                            PID = item["Account_#"].ToString();
                        }
                        string QRY2 = "INSERT INTO Party_Ledger (Voucher_#,P_Account_#,DATE,Vou_TYPE,Naration1,Invoice,Debit,Credit,Party_Name,Project_Name) VALUES (@Voc#,@P_AC,@DATE,@V_TYPE,@Nara1,@INV,@DEBIT,@CREDIT,@P_Name,@Proj_Name)";
                        SqlCommand cmd1 = new SqlCommand(QRY2, con);
                        cmd1.CommandType = CommandType.Text;
                        cmd1.Parameters.AddWithValue("@Voc#", ViewState["voc_no"].ToString());
                        cmd1.Parameters.AddWithValue("@P_AC", PID);
                        cmd1.Parameters.AddWithValue("@DATE", Convert.ToDateTime(ViewState["date"]).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture));
                        cmd1.Parameters.AddWithValue("@V_TYPE", "JV");
                        cmd1.Parameters.AddWithValue("@Nara1", gridview.Rows[i].Cells[2].Text);
                        cmd1.Parameters.AddWithValue("@INV", gridview.Rows[i].Cells[3].Text);
                        cmd1.Parameters.AddWithValue("@DEBIT", gridview.Rows[i].Cells[4].Text);
                        cmd1.Parameters.AddWithValue("@CREDIT", gridview.Rows[i].Cells[5].Text);
                        cmd1.Parameters.AddWithValue("@P_Name", gridview.Rows[i].Cells[1].Text);
                        cmd1.Parameters.AddWithValue("@Proj_Name", ComboBox2.Text);

                        if (RadioButton1.Checked == true)
                        {
                            ab = "Cheque";
                        }
                        else
                        {
                            ab = "Cash";
                        }
                        cmd1.Parameters.AddWithValue("@Voc_cash_chq_type", ab);

                        cmd1.ExecuteNonQuery();

                        con.Close();
                        txtzero();
                        loadcombo_list();
                        code_check();

                    Sum();
                }
            }
            ViewState["voc_no"] = 0;
            nullvalue();
            record();
            gridview.Rows[0].Cells[1].ColumnSpan = 2;
            gridview.Rows[0].Cells.RemoveAt(0);
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
           
            GlobalClass.id = 0;
            opn_blc();
            clo_blc();
                ComboBox2.SelectedIndex = -1;
            }
        }
        public void search()
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {
                ViewState["voc_no"] = TextBox5.Text;
                string QRY = "SELECT  Party_Ledger.DATE,Party_Ledger.Voucher_#,Party_Ledger.Vou_TYPE,Party_Ledger.Credit,Party_Ledger.Debit,Party_Ledger.Invoice,Party_Ledger.Naration1,Party_Ledger.Party_Name,Party_Ledger.P_Account_#,Party_Ledger.Chq#,Party_Ledger.Chq_Date,Party_Ledger.Project_Name FROM Party_Ledger INNER JOIN Party_AC ON Party_Ledger.P_Account_# = Party_AC.Account_# WHERE (Party_AC.Account_Name <> 'CASH') AND (Party_Ledger.Vou_TYPE = 'JV')AND(Party_Ledger.Voucher_#= '" + TextBox5.Text + "')";
                SqlCommand cmd = new SqlCommand(QRY, CON);
                SqlDataAdapter DA = new SqlDataAdapter(cmd);
                DataTable DT = new DataTable();
                DA.Fill(DT);

                dtt.Columns.Add("Party_Name");
                dtt.Columns.Add("Naration");
                dtt.Columns.Add("Invoice");
           
                dtt.Columns.Add("Debit");
                dtt.Columns.Add("Credit");
                ViewState["Cash"] = dtt;
                dtt = (DataTable)ViewState["Cash"];

                foreach (DataRow item in DT.Rows)
                {
                    TextBox1.Text = Convert.ToDateTime(item["DATE"]).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                    ComboBox2.SelectedValue = item["Project_Name"].ToString();
                    TextBox5.Text = item["Voucher_#"].ToString();
                    dtt.Rows.Add(item["Party_Name"].ToString(), item["Naration1"].ToString(), item["Invoice"].ToString(), item["Debit"].ToString(), item["Credit"].ToString());
                    gridview.DataSource = dtt;
                    gridview.DataBind();
                    GlobalClass.id = 1;
                }
                if (dtt.Rows.Count > 0)
                {
                    Sum();
                }
            }
            
        }

        protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (RadioButton2.Checked==true)
            {
                RadioButton1.Checked = false;
                nullvalue();
                record();
                gridview.Rows[0].Cells[1].ColumnSpan = 2;
                gridview.Rows[0].Cells.RemoveAt(0);
            }
        }

        protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (RadioButton1.Checked == true)
            {
                RadioButton2.Checked = false;
                nullvalue();
                record();
                gridview.Rows[0].Cells[1].ColumnSpan = 2;
                gridview.Rows[0].Cells.RemoveAt(0);
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            search();
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            Sum();
        }

    }
}