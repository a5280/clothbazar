﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace HafizBuilder.matrix_admin_bt5_master.html.ltr
{
    public partial class Create_User : System.Web.UI.Page
    {
        string cnString = ConfigurationManager.ConnectionStrings["hafizbuilder"].ConnectionString;
        DataTable dtt = new DataTable();
        private string PID,PID2, Total;
        int inc = 0, Total2=0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                code_check();
               
            }
        }
        protected void Button4_Click(object sender, EventArgs e)
        {
            //inc++;
            //Total2 = inc + Convert.ToInt32(TextBox5.Text);

            //if (GlobalClass.id == 0)
            //{
            //    dtt = (DataTable)ViewState["Cash"];
            //    dtt.Rows.RemoveAt(0);
            //    gridview.DataSource = dtt;
            //    gridview.DataBind();
            //    GlobalClass.id = 1;
            //}

            //dtt = (DataTable)ViewState["Cash"];

            //dtt.Rows.Add(TextBox5.Text,TextBox2.Text, TextBox6.Text);
            //gridview.DataSource = dtt;
            //gridview.DataBind();
            //TextBox6.Text = string.Empty;

            //TextBox2.Text = "";
            //TextBox5.Text = "";
            //TextBox6.Text = string.Empty;
            //TextBox2.Focus();

            //TextBox5.Text = Total2.ToString();
        }
        //public void nullvalue()
        //{
        //    gridview.DataSource = null;
        //    gridview.DataBind();
        //}
        //public void record()
        //{
        //    dtt.Columns.Add("Project_#");
        //    dtt.Columns.Add("Project Name");
        //    dtt.Columns.Add("Naration");
        //    ViewState["Cash"] = dtt;
        //    dtt = (DataTable)ViewState["Cash"];
        //    dtt.Rows.Add(" No record available");
        //    gridview.DataSource = dtt;
        //    gridview.DataBind();

        //}
        protected void Button6_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {             
                    con.Open();
                    string QRY = "INSERT INTO User_AC (User_ID,Username,Password,Role) VALUES (@User_Id,@name,@pass,@role)";
                    SqlCommand cmd2 = new SqlCommand(QRY, con);
                    cmd2.CommandType = CommandType.Text;
                    cmd2.Parameters.AddWithValue("@User_Id", TextBox1.Text );
                    cmd2.Parameters.AddWithValue("@name", TextBox2.Text);
                    cmd2.Parameters.AddWithValue("@pass", TextBox3.Text);
                    cmd2.Parameters.AddWithValue("@role", ComboBox1.Text );
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    txtzero();
            }
            code_check();
        }
        protected void grid_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    if (e.Row.RowType == DataControlRowType.DataRow)
            //    {
            //        e.Row.Cells[1].ColumnSpan = 2;
            //        //now make up for the colspan from cell2
            //        e.Row.Cells.RemoveAt(0);
            //    }
            //}

        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            //int rowid = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            //TextBox5.Text = gridview.Rows[rowid].Cells[1].Text;
            //TextBox2.Text = gridview.Rows[rowid].Cells[2].Text;
            //TextBox6.Text = gridview.Rows[rowid].Cells[3].Text;
          

            //dtt = (DataTable)ViewState["Cash"];
            //dtt.Rows.RemoveAt(rowid);
            //gridview.DataSource = dtt;
            //gridview.DataBind();
       
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            //dtt = (DataTable)ViewState["Cash"];
            //int rowid = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            //dtt.Rows.RemoveAt(rowid);
            //gridview.DataSource = dtt;
            //gridview.DataBind();

            //if (gridview.Rows.Count == 0)
            //{
            //    ViewState["Cash"] = dtt;
            //    dtt = (DataTable)ViewState["Cash"];

            //    dtt.Rows.Add(" No record available");
            //    gridview.DataSource = dtt;
            //    gridview.DataBind();
            //    gridview.Rows[0].Cells[1].ColumnSpan = 2;
            //    gridview.Rows[0].Cells.RemoveAt(0);
            //    GlobalClass.id = 0;
            //}
          
        }

        public void loadcombo_list()
        {
            //SqlConnection con = new SqlConnection(cnString);
            //using (con)
            //{
            //    SqlCommand cmd = new SqlCommand("select distinct Catagory from Product ", con);
            //    con.Open();
            //    TextBox6.DataSource = cmd.ExecuteReader();
            //    TextBox6.DataTextField = "Catagory";
            //    TextBox6.DataBind();
            //    con.Close();
            //}
        }
        public void code_check()
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select count(User_ID) from User_AC", con);
                int check_null_db = Convert.ToInt32(cmd.ExecuteScalar());

                if (check_null_db != Convert.ToInt32(null))
                {
                    SqlCommand cmd1 = new SqlCommand("select max(User_ID + 1) from User_AC", con);
                    int main_code = Convert.ToInt32(cmd1.ExecuteScalar());
                    TextBox1.Text = main_code.ToString();
                }
                else
                {
                    TextBox1.Text = "1";
                }
                con.Close();
            }

        }    
        public void txtzero()
        {
            TextBox1.Text = string.Empty;
            TextBox2.Text = string.Empty;
            TextBox3.Text = string.Empty;
            ComboBox1.SelectedIndex = -1;


        }
        protected void Button1_Click(object sender, EventArgs e)
        {

            txtzero();
            code_check();

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(cnString))
            {
                con.Open();
                string QRY = "DELETE FROM User_AC WHERE User_ID='" + TextBox1.Text + "' ";
                SqlCommand cmd = new SqlCommand(QRY, con);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            txtzero();
            code_check();
        }
        protected void Button7_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text != "")
            {
                using (SqlConnection con = new SqlConnection(cnString))
                {
                    con.Open();
                    string QRY1 = "DELETE FROM User_AC WHERE User_ID='" + TextBox1.Text + "' ";
                    SqlCommand cmd = new SqlCommand(QRY1, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    con.Close();

                    con.Open();
                    string QRY = "INSERT INTO User_AC (User_ID,Username,Password,Role) VALUES (@User_Id,@name,@pass,@role)";
                    SqlCommand cmd2 = new SqlCommand(QRY, con);
                    cmd2.CommandType = CommandType.Text;
                    cmd2.Parameters.AddWithValue("@User_Id", TextBox1.Text);
                    cmd2.Parameters.AddWithValue("@name", TextBox2.Text);
                    cmd2.Parameters.AddWithValue("@pass", TextBox3.Text);
                    cmd2.Parameters.AddWithValue("@role", ComboBox1.Text);
                    cmd2.ExecuteNonQuery();
                    con.Close();
                    txtzero();
                }
            }
            txtzero();
            code_check();
        }
        string ab;
        public void search()
        {
            using (SqlConnection CON = new SqlConnection(cnString))
            {
                string QRY = "SELECT User_ID,Username,Password,Role from User_AC  WHERE User_ID = '" + TextBox1.Text + "' ";
                SqlCommand cmd = new SqlCommand(QRY, CON);
                SqlDataAdapter DA = new SqlDataAdapter(cmd);
                DataTable DT = new DataTable();
                DA.Fill(DT);
                
                foreach (DataRow item in DT.Rows)
                {
                    TextBox2.Text = item["Username"].ToString();
                    TextBox3.Text = item["Password"].ToString();
                     ab= item["Role"].ToString();
                    if (ab=="Admin")
                    {
                        ComboBox1.Text = "Admin";
                    }
                    else
                    {
                        ComboBox1.Text = "User";
                    }
                }
               
            }
        }
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            search();
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            
        }

    }
}