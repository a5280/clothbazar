﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Chart_of_account.aspx.cs" Inherits="HAFIZ_BUILDERS.matrix_admin_bt5_master.html.ltr.Chart_of_account" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, Tech No Hub lite admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, Tech No Hub lite design, Tech No Hub lite dashboard bootstrap 5 dashboard template">
    <meta name="description"
        content="Tech No Hub Lite Free Version is powerful and clean admin dashboard template, inpired from Bootstrap Framework">
    <meta name="robots" content="noindex,nofollow">
     <title>Hafiz Builder</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <!-- Custom CSS -->
   
    <link href="../../assets/libs/flot/css/float-chart.css" rel="stylesheet"/>
    <link href="../../dist/css/style.min.css" rel="stylesheet"/>
   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <style>
         @media only screen and  (max-width: 540px) {
             .txt{
                   
                }
             .combo_size{
                 
             }
         }
        @media screen and (min-width:541px) and (max-width: 1366px) {
        .txt {
           
            width:100%;
            
        }
        .list_size{
            
        }
        .codetxt_size{
                 width:57%;
                 height:29px;
                  }
       
    }
        .auto-style25 {
            width: 27%;
        }
        .auto-style34 {
            width: 1%;
        }
        .auto-style37 {
            width: 291px;
        }
        .auto-style38 {
            width: 15%
        }
        .auto-style47 {
            width: 256px;
        }
        .auto-style51 {
            width: 75px;
        }
        .auto-style52 {
            width: 80px;
        }
        .auto-style53 {
            width: 65px;
        }
        .auto-style55 {
            width: 87px;
        }
        .auto-style56 {
            width: 85px;
        }
        .auto-style57 {
            width: 86px;
        }
        .auto-style58 {
            width: 101px;
        }
        .auto-style60 {
            margin-top: 1rem;
            margin-left: 0;
        }
        .auto-style62 {
            width: 92px;
        }
        .auto-style63 {
            width: 5px
        }
        .auto-style64 {
            width: 87px;
            height: 31px;
        }
        .auto-style65 {
            width: 75px;
            height: 31px;
        }
        .auto-style66 {
            width: 80px;
            height: 31px;
        }
        .auto-style67 {
            width: 65px;
            height: 31px;
        }
        .auto-style68 {
            height: 31px;
        }
        .auto-style70 {
            width: 128px;
        }
        .auto-style71 {
            width: 256px;
            height: 27px;
        }
        .auto-style72 {
            width: 291px;
            height: 27px;
        }
        .auto-style73 {
            width: 15%;
            height: 27px;
        }
        .auto-style74 {
            width: 27%;
            height: 27px;
        }
        .auto-style75 {
            width: 1%;
            height: 27px;
        }
        .auto-style76 {
            width: 256px;
            height: 18px;
        }
        .auto-style77 {
            width: 291px;
            height: 18px;
        }
        .auto-style78 {
            width: 15%;
            height: 18px;
        }
        .auto-style79 {
            width: 27%;
            height: 18px;
        }
        .auto-style80 {
            width: 1%;
            height: 18px;
        }
    </style>
  
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b class="logo-icon ps-2">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="../../assets/images/logo-icon.png" alt="homepage" class="light-logo" />

                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text">
                            <!-- dark Logo text -->
                           
                            <span>Hafiz Builder</span>

                        </span>
                        <!-- Logo icon -->
                        <!-- <b class="logo-icon"> -->
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                        <!-- Dark Logo icon -->
                        <!-- <img src="../../assets/images/logo-text.png" alt="homepage" class="light-logo" /> -->

                        <!-- </b> -->
                        <!--End Logo icon -->
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-start me-auto">
                        <li class="nav-item d-none d-lg-block"><a
                                class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)"
                                data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- create new -->
                        <!-- ============================================================== -->
                    <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <span class="d-none d-md-block">Create New User <i class="fa fa-angle-down"></i></span>
                                <span class="d-block d-md-none"><i class="fa fa-plus"></i></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="Create_User.aspx">ADD User</a></li>
                              
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search position-absolute">
                                <input type="text" class="form-control" placeholder="Search &amp; enter"> <a
                                    class="srh-btn"><i class="ti-close"></i></a>
                            </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-end">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-bell font-24"></i>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" id="2" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                 <i class="font-24 mdi mdi-comment-processing"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end mailbox animated bounceInDown" aria-labelledby="2">
                                <ul class="list-style-none">
                                    <li>
                                        <div class="">
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-success btn-circle"><i
                                                            class="ti-calendar"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Event today</h5>
                                                        <span class="mail-desc">Just a reminder that event</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-info btn-circle"><i
                                                            class="ti-settings"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Settings</h5>
                                                        <span class="mail-desc">You can customize this template</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-primary btn-circle"><i
                                                            class="ti-user"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Pavan kumar</h5>
                                                        <span class="mail-desc">Just see the my admin!</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-danger btn-circle"><i
                                                            class="fa fa-link"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Luanch Admin</h5>
                                                        <span class="mail-desc">Just see the my new admin!</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="../../assets/images/users/1.jpg" alt="user" class="rounded-circle" width="31">
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end user-dd animated" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user me-1 ms-1"></i>
                                    My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet me-1 ms-1"></i>
                                    My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email me-1 ms-1"></i>
                                    Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i
                                        class="ti-settings me-1 ms-1"></i> Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i
                                        class="fa fa-power-off me-1 ms-1"></i> Logout</a>
                                <div class="dropdown-divider"></div>
                                <div class="ps-4 p-10"><a href="javascript:void(0)"
                                        class="btn btn-sm btn-success btn-rounded text-white">View Profile</a></div>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
            <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="pt-4">
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link"
                               href="index1.aspx" aria-expanded="false">
                                <i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-chart-line "></i><span class="hide-menu">Accounts</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="Chart_of_account.aspx" class="sidebar-link">
                                        <i class="far fa-user-circle"></i><span class="hide-menu">
                                            Chart of Account
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Cash_Receipt_Voucher.aspx" class="sidebar-link">
                                        <i class="mdi mdi-receipt"></i><span class="hide-menu">
                                            Cash Receipt Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Cash_Payment_Voucher.aspx" class="sidebar-link">
                                        <i class="far fa-credit-card"></i><span class="hide-menu">
                                            Cash Payment Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Bank_Receipt_Voucher.aspx" class="sidebar-link">
                                        <i class="mdi mdi-receipt"></i><span class="hide-menu">
                                            Bank Receipt Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Bankk_Payment_Voucher.aspx" class="sidebar-link">
                                        <i class="far fa-credit-card"></i><span class="hide-menu">
                                            Bank Payment Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Journall_Voucher.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Journal Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Account_Name_Update.aspx" class="sidebar-link">
                                        <i class="fas fa-edit"></i><span class="hide-menu">
                                            Account Name Update
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Bank_Cheque_Transaction.aspx" class="sidebar-link">
                                        <i class="fas fa-money-bill-alt"></i><span class="hide-menu">
                                            Bank Cheque Transaction
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="mdi mdi-chart-bar"></i><span class="hide-menu">Inventory</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="Project.aspx" class="sidebar-link">
                                        <i class="fas fa-building"></i><span class="hide-menu">
                                            Project Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Product.aspx" class="sidebar-link">
                                        <i class="fas fa-boxes"></i><span class="hide-menu">
                                            Product Voucher
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-shopping-cart"></i><span class="hide-menu">Purchase</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="Purchase.aspx" class="sidebar-link">
                                        <i class="fas fa-shopping-basket"></i><span class="hide-menu">
                                            Purchase Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Purchase_Return.aspx" class="sidebar-link">
                                        <i class="fas fa-undo-alt"></i><span class="hide-menu">
                                            Purchase Return Voucher
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-edit"></i><span class="hide-menu">Sale</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="Sale.aspx" class="sidebar-link">
                                        <i class="fab fa-sellsy"></i><span class="hide-menu">
                                            Sale Voucher
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Sale_Return.aspx" class="sidebar-link">
                                        <i class="fas fa-undo-alt"></i><span class="hide-menu">
                                            Sale Return Voucher
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                         <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="far fa-chart-bar"></i><span class="hide-menu">Reports</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">                                                                                           
                                <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Reports
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>


                      <%--  <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="far fa-chart-bar"></i><span class="hide-menu">Cash Reports</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item" >
                                    <a href="Date_form.aspx"  class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"  ></i><span class="hide-menu">
                                            Cash Receive Report
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item" >
                                    <a href="Date_form.aspx"  class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"  ></i><span class="hide-menu">
                                            Cash Payment Report
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item" >
                                    <a href="Date_form.aspx"  class="sidebar-link">
                                        <i class="fas fa-file-medical-alt" ></i><span class="hide-menu">
                                            Bank Receive Report
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Bank Payment Report
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Journal Voucher Report
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="far fa-chart-bar"></i><span class="hide-menu">Purchase Reports</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item" >
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt" ></i><span class="hide-menu">
                                            Purchase Party Wise
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Purchase Product Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Purchase Date Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Project Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Return Party Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Return Product WS
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Return Date Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Purchase Return Project Wise
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                         <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="far fa-chart-bar"></i><span class="hide-menu">Sale Reports</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item" >
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt" ></i><span class="hide-menu">
                                            Sale Party Wise
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Sale Product Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                            Sale Date Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Project Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Return Party Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Return Product WS
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Return Date Wise
                                        </span>
                                    </a>
                                </li>
                                 <li class="sidebar-item">
                                    <a href="Date_form.aspx" class="sidebar-link">
                                        <i class="fas fa-file-medical-alt"></i><span class="hide-menu">
                                           Sale Return Project Wise
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>--%>

                        

                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-wrench"></i><span class="hide-menu">Utilities</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="icon-material.aspx" class="sidebar-link">
                                        <i class="mdi mdi-emoticon"></i><span class="hide-menu">
                                            Material Icons
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="icon-fontawesome.aspx" class="sidebar-link">
                                        <i class="mdi mdi-emoticon-cool"></i><span class="hide-menu">
                                            Font Awesome
                                            Icons
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-cogs"></i><span class="hide-menu">SetUp</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="icon-material.aspx" class="sidebar-link">
                                        <i class="mdi mdi-emoticon"></i><span class="hide-menu">
                                            Material Icons
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="icon-fontawesome.aspx" class="sidebar-link">
                                        <i class="mdi mdi-emoticon-cool"></i><span class="hide-menu">
                                            Font Awesome
                                            Icons
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark"
                               href="javascript:void(0)" aria-expanded="false">
                                <i class="mdi mdi-account-key"></i><span class="hide-menu">Authentication</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="authentication-login.aspx" class="sidebar-link">
                                        <i class="fas fa-user"></i><span class="hide-menu"> Login </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="Create_User.aspx" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i><span class="hide-menu">
                                            Register
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Chart Of Account</h4>
                        <div class="ms-auto text-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- AC-page-1 -->
            <div class="container-fluid" style="margin-top:0px">
                <form id="frm" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                    <div class="row ">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title " style="font-family: pmingLiU-ExtB; font-size: x-large; width: 896px; left: 0px; top: 0px;">Account Entry</h4>
                                    <table class="w-100 mt-5">
                                        <tr>
                                            <td class="auto-style47" colspan="1">
                                                <asp:Label ID="Label26" runat="server" Text="Account #" Width="100px"></asp:Label>
                                            </td>
                                            <td class="auto-style37">
                                                <asp:TextBox ID="TextBox16" runat="server" Width="67px" OnTextChanged="TextBox16_TextChanged"  ></asp:TextBox>
                                                <asp:ImageButton ID="ImageButton1" runat="server" Height="18px" ImageAlign="AbsMiddle" ImageUrl="~/Images/search-512.png" OnClick="ImageButton1_Click" Width="22px" />
                                            </td>
                                            <td class="auto-style38"></td>
                                            <td class="auto-style34">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style47" colspan="1">
                                                <asp:Label ID="Label101" runat="server" Text="Date" Width="100px"></asp:Label>
                                            </td>
                                            <td class="auto-style37">
                                                <asp:TextBox ID="TextBox19" runat="server" onkeypress="pressEnter();return false; " OnTextChanged="TextBox16_TextChanged" Width="89px"></asp:TextBox>
                                                <asp:ImageButton ID="ImageButton2" runat="server" Height="18px" ImageAlign="AbsMiddle" ImageUrl="~/Images/—Pngtree—calendar icon in trendy style_5003062.png" OnClick="ImageButton1_Click" Width="22px" />
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"  Format="yyyy-MM-dd" PopupButtonID="ImageButton2" PopupPosition="BottomRight" TargetControlID="TextBox19" />

                                            </td>
                                            <td class="auto-style38">&nbsp;</td>
                                            <td class="auto-style34">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style47">
                                                <asp:Label ID="Label18" runat="server" Text="Account Name" Width="100px"></asp:Label>
                                            </td>
                                            <td class="auto-style37" colspan="1">
                                                <asp:TextBox ID="TextBox10" runat="server" Width="190px" onkeypress="isNumberKey(evt)"></asp:TextBox>
                                            </td>
                                            <td class="auto-style38">
                                                &nbsp;</td>
                                            <td class="auto-style25">&nbsp;</td>
                                            <td class="auto-style34">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style71">
                                                <asp:Label ID="Label19" runat="server" Text="Email" Width="100px"></asp:Label>
                                            </td>
                                            <td class="auto-style72" colspan="1">
                                                <%-- <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                    <ContentTemplate>--%>
                                                <asp:TextBox ID="TextBox12" runat="server" Width="190px" onkeypress="pressEnter2()"></asp:TextBox>
                                               <%-- </ContentTemplate></asp:UpdatePanel>--%>
                                            </td>
                                            <td class="auto-style73">
                                                
                                            </td>
                                            <td class="auto-style74">
                                                </td>
                                            <td class="auto-style75"></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style47">
                                                <asp:Label ID="Label20" runat="server" Text="Address" Width="100px"></asp:Label>
                                            </td>
                                            <td class="auto-style37" colspan="1">
                                                <asp:TextBox ID="TextBox13" runat="server" Width="190px" onkeypress="pressEnter3()"></asp:TextBox>
                                            </td>
                                            <td class="auto-style38">
                                                &nbsp;</td>
                                            <td class="auto-style25">
                                                &nbsp;</td>
                                            <td class="auto-style34">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style47">
                                                <asp:Label ID="Label21" runat="server" Text="Contact #" Width="100px"></asp:Label>
                                            </td>
                                            <td class="auto-style37" colspan="1">
                                                <asp:TextBox ID="TextBox14" runat="server" Width="190px" onkeypress="pressEnter4()"></asp:TextBox>
                                            </td>
                                            <td class="auto-style38">
                                                &nbsp;</td>
                                            <td class="auto-style25">&nbsp;</td>
                                            <td class="auto-style34">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style47">
                                                <asp:Label ID="Label22" runat="server" Text="NTN #" Width="100px"></asp:Label>
                                            </td>
                                            <td class="auto-style37" colspan="1">
                                                <asp:TextBox ID="TextBox15" runat="server" OnTextChanged="TextBox15_TextChanged" Width="190px" onkeypress="pressEnter5()"></asp:TextBox>
                                            </td>
                                            <td class="auto-style38">
                                                &nbsp;</td>
                                            <td class="auto-style25">&nbsp;</td>
                                            <td class="auto-style34">&nbsp;</td>
                                        </tr>
                                    
                                        <tr>
                                            <td class="auto-style47">
                                                <asp:Label ID="Label102" runat="server" Text="Opening Blc" Width="100px"></asp:Label>
                                            </td>
                                            <td class="auto-style37" colspan="1">
                                                <asp:TextBox ID="TextBox18" runat="server" onkeypress="pressEnter5()" OnTextChanged="TextBox15_TextChanged" Width="100px"></asp:TextBox>
                                                <asp:RadioButton ID="RadioButton1" runat="server" GroupName="gender" Text="DR" />
                                                <asp:RadioButton ID="RadioButton2" runat="server" GroupName="gender" Text="CR" />
                                            </td>
                                            <td class="auto-style38">
                                                &nbsp;</td>
                                            <td class="auto-style25">
                                                &nbsp;</td>
                                            <td class="auto-style34">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style47">
                                                <asp:Label ID="Label103" runat="server" Text="Project Name" Width="100px"></asp:Label>
                                            </td>
                                            <td class="auto-style37" colspan="1">
                                                <ajaxToolkit:ComboBox ID="ComboBox8" runat="server" AutoCompleteMode="SuggestAppend" OnSelectedIndexChanged="ComboBox1_SelectedIndexChanged" Width="160px" >
                                                </ajaxToolkit:ComboBox>
                                            </td>
                                            <td class="auto-style38">&nbsp;</td>
                                            <td class="auto-style25">&nbsp;</td>
                                            <td class="auto-style34">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style47">
                                                <asp:Label ID="Label23" runat="server" Text="Account Type" Width="100px"></asp:Label>
                                            </td>
                                            <td class="auto-style37" colspan="1">
                                                <%--<asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>--%>
                                                        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged2" Width="190px" onkeypress="pressEnter6()" OnTextChanged="DropDownList1_TextChanged" >
                                                        </asp:DropDownList>
                                             <%--       </ContentTemplate> 
                                                </asp:UpdatePanel>--%>
                                            </td>
                                            <td class="auto-style38">
                                                &nbsp;</td>
                                            <td class="auto-style25">
                                                        &nbsp;</td>
                                            <td class="auto-style34">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style47" colspan="4">
                                                <asp:Label ID="Label100" runat="server" CssClass="mt-2"></asp:Label>
                                            </td>
                                            <td class="auto-style37" colspan="1">&nbsp;</td>
                                            <td class="auto-style38">&nbsp;</td>
                                            <td class="auto-style25">&nbsp;</td>
                                            <td class="auto-style34">&nbsp;</td>
                                        </tr>
                                     </table>
                                   <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>--%>
                                           <table class="w-100">
                                                <tr>
                                                    <td class="auto-style64">
                                                        <asp:Button ID="Button1" runat="server" CssClass="mt-3" Text="Save" Width="95px" OnClick="Button1_Click" />
                                                    </td>
                                                    <td class="auto-style65">
                                                        <asp:Button ID="Button2" runat="server" CssClass="mt-3" Text="Update" Width="95px" OnClick="Button2_Click " />
                                                    </td>
                                                    <td class="auto-style66">
                                                        <asp:Button ID="Button3" runat="server" Text="Reset" CssClass="mt-3" Width="95px" OnClick="Button3_Click" />
                                                    </td>
                                                    <td class="auto-style67">
                                                        </td>
                                                    <td class="auto-style68">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style55">&nbsp;</td>
                                                    <td class="auto-style51">&nbsp;</td>
                                                    <td class="auto-style52">&nbsp;</td>
                                                    <td class="auto-style53">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                           </table>
                                    <%--    </ContentTemplate>
                                    </asp:UpdatePanel>--%>
                                </div>
                            </div>
                            <table class="w-100">
                                <tr>
                                    <td class="auto-style70">
                                        <asp:TextBox ID="txt_ppid" runat="server" Visible="False"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="C3" runat="server" Visible="False"></asp:TextBox>
                                        <asp:TextBox ID="txt_level3_id" runat="server" Visible="False"></asp:TextBox>
                                        <asp:TextBox ID="txt_level2_id" runat="server" OnTextChanged="txt_level2_id_TextChanged" Visible="False"></asp:TextBox>
                                        <asp:TextBox ID="txt_level1_id" runat="server" Visible="False"></asp:TextBox>
                                        <asp:TextBox ID="txt_party_id" runat="server" Visible="False"></asp:TextBox>
                                        <asp:TextBox ID="C4" runat="server" Visible="False"></asp:TextBox>
                                        <asp:TextBox ID="txt_level4_id" runat="server" Visible="False"></asp:TextBox>
                                        <asp:TextBox ID="C5" runat="server" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                              
                           
                            </table>
                       <%--      </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </div>
                        <div class="col-md-6 ">                                  
                       <%-- <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                 <ContentTemplate>--%>
                                     <div class="card">
                                         <div class="card-body">                                             
                                              <h4 class="card-title " style="font-family: pmingLiU-ExtB; font-size: x-large; width: 896px; left: 0px; top: 0px;">Codes Entry</h4>
                                              <!-- Main Code-1 -->
                                              <asp:Panel ID="Panel1" runat="server" BorderStyle="Ridge" CssClass=" p-1">
                                        
                                                      <table class="w-100">
                                                            <tr>
                                                                <td class="auto-style56">
                                                                    <asp:Label ID="Label27" runat="server" Text="Main Code" Width="100px"></asp:Label>
                                                                </td>
                                                                <td class="auto-style57">
                                                                    <asp:TextBox ID="TextBox17" runat="server" onkeypress="P1_pressEnter()" ReadOnly="True"></asp:TextBox>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="auto-style56">
                                                                    <asp:Label ID="Label28" runat="server" Text="Description" Width="100px"></asp:Label>
                                                                </td>
                                                                <td class="auto-style57">
                                                                    <ajaxToolkit:ComboBox ID="ComboBox1" runat="server" CssClass="txt" OnSelectedIndexChanged="ComboBox1_SelectedIndexChanged" AutoCompleteMode="SuggestAppend" ></ajaxToolkit:ComboBox>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        <table class="w-100">
                                                        
                                                       
                                                            <tr>
                                                                <td class="auto-style58">
                                                                    &nbsp;</td>
                                                                <td class="auto-style62">
                                                                    <asp:Button ID="Button4" runat="server" CssClass="auto-style60" Text="Save" Width="88px" OnClick="Button4_Click" />
                                                                </td>
                                                                <td class="auto-style63">&nbsp;</td>
                                                                <td>
                                                                    <asp:Button ID="Button5" runat="server" CssClass="mt-3" Text="Reset" Width="95px" OnClick="Button5_Click" />
                                                                </td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        
                                                        </table>
                                                        <br />
                                                </asp:Panel>
                                              <!-- Sub Code-1 -->
                                              <asp:Panel ID="Panel2" runat="server" BorderStyle="Ridge" CssClass="p-10 mt-1">
                                                 <table class="w-100">
                                                    <tr>
                                                        <td class="auto-style56">
                                                            <asp:Label ID="Label1" runat="server" Text="Sub Code" Width="100px"></asp:Label>
                                                        </td>
                                                        <td class="auto-style57">
                                                            <asp:TextBox ID="TextBox1" runat="server" ReadOnly="True" ></asp:TextBox>
  
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style56">
                                                            <asp:Label ID="Label2" runat="server" Text="Description" Width="100px"></asp:Label>
                                                        </td>
                                                        <td class="auto-style57">
                                                            <ajaxToolkit:ComboBox ID="ComboBox2" runat="server"   Height="29px" AutoCompleteMode="SuggestAppend" ></ajaxToolkit:ComboBox>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                         <td class="auto-style56">
                                                             <asp:Label ID="Label29" runat="server" Text="Main Des.." Width="100px"></asp:Label>
                                                         </td>
                                                         <td class="auto-style57">
                                                             <ajaxToolkit:ComboBox ID="ComboBox6" runat="server" Height="29px" AutoCompleteMode="SuggestAppend">
                                                             </ajaxToolkit:ComboBox>
                                                         </td>
                                                         <td>&nbsp;</td>
                                                         <td>&nbsp;</td>
                                                     </tr>
                                                </table>
                                                <table class="w-100">
                                                    <tr>
                                                        <td class="auto-style58">
                                                            &nbsp;</td>
                                                        <td class="auto-style62">
                                                            <asp:Button ID="Button6" runat="server" CssClass="auto-style60" Text="Save" Width="88px" OnClick="Button6_Click" style="height: 29px" />
                                                        </td>
                                                        <td class="auto-style63">&nbsp;</td>
                                                        <td>
                                                            <asp:Button ID="Button7" runat="server" CssClass="mt-3" Text="Reset" Width="95px" OnClick="Button7_Click" />
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                                <br />                                     
                                                </asp:Panel>
                                              <!-- Sub Code-1 -->
                                              <asp:Panel ID="Panel3" runat="server" BorderStyle="Ridge" CssClass="p-10 mt-1">
                                                  <table class="w-100">
                                                    <tr>
                                                        <td class="auto-style56">
                                                            <asp:Label ID="Label3" runat="server" Text="Control Code" Width="100px"></asp:Label>
                                                        </td>
                                                        <td class="auto-style57">
                                                            <asp:TextBox ID="TextBox2" runat="server" ReadOnly="True"></asp:TextBox>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style56">
                                                            <asp:Label ID="Label4" runat="server" Text="Description" Width="100px"></asp:Label>
                                                        </td>
                                                        <td class="auto-style57">
                                                            <ajaxToolkit:ComboBox ID="ComboBox3" runat="server"   Height="29px" AutoCompleteMode="SuggestAppend" ></ajaxToolkit:ComboBox>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                         <td class="auto-style56">
                                                             <asp:Label ID="Label5" runat="server" Text="Sub Des.." Width="100px"></asp:Label>
                                                         </td>
                                                         <td class="auto-style57">
                                                             <ajaxToolkit:ComboBox ID="ComboBox4" runat="server" Height="29px" AutoCompleteMode="SuggestAppend">
                                                             </ajaxToolkit:ComboBox>
                                                         </td>
                                                         <td>&nbsp;</td>
                                                         <td>&nbsp;</td>
                                                     </tr>
                                                </table>
                                                <table class="w-100">
                                                    <tr>
                                                        <td class="auto-style58">
                                                            &nbsp;</td>
                                                        <td class="auto-style62">
                                                            <asp:Button ID="Button8" runat="server" CssClass="auto-style60" Text="Save" Width="88px" OnClick="Button8_Click" />
                                                        </td>
                                                        <td class="auto-style63">&nbsp;</td>
                                                        <td>
                                                            <asp:Button ID="Button9" runat="server" CssClass="mt-3" Text="Reset" Width="95px" OnClick="Button9_Click" />
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style58">&nbsp;</td>
                                                        <td class="auto-style62">&nbsp;</td>
                                                        <td class="auto-style63">&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                  
                                                </table>
                                             </asp:Panel>  
                                             
                                               <asp:Panel ID="Panel4" runat="server" BorderStyle="Ridge" CssClass="p-10 mt-1">
                                                  <table class="w-100">
                                                    <tr>
                                                        <td class="auto-style56">
                                                            <asp:Label ID="Label6" runat="server" Text="Sub Control" Width="100px"></asp:Label>
                                                        </td>
                                                        <td class="auto-style57">
                                                            <asp:TextBox ID="TextBox3" runat="server" ReadOnly="True"></asp:TextBox>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style56">
                                                            <asp:Label ID="Label7" runat="server" Text="Description" Width="100px"></asp:Label>
                                                        </td>
                                                        <td class="auto-style57">
                                                            <ajaxToolkit:ComboBox ID="ComboBox5" runat="server"   Height="29px" AutoCompleteMode="SuggestAppend" ></ajaxToolkit:ComboBox>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                         <td class="auto-style56">
                                                             <asp:Label ID="Label8" runat="server" Text="Control Des.." Width="100px"></asp:Label>
                                                         </td>
                                                         <td class="auto-style57">
                                                             <ajaxToolkit:ComboBox ID="ComboBox7" runat="server" Height="29px" AutoCompleteMode="SuggestAppend">
                                                             </ajaxToolkit:ComboBox>
                                                         </td>
                                                         <td>&nbsp;</td>
                                                         <td>&nbsp;</td>
                                                     </tr>
                                                </table>
                                                <table class="w-100">
                                                    <tr>
                                                        <td class="auto-style58">
                                                            &nbsp;</td>
                                                        <td class="auto-style62">
                                                            <asp:Button ID="Button11" runat="server" CssClass="auto-style60" Text="Save" Width="88px" OnClick="Button11_Click" />
                                                        </td>
                                                        <td class="auto-style63">&nbsp;</td>
                                                        <td>
                                                            <asp:Button ID="Button12" runat="server" CssClass="mt-3" Text="Reset" Width="95px" OnClick="But_Click" />
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style58">&nbsp;</td>
                                                        <td class="auto-style62">&nbsp;</td>
                                                        <td class="auto-style63">&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style58">&nbsp;</td>
                                                        <td class="auto-style62" colspan="3">
                                                            <asp:Button ID="Button10" runat="server" OnClick="Button10_Click1" Text="Reset All" Width="195px" />
                                                        </td>
                                                        <td class="auto-style63">&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                             </asp:Panel>   
                                                                          
                                          </div>
                                     </div>
                       <%--     </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </div>
                    </div> 
                    </ContentTemplate>
                    </asp:UpdatePanel>     
                 </form>
            </div>
            <!-- End Charts -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Admin Designed and Developed by TECH<b>NO</b>HUB.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script type="text/javascript">
    //function pressEnter() {
    //  // Key Code for ENTER = 13
    //  if ((event.keyCode == 13)) {
    //      // document.getElementById("TextBox10").focus({preventScroll:false});
    //    //  PageMethods.search();
    //  }
        //}
        function isNumberKey(evt)
        {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

    function pressEnter1() {
        if ((event.keyCode == 13)) {
            document.getElementById("TextBox12").focus({ preventScroll: false });
        }
    }
    function pressEnter2() {
        if ((event.keyCode == 13)) {
            document.getElementById("TextBox13").focus({ preventScroll: false });
        }
    }
    function pressEnter3() {
        if ((event.keyCode == 13)) {
            document.getElementById("TextBox14").focus({ preventScroll: false });
        }
    }
    function pressEnter4() {
      if ((event.keyCode == 13)) {
          document.getElementById("TextBox15").focus({ preventScroll: false });
      }
    }
    function pressEnter5() {
        if ((event.keyCode == 13)) {
            document.getElementById("DropDownList1").focus({ preventScroll: false });
        }
    }
    function pressEnter6() {
        if ((event.keyCode == 13)) {
            document.getElementById("Button2").focus({ preventScroll: false });
        }
    }
    function P1_pressEnter() {
        if ((event.keyCode == 13)) {
            document.getElementById("ComboBox1").focus({ preventScroll: false });
        }
    }
    function P1_pressEnter7() {
        if ((event.keyCode == 13)) {
            document.getElementById("Button2").focus({ preventScroll: false });
        }
    }
    function P1_pressEnter6_btn_s() {
        if ((event.keyCode == 13)) {
            document.getElementById("Button2").focus({ preventScroll: false });
        }
    }
    function search_key() {
        if ((event.keyCode == 113)) {
            PageMethods.search();
        }
    }
    </script>

   
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <!-- this page js -->
   <script src="../../assets/libs/chart/matrix.interface.js"></script>
    <script src="../../assets/libs/chart/excanvas.min.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.pie.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.time.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.stack.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.crosshair.js"></script>
    <script src="../../assets/libs/chart/jquery.peity.min.js"></script>
    <script src="../../assets/libs/chart/matrix.charts.js"></script>
    <script src="../../assets/libs/chart/jquery.flot.pie.min.js"></script>
    <script src="../../assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="../../assets/libs/chart/turning-series.js"></script>
    <script src="../../dist/js/pages/chart/chart-page-init.js"></script>
</body>

</html>
