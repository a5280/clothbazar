﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HafizBuilder.Startup))]
namespace HafizBuilder
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
