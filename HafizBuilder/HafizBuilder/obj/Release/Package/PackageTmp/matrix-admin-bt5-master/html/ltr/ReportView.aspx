﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportView.aspx.cs" Inherits="HafizBuilder.matrix_admin_bt5_master.html.ltr.ReportView" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report View</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" Visible="False" />
        <asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="TextBox2" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="TextBox3" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="TextBox4" runat="server" Visible="False"></asp:TextBox>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="True" GroupTreeImagesFolderUrl="" Height="50px" ReuseParameterValuesOnRefresh="True" ToolbarImagesFolderUrl="" ToolPanelWidth="200px" Width="350px" OnDisposed="CrystalReportViewer1_Disposed" />
    
    </div>
    </form>
</body>
</html>
<a href="ReportView.aspx">ReportView.aspx</a>