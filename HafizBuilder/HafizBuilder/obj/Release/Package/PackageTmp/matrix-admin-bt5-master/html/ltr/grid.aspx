﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="grid.aspx.cs" Inherits="HAFIZ_BUILDERS.matrix_admin_bt5_master.html.ltr.grid" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, Matrix lite admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, Matrix admin lite design, Matrix admin lite dashboard bootstrap 5 dashboard template">
    <meta name="description"
        content="Matrix Admin Lite Free Version is powerful and clean admin dashboard template, inpired from Bootstrap Framework">
    <meta name="robots" content="noindex,nofollow">
    <title>Matrix Admin Lite Free Versions Template by WrapPixel</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <style type="text/css">
        .auto-style1 {
            width: 72px;
        }
        .auto-style2 {
            width: 1px;
        }
        .auto-style3 {
            width: 71px;
        }
        .auto-style4 {
            width: 228px;
        }
        .auto-style5 {
            width: 164px;
        }
        .auto-style6 {
            width: 100px;
        }
        .auto-style7 {
            width: 59px;
        }
        .auto-style8 {
            width: 93px;
        }
        .auto-style9 {
            width: 17px;
        }
        .auto-style10 {
            width: 41px;
        }
        .auto-style12 {
            width: 162px;
        }
        .auto-style13 {
            width: 99px;
        }
        .auto-style15 {
            width: 163px;
        }
        .auto-style19 {
            width: 120px;
        }
        .auto-style20 {
            width: 135px;
        }
        .auto-style21 {
            width: 126px;
        }
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="index.aspx">
                        <!-- Logo icon -->
                        <b class="logo-icon ps-2">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="../../assets/images/logo-icon.png" alt="homepage" class="light-logo" />

                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text">
                            <!-- dark Logo text -->
                            <img src="../../assets/images/logo-text.png" alt="homepage" class="light-logo" />

                        </span>
                        <!-- Logo icon -->
                        <!-- <b class="logo-icon"> -->
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                        <!-- Dark Logo icon -->
                        <!-- <img src="../../assets/images/logo-text.png" alt="homepage" class="light-logo" /> -->

                        <!-- </b> -->
                        <!--End Logo icon -->
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-start me-auto">
                        <li class="nav-item d-none d-lg-block"><a
                                class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)"
                                data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- create new -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <span class="d-none d-md-block">Create New <i class="fa fa-angle-down"></i></span>
                                <span class="d-block d-md-none"><i class="fa fa-plus"></i></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search position-absolute">
                                <input type="text" class="form-control" placeholder="Search &amp; enter"> <a
                                    class="srh-btn"><i class="ti-close"></i></a>
                            </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-end">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-bell font-24"></i>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" id="2" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                 <i class="font-24 mdi mdi-comment-processing"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end mailbox animated bounceInDown" aria-labelledby="2">
                                <ul class="list-style-none">
                                    <li>
                                        <div class="">
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-success btn-circle"><i
                                                            class="ti-calendar"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Event today</h5>
                                                        <span class="mail-desc">Just a reminder that event</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-info btn-circle"><i
                                                            class="ti-settings"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Settings</h5>
                                                        <span class="mail-desc">You can customize this template</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-primary btn-circle"><i
                                                            class="ti-user"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Pavan kumar</h5>
                                                        <span class="mail-desc">Just see the my admin!</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-danger btn-circle"><i
                                                            class="fa fa-link"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Luanch Admin</h5>
                                                        <span class="mail-desc">Just see the my new admin!</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="../../assets/images/users/1.jpg" alt="user" class="rounded-circle" width="31">
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end user-dd animated" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user me-1 ms-1"></i>
                                    My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet me-1 ms-1"></i>
                                    My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email me-1 ms-1"></i>
                                    Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i
                                        class="ti-settings me-1 ms-1"></i> Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i
                                        class="fa fa-power-off me-1 ms-1"></i> Logout</a>
                                <div class="dropdown-divider"></div>
                                <div class="ps-4 p-10"><a href="javascript:void(0)"
                                        class="btn btn-sm btn-success btn-rounded text-white">View Profile</a></div>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="pt-4">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="index.aspx" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span
                                    class="hide-menu">Dashboard</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="charts.aspx" aria-expanded="false"><i class="mdi mdi-chart-bar"></i><span
                                    class="hide-menu">Charts</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="widgets.aspx" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span
                                    class="hide-menu">Widgets</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="tables.aspx" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span
                                    class="hide-menu">Tables</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="grid.aspx" aria-expanded="false"><i class="mdi mdi-blur-linear"></i><span
                                    class="hide-menu">Full Width</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark"
                                href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span
                                    class="hide-menu">Forms </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="form-basic.aspx" class="sidebar-link"><i
                                            class="mdi mdi-note-outline"></i><span class="hide-menu"> Form Basic
                                        </span></a></li>
                                <li class="sidebar-item"><a href="form-wizard.aspx" class="sidebar-link"><i
                                            class="mdi mdi-note-plus"></i><span class="hide-menu"> Form Wizard
                                        </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="pages-buttons.aspx" aria-expanded="false"><i
                                    class="mdi mdi-relative-scale"></i><span class="hide-menu">Buttons</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark"
                                href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-face"></i><span
                                    class="hide-menu">Icons </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="icon-material.aspx" class="sidebar-link"><i
                                            class="mdi mdi-emoticon"></i><span class="hide-menu"> Material Icons
                                        </span></a></li>
                                <li class="sidebar-item"><a href="icon-fontawesome.aspx" class="sidebar-link"><i
                                            class="mdi mdi-emoticon-cool"></i><span class="hide-menu"> Font Awesome
                                            Icons </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="pages-elements.aspx" aria-expanded="false"><i class="mdi mdi-pencil"></i><span
                                    class="hide-menu">Elements</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark"
                                href="javascript:void(0)" aria-expanded="false"><i
                                    class="mdi mdi-move-resize-variant"></i><span class="hide-menu">Addons </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="index2.aspx" class="sidebar-link"><i
                                            class="mdi mdi-view-dashboard"></i><span class="hide-menu"> Dashboard-2
                                        </span></a></li>
                                <li class="sidebar-item"><a href="pages-gallery.aspx" class="sidebar-link"><i
                                            class="mdi mdi-multiplication-box"></i><span class="hide-menu"> Gallery
                                        </span></a></li>
                                <li class="sidebar-item"><a href="pages-calendar.aspx" class="sidebar-link"><i
                                            class="mdi mdi-calendar-check"></i><span class="hide-menu"> Calendar
                                        </span></a></li>
                                <li class="sidebar-item"><a href="pages-invoice.aspx" class="sidebar-link"><i
                                            class="mdi mdi-bulletin-board"></i><span class="hide-menu"> Invoice
                                        </span></a></li>
                                <li class="sidebar-item"><a href="pages-chat.aspx" class="sidebar-link"><i
                                            class="mdi mdi-message-outline"></i><span class="hide-menu"> Chat Option
                                        </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark"
                                href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-key"></i><span
                                    class="hide-menu">Authentication </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="authentication-login.aspx" class="sidebar-link"><i
                                            class="mdi mdi-all-inclusive"></i><span class="hide-menu"> Login </span></a>
                                </li>
                                <li class="sidebar-item"><a href="authentication-register.aspx" class="sidebar-link"><i
                                            class="mdi mdi-all-inclusive"></i><span class="hide-menu"> Register
                                        </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark"
                                href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-alert"></i><span
                                    class="hide-menu">Errors </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="error-403.aspx" class="sidebar-link"><i
                                            class="mdi mdi-alert-octagon"></i><span class="hide-menu"> Error 403
                                        </span></a></li>
                                <li class="sidebar-item"><a href="error-404.aspx" class="sidebar-link"><i
                                            class="mdi mdi-alert-octagon"></i><span class="hide-menu"> Error 404
                                        </span></a></li>
                                <li class="sidebar-item"><a href="error-405.aspx" class="sidebar-link"><i
                                            class="mdi mdi-alert-octagon"></i><span class="hide-menu"> Error 405
                                        </span></a></li>
                                <li class="sidebar-item"><a href="error-500.aspx" class="sidebar-link"><i
                                            class="mdi mdi-alert-octagon"></i><span class="hide-menu"> Error 500
                                        </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item p-3">
                            <a href="https://github.com/wrappixel/matrix-admin-bt5" target="_blank" class="w-100 btn btn-cyan d-flex align-items-center text-white"><i class="mdi mdi-cloud-download font-20 me-2"></i>Download Free</a>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                       <h4 class="page-title "  style="font-family: PMingLiU-ExtB; font-size: x-large; font-weight: 500; font-style: inherit; font-variant: normal">Journal Voucher</h4>
                        <div class="ms-auto text-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
                
                <div class="container-fluid" style="margin-top:0px">
                <form id="frm" runat="server">
                    <div class="row ">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title " style="font-family: pmingLiU-ExtB; font-size: x-large;">Payment Receive &amp; Pay</h4>
                                </div>
                                <div class="comment-widgets scrollable table-responsive-sm">
                                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <table class="w-100 ">
                                                <tr >                                       
                                                    <td class="auto-style9"></td>
                                                    <td class="auto-style8" colspan="1">
                                                        <asp:Label ID="Label1" Text="Date"    runat="server" Font-Bold="True" Font-Italic="False" Font-Names="PMingLiU-ExtB" Font-Size="13pt"/>
                                                    </td>
                                                    <td class="auto-style15" colspan="2">
                                                        <asp:TextBox ID="TextBox1" runat="server"   onkeypress="pressEnter()"  />
                                                        <asp:ImageButton ID="But" runat="server" Text="" Font-Bold="True" ImageUrl="~/Images/—Pngtree—calendar icon in trendy style_5003062.png" Width="25px" Height="25px" CssClass="auto-style4" ImageAlign="TextTop"  />
                                        <%--                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"  Format="dd/MM/yyyy" PopupButtonID="But" PopupPosition="BottomRight" TargetControlID="TextBox1" />--%>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="But" PopupPosition="BottomRight" TargetControlID="TextBox1"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                       
                                                    <td class="auto-style9"></td>
                                                    <td class="auto-style8" colspan="1">
                                                        <asp:Label ID="Label5" Text="Voc#"   runat="server" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt"/>
                                                    </td>
                                                    <td class="auto-style28" colspan="9">
                                                        <asp:TextBox ID="TextBox5" runat="server"   onkeypress="pressEnter1()" />
                                                    </td>                                       
                                                    <td class="auto-style56"></td>                                       
                                                    <td class="auto-style56"></td>                                        
                                                </tr>
                                                <tr>                                      
                                                    <td class="auto-style9"></td>
                                                    <td class="auto-style8" colspan="1">
                                                        <asp:Label ID="Label7" Text="Party Name"   runat="server" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt"/>
                                                    </td>
                                                    <td class="auto-style5" colspan="1">
                                                        <ajaxToolkit:ComboBox ID="ComboBox1" runat="server" onkeypress="pressEnter2()" >
                                                            <asp:ListItem>London</asp:ListItem>
                                                            <asp:ListItem>Pakistan</asp:ListItem>
                                                            <asp:ListItem></asp:ListItem>
                                                        </ajaxToolkit:ComboBox>
                                                    </td>
                                                    <td class="auto-style2">
                                                        <asp:Label ID="Label2" Text="Naration"   runat="server" CssClass="m-l-15" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt"/>
                                                    </td>
                                                    <td class="auto-style2">
                                                        <asp:TextBox ID="TextBox2" runat="server" onkeypress="pressEnter3()"/>
                                                    </td>
                                                    <td class="auto-style3">
                                                        <asp:Label ID="Label3" Text="Invoice"  runat="server" CssClass="m-l-15" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt"/>
                                                    </td>
                                                    <td class="auto-style1">
                                                        <asp:TextBox ID="TextBox3" runat="server"  onkeypress="pressEnter4()" Width="60px"/>
                                                    </td>
                                                    <td class="auto-style7">
                                                        <asp:Label ID="Label4" Text="Debit"   runat="server" CssClass="m-l-15" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt"/>
                                                    </td>
                                                    <td class="auto-style6">
                                                        <asp:TextBox ID="TextBox4" runat="server" OnKeyDown="TextBox4_KeyDown" onkeypress="pressEnter5()" Width="90px"  />
                                                    </td>
                                                    <td class="auto-style10">
                                                        <asp:Label ID="Label8" runat="server" CssClass="m-l-15" Text="Credit" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt"></asp:Label>
                                                    </td>
                                                    <td class="auto-style57">
                                                        <asp:TextBox ID="TextBox7" runat="server" Width="90px" onkeypress="pressEnter6()"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            
                                            <asp:GridView  ID="GridView1" runat="server"  ShowHeaderWhenEmpty="True"
                                                AutoGenerateColumns="False" onrowdeleting="RowDeleting"
                                                OnRowCancelingEdit="cancelRecord" OnRowEditing="editRecord"
                                                OnRowUpdating="updateRecord" CellPadding="4" Width="95%" GridLines="None" 
                                                ForeColor="#333333" CssClass="mt-1 m-l-20"  Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="12pt"
                                                    >
                                                <RowStyle BackColor="#E3EAEB" HorizontalAlign="Center" />
                                                <AlternatingRowStyle BackColor="White"/>
 
                                                <Columns>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Voc_#</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Label ID ="lblSr" runat="server" Text='<%#Bind("Voc")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle  Width="90px"/>
                                                    </asp:TemplateField>
                               
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Party_Name</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Label ID ="lblName" runat="server" Text='<%#Bind("Party") %>'></asp:Label>
                                                        </ItemTemplate>
                                                            <HeaderStyle   />
                                                        <EditItemTemplate>
                                                        <asp:TextBox ID="txtName" runat="server" Text='<%#Bind("Party") %>' MaxLength="50"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtName" runat="server" Text="*" ToolTip="Enter name" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revtxtName" runat="server" Text="*" ToolTip="Enter alphabate " ControlToValidate="txtName" ValidationExpression="^[a-zA-Z'.\s]{1,40}$"></asp:RegularExpressionValidator> 
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                        <asp:TextBox ID="txtNewName" runat="server" MaxLength="50"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtNewName" runat="server" Text="*" ToolTip="Enter name" ControlToValidate="txtNewName"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revtxtNewName" runat="server" Text="*" ToolTip="Enter alphabate " ControlToValidate="txtNewName" ValidationExpression="^[a-zA-Z'.\s]{1,40}$"></asp:RegularExpressionValidator>          
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Naration</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Label ID = "lblCountry" runat="server" Text='<%#Bind("DISCRIPTION") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                        <asp:TextBox ID="txtCountry" runat="server" Text='<%#Bind("DISCRIPTION") %>' MaxLength="20"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtCountry" runat="server" Text="*" ToolTip="Enter country" ControlToValidate="txtCountry"></asp:RequiredFieldValidator>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                        <asp:TextBox ID="txtNewCountry" runat="server" MaxLength="20"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtNewCountry" runat="server" Text="*" ToolTip="Enter country" ControlToValidate="txtNewCountry"></asp:RequiredFieldValidator>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
       
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Invoice</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Label ID = "lblSalary" runat="server" Text='<%#Bind("INVOICE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                        <asp:TextBox ID="txtSalary" runat="server" Text='<%#Bind("INVOICE") %>'  MaxLength="10"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtSalary" runat="server" Text="*"  ToolTip="Enter salary" ControlToValidate="txtSalary"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revtxtSalary" runat="server" Text="*" ToolTip="Enter numeric value" ControlToValidate="txtSalary" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>        
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                        <asp:TextBox ID="txtNewSalary" runat="server" MaxLength="10"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtNewSalary" runat="server" Text="*"  ToolTip="Enter salary" ControlToValidate="txtNewSalary"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revtxtNewSalary" runat="server" Text="*" ToolTip="Enter numeric value" ControlToValidate="txtNewSalary" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Amount</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Label ID = "lblAmt" runat="server" Text='<%#Bind("Amount") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                        <asp:TextBox ID="txtAmt" runat="server" Text='<%#Bind("Amount") %>'  MaxLength="10"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtAmt" runat="server" Text="*"  ToolTip="Enter salary" ControlToValidate="txtAmt"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revtxtAmt" runat="server" Text="*" ToolTip="Enter numeric value" ControlToValidate="txtAmt" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
          
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                        <asp:TextBox ID="txtNewAmt" runat="server" MaxLength="10"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtNewAmt" runat="server" Text="*"  ToolTip="Enter Amt" ControlToValidate="txtNewAmt"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revtxtNewAmt" runat="server" Text="*" ToolTip="Enter numeric value" ControlToValidate="txtNewAmt" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
 
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Operation</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Button ID="btnEdit" runat="server" CommandName="Edit" Text="Edit" />
                                                        <asp:Button ID="btnDelete" runat="server" CommandName="Delete" Text="Delete" CausesValidation="true" OnClientClick="return confirm('Are you sure?')" />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                        <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Update" />
                                                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Cancel" CausesValidation="false" />
                                                        </EditItemTemplate>
 
                                                        <FooterTemplate>
                                                        <asp:Button ID="btnNewInsert" runat="server" Text="Insert" OnClick="InsertNewRecord"/>
                                                        <asp:Button ID="btnNewCancel" runat="server" Text="Cancel" OnClick="AddNewCancel" CausesValidation="false" />
                                                        </FooterTemplate>        
                                                    </asp:TemplateField>           
                                                </Columns>

                                                <EditRowStyle BackColor="#7C6F57" />
                                                <EmptyDataTemplate> No record available</EmptyDataTemplate>       
                                                <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                                                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                                <RowStyle HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
 
                                                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                                                <SortedAscendingHeaderStyle BackColor="#246B61" />
                                                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                                                <SortedDescendingHeaderStyle BackColor="#15524A" />
 
                                            </asp:GridView>
                           
                                            <asp:TextBox ID="TextBox6" runat="server" CssClass="m-l-5 m-r-40 mt-2" Style="float:right" OnTextChanged="TextBox6_TextChanged" Width="90px"></asp:TextBox>
                                            <asp:Label ID="Label6" Text="Total Credit"   runat="server" CssClass="m-r-5 mt-2" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt" Style="float:right"/>
                                             <asp:TextBox ID="TextBox8" runat="server" CssClass="m-l-5 m-r-40 mt-2" Style="float:right" OnTextChanged="TextBox6_TextChanged" Width="90px"></asp:TextBox>
                                            <asp:Label ID="Label9" Text="Total Debit"   runat="server" CssClass="m-r-5 mt-2" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt" Style="float:right"/>
                                           
                                            <br />
                                            <br />
                           
                                            <table class="w-100 mt-5">
                                               
                                                <tr>
                                                    <td class="auto-style200"></td>
                                                    <td class="auto-style20">&nbsp;</td>
                                                    <td class="auto-style12">                          
                                                        <asp:Button ID="btnAdd" runat="server" Text="Add New Record" OnClick="AddNewRecord" BackColor="#1C5E55" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt" Font-Strikeout="False" Font-Underline="False" ForeColor="White" Height="30px" Width="159px" />  
                                                    </td>
                                                    <td class="auto-style13">  
                                                        <asp:Button ID="btninsert" runat="server" Text="Insert" OnClick="btninsert_Click" BackColor="#1C5E55" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt" Font-Strikeout="False" Font-Underline="False" ForeColor="White" Height="30px" Width="113px"/>
                                                    </td>
                                                    <td class="auto-style19">
                                                        <asp:Button ID="Button1" runat="server" Text="Reset" OnClick="Button1_Click" BackColor="#1C5E55" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt" Font-Strikeout="False" Font-Underline="False" ForeColor="White" Height="30px" Width="120px" />  
                                                    </td>
                                                    <td class="auto-style21">  
                                                        <asp:Button ID="Button3" runat="server" Text="Print" BackColor="#1C5E55" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt" Font-Strikeout="False" Font-Underline="False" ForeColor="White" Height="30px"  Width="117px" />  
                                                    </td>
                                                    <td class="auto-style71">&nbsp;</td>
                                                    <td class="auto-style72"></td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div> 
                        </div>
                    </div>
                </form>
            </div>


            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Right sidebar -->
            <!-- ============================================================== -->
            <!-- .right-sidebar -->
            <!-- ============================================================== -->
            <!-- End Right sidebar -->
            <!-- ============================================================== -->
      
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
             <footer class="footer text-center">
                All Rights Reserved by Admin Designed and Developed by TECH<b>NO</b>HUB.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
   
    <script type="text/javascript">
    function pressEnter() {
      // Key Code for ENTER = 13
      if ((event.keyCode == 13)) {
        document.getElementById("TextBox5").focus({preventScroll:false});
      }
    }
    function pressEnter1() {
        if ((event.keyCode == 13)) {
            document.getElementById("ComboBox1").focus({ preventScroll: false });
        }
    }
    function pressEnter2() {
        if ((event.keyCode == 13)) {
            document.getElementById("TextBox2").focus({ preventScroll: false });
        }
    }
    function pressEnter3() {
        if ((event.keyCode == 13)) {
            document.getElementById("TextBox3").focus({ preventScroll: false });
        }
    }
    function pressEnter4() {
      if ((event.keyCode == 13)) {
          document.getElementById("TextBox4").focus({ preventScroll: false });
      }
    }
    function pressEnter5() {
        if ((event.keyCode == 13)) {
            document.getElementById(" TextBox7").focus({ preventScroll: false });
        }
    }
    function pressEnter6() {
        if ((event.keyCode == 13)) {
            document.getElementById("btninsert").focus({ preventScroll: false });
        }
    }
    </script>
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
</body>

</html>