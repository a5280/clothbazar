﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Bank_Payment_Voucher.aspx.cs" Inherits="HAFIZ_BUILDERS.matrix_admin_bt5_master.html.ltr.Bank_Payment_Voucher" %>


<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<!DOCTYPE html>

<html dir="ltr" lang="en">

<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, Matrix lite admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, Matrix admin lite design, Matrix admin lite dashboard bootstrap 5 dashboard template">
    <meta name="description"
        content="Matrix Admin Lite Free Version is powerful and clean admin dashboard template, inpired from Bootstrap Framework">
    <meta name="robots" content="noindex,nofollow">
    <title>Matrix Admin Lite Free Versions Template by WrapPixel</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">

    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  
    <style>

         @media screen and (min-width:0px) and (max-width: 540px) {
           .txt {
                width:auto;
                height: 29px;
                margin: 0 0 0 20px;
            }
            #GridView1{
                width:10%;
            }
            .smtxtsize{
                width:100%;
                height:auto;
            }
        }
        @media screen and (min-width:541px) and (max-width: 1366px) {
            .txt {
                width: 15%;
                height: 29px;
                margin: 0 0 0 20px;
            }
            #GridView1{
                width:95%;
            }
        }
        
        .auto-style4 {
            margin-bottom: 0px;
        }
                
        .auto-style15 {
            width: 23%;
            height: 35px;
        }
        .auto-style47 {
            height: 23px;
        }
        
        .auto-style48 {
            width: 139px;
            height: 26px;
        }
        .auto-style49 {
            width: 75px;
            height: 26px;
        }
        .auto-style50 {
            width: 106px;
            height: 26px;
        }
        .auto-style51 {
            width: 125px;
            height: 26px;
        }
        .auto-style52 {
            width: 177px;
            height: 26px;
        }
        .auto-style53 {
            margin-right: 8px;
            margin-left: 0;
        }
        
        .auto-style54 {
            height: 35px;
            width: 2%;
        }
        .auto-style59 {
            width: 4%;
            height: 23px;
            text-align: right;
        }
        .auto-style62 {
            height: 31px;
            width: 2%;
        }
        .auto-style64 {
            width: 2%
        }
        .auto-style65 {
            width: 2%;
            height: 23px;
        }
        .auto-style67 {
            width: 177px;
            height: 32px;
        }
        .auto-style68 {
            width: 139px;
            height: 32px;
        }
        .auto-style69 {
            width: 75px;
            height: 32px;
        }
        .auto-style70 {
            width: 106px;
            height: 32px;
        }
        .auto-style71 {
            width: 125px;
            height: 32px;
        }
        .auto-style72 {
            height: 32px;
        }
                
        .auto-style75 {
            height: 31px;
            width: 1%;
        }
        .auto-style76 {
            font-weight: bold;
        }
        
        .auto-style77 {
            height: 26px;
        }
        
        .auto-style78 {
            height: 23px;
            text-align: right;
        }
        .auto-style79 {
            width: 6%;
            height: 23px;
        }
        .auto-style80 {
            width: 6%;
            height: 31px;
        }
                
        .auto-style84 {
            width: 1%;
            height: 23px;
        }
        
        .auto-style85 {
            height: 35px;
            width: 2%;
            text-align: right;
        }
        
        .auto-style90 {
            width: 3%;
            height: 23px;
            text-align: right;
        }
        .auto-style91 {
            height: 35px;
            width: 1%;
            text-align: right;
        }
        .auto-style92 {
            height: 35px;
            width: 3%;
        }
        .auto-style95 {
            width: 3%;
            height: 31px;
        }
        .auto-style96 {
            width: 3%;
            height: 23px;
        }
        .auto-style97 {
            height: 35px;
            width: 6%;
            text-align: right;
        }
        .auto-style99 {
            width: 6%;
            height: 23px;
            text-align: right;
        }
        
    </style>
  <%--  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="<%= Page.ResolveUrl("~/js/jquery-1.11.1.js") %>"></script>


   <script lang="javascript" type="text/javascript">

       $(function () {
           $('#<%= TextBox5.ClientID %>').autocomplete({
               source: function (request, response) {
                   $.ajax({
                       url: "widgets.aspx/GetAccount_Name",
                       data: "{'pre' : ' " + request.term + " '}",
                       dataType: "json",
                       type: "POST",
                       contentType: "application/json;charset=utf-8",
                       success: function (data) {
                           response($.map(data.d,function(item)
                           {
                               return {value:item}  
                           }))
                       },
                       error: function (XMLHttpRequest,textStatus,errorThrown) {
                           alert(textStatus);
                       }
                   });
               }
           });
       });
   </script>--%>

</head>

<body >
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
        <br dir="rtl" />
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="index.aspx">
                        <!-- Logo icon -->
                        <b class="logo-icon ps-2">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="../../assets/images/logo-icon.png" alt="homepage" class="light-logo" />

                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text">
                            <!-- dark Logo text -->
                           <%-- <img src="../../assets/images/logo-text.png" alt="homepage" class="light-logo" />--%>
                             <label runat="server" class=" mt-2">Hafiz Builder</label>
                        </span>
                        <!-- Logo icon -->
                        <!-- <b class="logo-icon"> -->
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                        <!-- Dark Logo icon -->
                        <!-- <img src="../../assets/images/logo-text.png" alt="homepage" class="light-logo" /> -->

                        <!-- </b> -->
                        <!--End Logo icon -->
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-start me-auto">
                        <li class="nav-item d-none d-lg-block"><a
                                class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)"
                                data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- create new -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <span class="d-none d-md-block">Create New <i class="fa fa-angle-down"></i></span>
                                <span class="d-block d-md-none"><i class="fa fa-plus"></i></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search position-absolute">
                                <input type="text" class="form-control" placeholder="Search &amp; enter"> <a
                                    class="srh-btn"><i class="ti-close"></i></a>
                            </form>
                        </li>
                        <li class="nav-item search-box"> 
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-end">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-bell font-24"></i>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" id="2" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                 <i class="font-24 mdi mdi-comment-processing"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end mailbox animated bounceInDown" aria-labelledby="2">
                                <ul class="list-style-none">
                                    <li>
                                        <div class="">
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-success btn-circle"><i
                                                            class="ti-calendar"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Event today</h5>
                                                        <span class="mail-desc">Just a reminder that event</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-info btn-circle"><i
                                                            class="ti-settings"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Settings</h5>
                                                        <span class="mail-desc">You can customize this template</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-primary btn-circle"><i
                                                            class="ti-user"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Pavan kumar</h5>
                                                        <span class="mail-desc">Just see the my admin!</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-danger btn-circle"><i
                                                            class="fa fa-link"></i></span>
                                                    <div class="ms-2">
                                                        <h5 class="mb-0">Luanch Admin</h5>
                                                        <span class="mail-desc">Just see the my new admin!</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="../../assets/images/users/1.jpg" alt="user" class="rounded-circle" width="31">
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end user-dd animated" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user me-1 ms-1"></i>
                                    My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet me-1 ms-1"></i>
                                    My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email me-1 ms-1"></i>
                                    Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i
                                        class="ti-settings me-1 ms-1"></i> Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i
                                        class="fa fa-power-off me-1 ms-1"></i> Logout</a>
                                <div class="dropdown-divider"></div>
                                <div class="ps-4 p-10"><a href="javascript:void(0)"
                                        class="btn btn-sm btn-success btn-rounded text-white">View Profile</a></div>
                            </ul>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
       <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="pt-4">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="index.aspx" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span
                                    class="hide-menu">Dashboard</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark"
                                href="javascript:void(0)" aria-expanded="false"><i class="fas fa-chart-line "></i><span
                                    class="hide-menu">Accounts</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="Chart_of_account.aspx" class="sidebar-link"><i
                                            class="far fa-user-circle"></i><span class="hide-menu"> Chart of Account
                                        </span></a></li>
                                <li class="sidebar-item"><a href="Cash_Receipt_Voucher.aspx" class="sidebar-link"><i
                                            class="mdi mdi-receipt"></i><span class="hide-menu"> Cash Receipt Voucher
                                        </span></a></li>
                                 <li class="sidebar-item"><a href="Cash_Payment_Voucher.aspx" class="sidebar-link">
                                            <i class="far fa-credit-card"></i><span class="hide-menu"> Cash Payment Voucher
                                        </span></a></li>
                                 <li class="sidebar-item"><a href="Bank_Receipt_Voucher.aspx" class="sidebar-link"><i
                                            class="mdi mdi-receipt"></i><span class="hide-menu"> Bank Receipt Voucher
                                        </span></a></li>
                                 <li class="sidebar-item"><a href="Bank_Payment_Voucher.aspx" class="sidebar-link">
                                            <i class="far fa-credit-card"></i><span class="hide-menu"> Bank Payment Voucher
                                        </span></a></li>
                                <li class="sidebar-item"><a href="Journal_Voucher.aspx" class="sidebar-link">
                                            <i class="fas fa-file-medical-alt"></i><span class="hide-menu"> Journal Voucher
                                        </span></a></li>
                                 <li class="sidebar-item"><a href="Account_Name_Update.aspx" class="sidebar-link">
                                            <i class="fas fa-edit"></i><span class="hide-menu"> Account Name Update
                                        </span></a></li>
                                <li class="sidebar-item"><a href="Bank_Cheque_Transaction.aspx" class="sidebar-link">
                                            <i class="fas fa-money-bill-alt"></i><span class="hide-menu"> Bank Cheque Transaction  
                                        </span></a></li>
                            </ul>
                        </li>
                         <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark"
                                href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-chart-bar"></i><span
                                    class="hide-menu">Inventory </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="icon-material.aspx" class="sidebar-link"><i
                                            class="mdi mdi-emoticon"></i><span class="hide-menu"> Material Icons
                                        </span></a></li>
                                <li class="sidebar-item"><a href="icon-fontawesome.aspx" class="sidebar-link"><i
                                            class="mdi mdi-emoticon-cool"></i><span class="hide-menu"> Font Awesome
                                            Icons </span></a></li>
                            </ul>
                        </li>
                          <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark"
                                href="javascript:void(0)" aria-expanded="false"><i class="fas fa-shopping-cart"></i><span
                                    class="hide-menu">Purchase </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="icon-material.aspx" class="sidebar-link"><i
                                            class="mdi mdi-emoticon"></i><span class="hide-menu"> Material Icons
                                        </span></a></li>
                                <li class="sidebar-item"><a href="icon-fontawesome.aspx" class="sidebar-link"><i
                                            class="mdi mdi-emoticon-cool"></i><span class="hide-menu"> Font Awesome
                                            Icons </span></a></li>
                            </ul>
                        </li>
                          <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark"
                                href="javascript:void(0)" aria-expanded="false"><i class="fas fa-edit"></i><span
                                    class="hide-menu">Sale </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="icon-material.aspx" class="sidebar-link"><i
                                            class="mdi mdi-emoticon"></i><span class="hide-menu"> Material Icons
                                        </span></a></li>
                                <li class="sidebar-item"><a href="icon-fontawesome.aspx" class="sidebar-link"><i
                                            class="mdi mdi-emoticon-cool"></i><span class="hide-menu"> Font Awesome
                                            Icons </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark"
                                href="javascript:void(0)" aria-expanded="false"><i class="fas fa-wrench"></i><span
                                    class="hide-menu">Utilities </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="icon-material.aspx" class="sidebar-link"><i
                                            class="mdi mdi-emoticon"></i><span class="hide-menu"> Material Icons
                                        </span></a></li>
                                <li class="sidebar-item"><a href="icon-fontawesome.aspx" class="sidebar-link"><i
                                            class="mdi mdi-emoticon-cool"></i><span class="hide-menu"> Font Awesome
                                            Icons </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark"
                                href="javascript:void(0)" aria-expanded="false"><i class="fas fa-cogs"></i><span
                                    class="hide-menu">SetUp </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="icon-material.aspx" class="sidebar-link"><i
                                            class="mdi mdi-emoticon"></i><span class="hide-menu"> Material Icons
                                        </span></a></li>
                                <li class="sidebar-item"><a href="icon-fontawesome.aspx" class="sidebar-link"><i
                                            class="mdi mdi-emoticon-cool"></i><span class="hide-menu"> Font Awesome
                                            Icons </span></a></li>
                            </ul>
                        </li>
                                           
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark"
                                href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-key"></i><span
                                    class="hide-menu">Authentication </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="authentication-login.aspx" class="sidebar-link"><i
                                            class="mdi mdi-all-inclusive"></i><span class="hide-menu"> Login </span></a>
                                </li>
                                <li class="sidebar-item"><a href="authentication-register.aspx" class="sidebar-link"><i
                                            class="mdi mdi-all-inclusive"></i><span class="hide-menu"> Register
                                        </span></a></li>
                            </ul>
                        </li>
                       
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title " font-size: x-large; font-weight: 500; font-style: inherit; font-variant: normal">Bank Payment Voucher</h4>
                        <div class="ms-auto text-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="container-fluid" style="margin-top:100px">
                <form id="frm" runat="server">
                    <div class="row ">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title "  font-size: x-large;">Bank Payment</h4>
                                </div>
                                <div class="comment-widgets scrollable table-responsive-sm">
                                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <table class="w-100 ">
                                                <tr >                                       
                                                    <td class="auto-style54">&nbsp;</td>
                                                    <td class="auto-style64" colspan="1">
                                                        <span class="hide-menu">
                                                        <asp:Label ID="Label8" runat="server" Font-Bold="False" Font-Italic="False" Text="Type" Width="85px" />
                                                        </span></td>
                                                    <td class="auto-style15" colspan="3">
                                                        <asp:RadioButton ID="RadioButton2" runat="server" Text="Cash" OnCheckedChanged="RadioButton2_CheckedChanged" Enabled="False" />
                                                        <span class="hide-menu">
                                                        <asp:RadioButton ID="RadioButton1" runat="server" CssClass="m-l-5" OnCheckedChanged="RadioButton1_CheckedChanged" Text="Cheque" Checked="True" />
                                                        </span>
                                                    </td>
                                                      <td class="auto-style54">&nbsp;</td>
                                                    <td class="auto-style54">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style54"></td>
                                                    <td class="auto-style54" colspan="1">
                                                        <asp:Label ID="Label1" runat="server" Font-Bold="False" Font-Italic="False" Text="Date" Width="85px" />
                                                    </td>
                                                    <td class="auto-style15" colspan="3">
                                                        <asp:TextBox ID="TextBox1" runat="server" onkeypress="pressEnter()" OnTextChanged="TextBox1_TextChanged" />
                                                        <asp:ImageButton ID="Button2" runat="server" CssClass="auto-style4" Font-Bold="True" Height="25px" ImageAlign="TextTop" ImageUrl="~/Images/—Pngtree—calendar icon in trendy style_5003062.png" Text="" Width="25px" />
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="yyyy-MM-dd" PopupButtonID="Button2" PopupPosition="BottomRight" TargetControlID="TextBox1" />
                                                    </td>
                                                       <td class="auto-style54">&nbsp;</td>
                                                    <td class="auto-style54"></td>
                                                     <td class="auto-style85">&nbsp;</td>
                                                      <td class="auto-style92">&nbsp;</td>
                                                     <td class="auto-style97">&nbsp;</td>
                                                     <td class="auto-style54">
                                                    </td>

                                                </tr>
                                                <tr>
                                       
                                                    <td class="auto-style62"></td>
                                                    <td class="auto-style62" colspan="1">
                                                        <asp:Label ID="Label5" Text="Voc#"   runat="server" Font-Bold="False" Width="85px"/>
                                                    </td>
                                                    <td class="auto-style80" colspan="1">
                                                        <asp:TextBox ID="TextBox5" runat="server" OnTextChanged="TextBox5_TextChanged"  onkeypress="pressEnter1()" />
                                                        <asp:TextBox ID="TextBox7" runat="server" Width="10px" Visible="False" Height="5px"></asp:TextBox>
                                                    </td>                                       
                                                    <td class="auto-style75">&nbsp;</td>
                                                    <td class="auto-style75"><span class="hide-menu">
                                                        <asp:Label ID="Label9" runat="server" Font-Bold="False" style="margin-left: 0px" Text="Cheque#" />
                                                        </span></td>                                       
                                                    <td class="auto-style75"><span class="hide-menu">
                                                        <asp:TextBox ID="TextBox8" runat="server" onkeypress="pressEnter4()" Width="127px" />
                                                        </span></td>                                        
                                                    <td class="auto-style75">&nbsp;</td>
                                                      <td class="auto-style75"><span class="hide-menu">
                                                          <asp:Label ID="Label10" runat="server" Font-Bold="False" style="text-align: right" Text="Chq Date" Width="70px" />
                                                          </span></td>
                                                      <td class="auto-style95" ><span class="hide-menu" >
                                                          <asp:TextBox ID="TextBox9" runat="server" onkeypress="pressEnter4()" Width="100px" />
                                                          </span></td>
                                                      <td class="auto-style80"><span class="hide-menu">
                                                          <asp:ImageButton ID="ImageButton1" runat="server" CssClass="auto-style4" Font-Bold="True" Height="25px" ImageAlign="TextTop" ImageUrl="~/Images/—Pngtree—calendar icon in trendy style_5003062.png" Text="" Width="25px" />
                                                          <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="yyyy-MM-dd" PopupButtonID="ImageButton1" PopupPosition="BottomRight" TargetControlID="TextBox9" />
    
                                                      </span></td>
                                                </tr>
                                                <tr>                                      
                                                    <td class="auto-style65"></td>
                                                    <td class="auto-style65" colspan="1">
                                                        <asp:Label ID="Label7" Text="Party Name"   runat="server" Font-Bold="False" Width="85px"/>
                                                    </td>
                                                    <td class="auto-style79" colspan="1">
                                                        <ajaxToolkit:ComboBox ID="ComboBox1" runat="server" onkeypress="pressEnter2()" AutoPostBack="True"  OnTextChanged="ComboBox1_TextChanged" AutoCompleteMode="SuggestAppend" >
                                                        </ajaxToolkit:ComboBox>
                                                    </td>
                                                    <td class="auto-style84">&nbsp;</td>
                                                    <td class="auto-style78">
                                                        <span class="hide-menu">
                                                        <asp:Label ID="Label2" runat="server" Font-Bold="False" Text="Naration" />
                                                        </span>
                                                    </td>
                                                    <td class="auto-style47">
                                                        <asp:TextBox ID="TextBox2" runat="server" OnTextChanged="TextBox2_TextChanged" onkeypress="pressEnter3()" style="width: 128px"/>
                                                    </td>
                                                    <td class="auto-style47">&nbsp;</td>
                                                    <td class="auto-style59">
                                                        <asp:Label ID="Label3" Text="Invoice"  runat="server" Font-Bold="False"/>
                                                    </td>
                                                    <td class="auto-style96">
                                                        <asp:TextBox ID="TextBox3" runat="server"  onkeypress="pressEnter4()" Width="100px"/>
                                                    </td>
                                                    <td class="auto-style99">
                                                        <asp:Label ID="Label4" Text="Amount"   runat="server" Font-Bold="False"/>
                                                    </td>
                                                    <td class="auto-style65">
                                                        <asp:TextBox ID="TextBox4" runat="server" OnKeyDown="TextBox4_KeyDown" onkeypress="pressEnter5()" Width="100px"  />
                                                    </td>
                                                    <td class="auto-style47"><span class="hide-menu">
                                                        <asp:Button ID="btninsert" runat="server" BackColor="#1C5E55" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt" Font-Strikeout="False" Font-Underline="False" ForeColor="White" Height="25px" OnClick="btninsert_Click" Text="⇩" Width="25px" />
                                                        </span></td>
                                                    
                                                </tr>
                                            </table>
                                            <br />
                                            
                                            <asp:GridView  ID="GridView1" runat="server"  ShowHeaderWhenEmpty="True" OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                                AutoGenerateColumns="False" onrowdeleting="RowDeleting"
                                                OnRowCancelingEdit="cancelRecord" OnRowEditing="editRecord" 
                                                OnRowUpdating="updateRecord" CellPadding="4" Width="95%" GridLines="None" 
                                                ForeColor="#333333" CssClass="mt-1 m-l-20"  Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="12pt" ShowFooter="True"
                                                    >
                                                <RowStyle BackColor="#E3EAEB" HorizontalAlign="Center" />
                                                <AlternatingRowStyle BackColor="White"/>
 
                                                <Columns>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Sr_No</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Label ID ="lblSr" runat="server" Text='<%#Bind("Sr_No")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle  Width="90px"/>
                                                    </asp:TemplateField>
                               
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Party_Name</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Label ID ="lblName" runat="server" Text='<%#Bind("Party_Name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                            <HeaderStyle   />
                                                        <EditItemTemplate>
                                                        <asp:TextBox ID="txtName" runat="server" Text='<%#Bind("Party_Name") %>' MaxLength="50"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtName" runat="server" Text="*" ToolTip="Enter name" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revtxtName" runat="server" Text="*" ToolTip="Enter alphabate " ControlToValidate="txtName" ValidationExpression="^[a-zA-Z'.\s]{1,40}$"></asp:RegularExpressionValidator> 
                                                        </EditItemTemplate>
                                                       <%-- <FooterTemplate>
                                                        <asp:TextBox ID="txtNewName" runat="server" MaxLength="50"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtNewName" runat="server" Text="*" ToolTip="Enter name" ControlToValidate="txtNewName"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revtxtNewName" runat="server" Text="*" ToolTip="Enter alphabate " ControlToValidate="txtNewName" ValidationExpression="^[a-zA-Z'.\s]{1,40}$"></asp:RegularExpressionValidator>          
                                                        </FooterTemplate>--%>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Naration</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Label ID = "lblCountry" runat="server" Text='<%#Bind("Naration1") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                        <asp:TextBox ID="txtCountry" runat="server" Text='<%#Bind("Naration1") %>' MaxLength="20"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtCountry" runat="server" Text="*" ToolTip="Enter country" ControlToValidate="txtCountry"></asp:RequiredFieldValidator>
                                                        </EditItemTemplate>
                                                      <%--  <FooterTemplate>
                                                        <asp:TextBox ID="txtNewCountry" runat="server" MaxLength="20"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtNewCountry" runat="server" Text="*" ToolTip="Enter country" ControlToValidate="txtNewCountry"></asp:RequiredFieldValidator>
                                                        </FooterTemplate>--%>
                                                    </asp:TemplateField>
       
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Invoice</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Label ID = "lblSalary" runat="server" Text='<%#Bind("Invoice") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                        <asp:TextBox ID="txtSalary" runat="server" Text='<%#Bind("Invoice") %>'  MaxLength="10"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtSalary" runat="server" Text="*"  ToolTip="Enter salary" ControlToValidate="txtSalary"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revtxtSalary" runat="server" Text="*" ToolTip="Enter numeric value" ControlToValidate="txtSalary" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>        
                                                        </EditItemTemplate>
                                                     <%--   <FooterTemplate>
                                                        <asp:TextBox ID="txtNewSalary" runat="server" MaxLength="10"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtNewSalary" runat="server" Text="*"  ToolTip="Enter salary" ControlToValidate="txtNewSalary"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revtxtNewSalary" runat="server" Text="*" ToolTip="Enter numeric value" ControlToValidate="txtNewSalary" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                                                        </FooterTemplate>--%>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Amount</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Label ID = "lblAmt" runat="server" Text='<%#Bind("Credit") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                        <asp:TextBox ID="txtAmt" runat="server" Text='<%#Bind("Credit") %>'  MaxLength="10"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtAmt" runat="server" Text="*"  ToolTip="Enter salary" ControlToValidate="txtAmt"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revtxtAmt" runat="server" Text="*" ToolTip="Enter numeric value" ControlToValidate="txtAmt" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
          
                                                        </EditItemTemplate>
                                                        <%--<FooterTemplate>
                                                        <asp:TextBox ID="txtNewAmt" runat="server" MaxLength="10"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtNewAmt" runat="server" Text="*"  ToolTip="Enter Amt" ControlToValidate="txtNewAmt"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revtxtNewAmt" runat="server" Text="*" ToolTip="Enter numeric value" ControlToValidate="txtNewAmt" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                                                        </FooterTemplate>--%>
                                                    </asp:TemplateField>
 
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Operation</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Button ID="btnEdit" runat="server" CommandName="Edit" Text="Edit" />
                                                        <asp:Button ID="btnDelete" runat="server" CommandName="Delete" Text="Delete" CausesValidation="true" OnClientClick="return confirm('Are you sure?')" />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                        <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Update" AutoPostBack="true" />
                                                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Cancel" CausesValidation="false" />
                                                        </EditItemTemplate>
 
                                                        <FooterTemplate>
                                                      <%-- <asp:Button ID="btnNewInsert" runat="server" Text="Insert" OnClick="InsertNewRecord"/>
                                                       <asp:Button ID="btnNewCancel" runat="server" Text="Cancel" OnClick="AddNewCancel" CausesValidation="false" />--%>
                                                        </FooterTemplate>        
                                                    </asp:TemplateField>           
                                                </Columns>

                                                <EditRowStyle BackColor="#7C6F57" />
                                                <EmptyDataTemplate> No record available</EmptyDataTemplate>       
                                                <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" HorizontalAlign="Center" />
                                                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                                <RowStyle HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
 
                                                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                                                <SortedAscendingHeaderStyle BackColor="#246B61" />
                                                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                                                <SortedDescendingHeaderStyle BackColor="#15524A" />
 
                                            </asp:GridView>
                           
                                            <br />
                           
                                            <table class="w-100">
                                                <tr>
                                                    <td class="auto-style52"></td>
                                                    <td class="auto-style48"></td>
                                                    <td class="auto-style49"></td>
                                                    <td class="auto-style50" colspan="2"></td>
                                                    <td class="auto-style51"></td>
                                                    <td class="auto-style77"></td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style67"><span class="hide-menu">
                                                        <asp:Button ID="btnAdd" runat="server" BackColor="#1C5E55" CssClass="auto-style53" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt" Font-Strikeout="False" Font-Underline="False" ForeColor="White" Height="30px" OnClick="AddNewRecord" Text="Add New Record" Visible="False" Width="152px" />
                                                        </span></td>
                                                    <td class="auto-style68">                          
                                                        &nbsp;</td>
                                                    <td class="auto-style69">  
                                                        <strong>
                                                        <asp:Button ID="Button5" runat="server" BackColor="#1C5E55" CssClass="auto-style76" Font-Names="PMingLiU-ExtB" Font-Size="13pt" ForeColor="White" Height="30px" OnClick="Button5_Click" Text="Save" Width="120px" />
                                                        </strong></td>
                                                    <td class="auto-style70">
                                                        <asp:Button ID="Button1" runat="server" Text="Reset" OnClick="Button1_Click" BackColor="#1C5E55" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt" Font-Strikeout="False" Font-Underline="False" ForeColor="White" Height="30px" style="margin-left: 11px" Width="120px" />  
                                                    </td>
                                                    <td class="auto-style71">  
                                                        <asp:Button ID="Button3" runat="server" Text="Print" BackColor="#1C5E55" Font-Bold="True" Font-Names="PMingLiU-ExtB" Font-Size="13pt" Font-Strikeout="False" Font-Underline="False" ForeColor="White" Height="30px" style="margin-left: 10px" Width="120px" OnClick="Button3_Click" />  
                                                    </td>
                                                    <td class="auto-style72"></td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div> 
                        </div>
                    </div>
                </form>
            </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
           
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
           <footer class="footer text-center">
                All Rights Reserved by Admin Designed and Developed by TECH<b>NO</b>HUB.
            </footer>

            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
        <script type="text/javascript">
    function pressEnter() {
      // Key Code for ENTER = 13
      if ((event.keyCode == 13)) {
        document.getElementById("TextBox5").focus({preventScroll:false});
      }
    }
    function pressEnter1() {
        if ((event.keyCode == 13)) {
            document.getElementById("ComboBox1").focus({ preventScroll: false });
        }
    }
    function pressEnter2() {
        if ((event.keyCode == 13)) {
            document.getElementById("TextBox2").focus({ preventScroll: false });
        }
    }
    function pressEnter3() {
        if ((event.keyCode == 13)) {
            document.getElementById("TextBox3").focus({ preventScroll: false });
        }
    }
    function pressEnter4() {
      if ((event.keyCode == 13)) {
          document.getElementById("TextBox4").focus({ preventScroll: false });
      }
    }
    function pressEnter5() {
        if ((event.keyCode == 13)) {
            document.getElementById("btninsert").focus({ preventScroll: false });
        }
    }
    </script>

    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
</body>

</html>



