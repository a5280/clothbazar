﻿using CODE_FIRST_APPROACH_OF_ENTITY_FRAMEWORK_IN_ASP.NET_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace CODE_FIRST_APPROACH_OF_ENTITY_FRAMEWORK_IN_ASP.NET_MVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        StudentContext db = new StudentContext();
        public ActionResult Index()
        {
            var data = db.Students.ToList();
            return View(data);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create( Student s)
        {
            if (ModelState.IsValid==true)
            {          
                db.Students.Add(s);
                int a=db.SaveChanges();
                if (a>0)
                {
                    TempData["message"] = "<script>alert('Data insert')</script>";
                    ModelState.Clear();
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.message = "<script>alert('Data not insert')</script>";
                }
            }                     
            return View();
        }

        public ActionResult Edit(int id)
        {
            var row = db.Students.Where(model => model.Id == id).FirstOrDefault();
            return View(row);
        }
        [HttpPost]
        public ActionResult Edit(Student s)
        {
            if (ModelState.IsValid==true)
            {
                db.Entry(s).State = EntityState.Modified;
                int a = db.SaveChanges();
                if (a > 0)
                {

                    TempData["message"] = "<script>alert('Data update')</script>";
                    ModelState.Clear();
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.message = "<script>alert('Data not update')</script>";
                }
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            var row=db.Students.Where(model => model.Id == id).FirstOrDefault();
            return View(row);
        }
        [HttpPost]
        public ActionResult Delete(Student s)
        {
     
            if (ModelState.IsValid==true)
            {
                db.Entry(s).State = EntityState.Deleted;
               var a= db.SaveChanges();
                if (a>0)
                {
                    TempData["message"] = "<script>alert('Data delete')</script>";
                    ModelState.Clear();
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.message = "<script>alert('Data not delete')</script>";

                }
            }
            return View();
        }

        public ActionResult Details(int id)
        {
            var row = db.Students.Where(model => model.Id == id).FirstOrDefault();
            return View(row);
        }
    }
}