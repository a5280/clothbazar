﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Custom_Validation.Models
{
    public class Employee
    {
        [Required(ErrorMessage ="Must Required")]
        public string  name
        {
            get; set;
        }
        [Custom_Validate]
        public string  Message
        {
            get; set;
        }
    }
}