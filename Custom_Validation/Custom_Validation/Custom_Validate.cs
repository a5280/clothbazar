﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Custom_Validation 
{
    public class Custom_Validate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value !=null)
            {
                string message = value.ToString();
                if (message.Equals("mehran"))
                {
                    return new ValidationResult("mehran already contain");
                }
                else
                {
                    return  ValidationResult.Success;
                }
            }
            return new  ValidationResult("Must Required");
        }
    }
}