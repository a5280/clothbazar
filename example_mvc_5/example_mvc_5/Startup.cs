﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(example_mvc_5.Startup))]
namespace example_mvc_5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
