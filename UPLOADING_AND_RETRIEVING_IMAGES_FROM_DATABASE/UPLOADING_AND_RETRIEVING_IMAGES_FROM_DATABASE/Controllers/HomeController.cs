﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UPLOADING_AND_RETRIEVING_IMAGES_FROM_DATABASE.Controllers
{
    public class HomeController : Controller
    {
        SSSEntities db = new SSSEntities();
        // GET: Home
        public ActionResult Index()
        {
            var data = db.Students.ToList();
            return View(data);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Student s)
        {
            string filename = Path.GetFileNameWithoutExtension(s.ImageFile.FileName);
            string extension = Path.GetExtension(s.ImageFile.FileName);

            HttpPostedFileBase postedfile = s.ImageFile;
            int length = postedfile.ContentLength;
            if (extension.ToLower() ==".jpg" || extension.ToLower() == ".png" || extension.ToLower() == ".jpeg")
            {
                if (length <= 1000000)
                {
                    /////Database save image
                    filename = filename + extension;
                    s.image_path = "~/Image/" + filename;

                    //////Server Project Image folder save image
                    filename = Path.Combine(Server.MapPath("~/Image/"), filename);
                    s.ImageFile.SaveAs(filename);
                    db.Students.Add(s);
                    int a = db.SaveChanges();
                    if (a > 0)
                    {
                        ViewBag.Message = "<script>alert('Record Inserted')</script>";
                        ModelState.Clear();
                    }
                    else
                    {
                        ViewBag.Message = "<script>alert('Record Not Inserted')</script>";

                    }
                }
                else
                {
                    ViewBag.SizeMessage = "<script>alert('Size Should be of 1 MB')</script>";
                }
            }
            else
            {
                ViewBag.ExtensionMessage = "<script>alert('Image extension not supported')</script>";
            }

            return View();
        }

       
        public ActionResult Edit(int id)
        {
            var data = db.Students.Where(model => model.ID == id).FirstOrDefault();
            Session["image"] = data.image_path;
            return View(data);
        }
        [HttpPost]
        public ActionResult Edit(Student s)
        {
            if (ModelState.IsValid==true)
            {
                if (s.ImageFile !=null)
                {
                    string filename = Path.GetFileNameWithoutExtension(s.ImageFile.FileName);
                    string extension = Path.GetExtension(s.ImageFile.FileName);

                    HttpPostedFileBase postedfile = s.ImageFile;
                    int length = postedfile.ContentLength;
                    if (extension.ToLower() == ".jpg" || extension.ToLower() == ".png" || extension.ToLower() == ".jpeg")
                    {
                        if (length <= 1000000)
                        {
                            /////Database save image
                            filename = filename + extension;
                            s.image_path = "~/Image/" + filename;

                            //////Server Project Image folder save image
                            filename = Path.Combine(Server.MapPath("~/Image/"), filename);
                            s.ImageFile.SaveAs(filename);
                            db.Students.Add(s);
                            db.Entry(s).State = EntityState.Modified;
                            int a = db.SaveChanges();
                            if (a > 0)
                            {
                                string ImagePath = Request.MapPath(Session["image"].ToString());
                                if (System.IO.File.Exists(ImagePath))
                                {
                                    System.IO.File.Delete(ImagePath);
                                }
                                TempData["updateMessage"] = "<script>alert('Record Update')</script>";
                                ModelState.Clear();
                            }
                            else
                            {
                                TempData["updateMessage"] = "<script>alert('Record Not Update')</script>";

                            }
                        }
                        else
                        {
                            ViewBag.SizeMessage = "<script>alert('Size Should be of 1 MB')</script>";
                        }
                    }
                    else
                    {
                        ViewBag.ExtensionMessage = "<script>alert('Image extension not supported')</script>";
                    }
                }
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            if (id > 0)
            {
                var data = db.Students.Where(model => model.ID == id).FirstOrDefault();
                if (data != null)
                {
                    db.Entry(data).State = EntityState.Deleted;
                    int a = db.SaveChanges();
                    if (a > 0)
                    {
                        TempData["DeleteRecord"] = "<script>alert('Record Delete')</script>";
                        string ImagePath = Request.MapPath(data.image_path.ToString());
                        if (System.IO.File.Exists(ImagePath))
                        {
                            System.IO.File.Delete(ImagePath);
                        }
                    }
                    else
                    {
                        TempData["DeleteRecord"] = "<script>alert('Record Not Delete')</script>";
                    }
                }
            }
            return RedirectToAction("Index", "Home");

        }

        public ActionResult Details(int id)
        {
            var data = db.Students.Where(model => model.ID == id).FirstOrDefault();
            Session["image"] = data.image_path.ToString();
            return View(data);
        }

    }
}