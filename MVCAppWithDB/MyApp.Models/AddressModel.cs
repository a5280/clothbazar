﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyApp.Models
{
  public  class AddressModel
    {
        public int Id
        {
            get; set;
        }
        [Required]
        public string Details
        {
            
            get; set;
        }
        [Required]
        public string State
        {
            
            get; set;
        }
        [Required]
        public string Country
        {
            get; set;
        }
    }
}
