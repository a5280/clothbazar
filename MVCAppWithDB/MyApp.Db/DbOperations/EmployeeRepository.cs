﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyApp.Models;
using System.Data.Entity;

namespace MyApp.Db.DbOperations
{
     public class EmployeeRepository
    {
        public int AddEmployee(EmployeeModel model)
        {
            using (var context =new EmployeeDBEntities())
            {
                Employee emp = new Employee()
                {
                    FirstName = model.FirstName,
                    LastName =model.LastName,
                    Email=model.Email,
                    Code=model.Code
                };

                if (model.Address!=null)
                {
                    emp.Address = new Address()
                    {
                        Details = model.Address.Details,
                        State=model.Address.State,
                        Country=model.Address.Country
                    };
                }
                context.Employee.Add(emp);
                context.SaveChanges();
                return emp.Id;
            }
        }

        public List<EmployeeModel> GetAllEmployee()
        {
            using (var context = new EmployeeDBEntities())
            {
                var result = context.Employee
                    .Select(x => new EmployeeModel
                    {
                        Id = x.Id,
                        AddressId = x.AddressId,
                        FirstName = x.FirstName,
                        LastName = x.LastName, 
                        Email = x.Email,
                        Code = x.Code,
                        Address = new AddressModel()
                        {
                            Details=x.Address.Details,
                            State=x.Address.State,
                            Country=x.Address.Country
                        }

                    }).ToList();
                return result;
            }

        }

        public EmployeeModel GetEmployee(int id)
        {
            using (var context = new EmployeeDBEntities())
            {
                var result = context.Employee
                    .Where(x => x.Id ==id)
                    .Select(x => new EmployeeModel
                    {
                        Id = x.Id,
                        AddressId = x.AddressId,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Email = x.Email,
                        Code = x.Code,
                        Address = new AddressModel()
                        {
                            Details = x.Address.Details,
                            State = x.Address.State,
                            Country = x.Address.Country
                        }

                    }).FirstOrDefault();
                return result;
            }
        }

        public bool UpdateEmployee(int id,EmployeeModel model)
        {
            using (var context = new EmployeeDBEntities())
            {
                //First Method Delete record but double hit database 

                //var employee = context.Employee.FirstOrDefault(x => x.Id == id);

                //if (employee !=null)
                //{
                //    employee.FirstName = model.FirstName;
                //    employee.LastName = model.LastName;
                //    employee.Code = model.Code;
                //    employee.Email = model.Email;
                //    employee.Address.Details = model.Address.Details;
                //    employee.Address.State = model.Address.State;
                //    employee.Address.Country = model.Address.Country;
                //}


                //Second Method Delete record but Single hit database 
                var employee = new Employee();

                if (employee != null)
                {
                    employee.Id = model.Id;
                    employee.FirstName = model.FirstName;
                    employee.LastName = model.LastName;
                    employee.Code = model.Code;
                    employee.Email = model.Email;
                    //employee.Address.Details = model.Address.Details;
                    //employee.Address.State = model.Address.State;
                    //employee.Address.Country = model.Address.Country;
                }
                context.Entry(employee).State = EntityState.Modified;

                context.SaveChanges();
                return true;
            }              
        }

        public bool DeleteEmployee(int id)
        {
            using (var context = new EmployeeDBEntities())
            {
                //First Method Delete record but double hit database 

                //var employee = context.Employee.FirstOrDefault(x => x.Id == id);

                //if (employee !=null)
                //{
                //    context.Employee.Remove(employee);
                //    context.SaveChanges();
                //    return true;
                //}

                //Second Method Delete record but Single hit database 
                var emp = new Employee()
                {
                    Id = id
                };
                context.Entry(emp).State = EntityState.Deleted;
                context.SaveChanges();

                return false;
            }
            
        }

    }
}
