﻿using MyApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyApp.Db.DbOperations;

namespace MVCAppWithDB.Controllers
{
    //[Route("Employee")]
    public class HomeController : Controller
    {
        EmployeeRepository repository = null;
        public HomeController()
        {
            repository = new EmployeeRepository();
        }
        // GET: Home
        //[Route("~/")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(EmployeeModel emp)
        {
            if (ModelState.IsValid)
            {
                int id= repository.AddEmployee(emp);
                if (id > 0)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Successfully Add Data!";
                    return RedirectToAction("GetAllEmployee");
                }
            }
            return View();
        }

        public ActionResult GetAllEmployee()
        {
            var result = repository.GetAllEmployee();
            return View(result);
        }

        public ActionResult Details(int id)
        {
            var result = repository.GetEmployee(id);
            return View(result);
        }

        public ActionResult Edit(int id)
        {
            var result = repository.GetEmployee(id);
            return View(result);
        }
        [HttpPost]
        public ActionResult Edit(EmployeeModel model)
        {
            if (ModelState.IsValid)
            {
                 repository.UpdateEmployee(model.Id, model);
                return RedirectToAction("GetAllEmployee");
            }
            
            return View();
        }

        public ActionResult Delete(int id)
        {
            repository.DeleteEmployee(id);
            return RedirectToAction("GetAllEmployee");
        }
    }
}