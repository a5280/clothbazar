﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Authorization_Filters.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        // GET: Home
        [AllowAnonymous]
        public ActionResult NonSecureMethod()
        {
            return View();
        }
        public ActionResult SecureMethod()
        {
            return View();
        }
    }
}