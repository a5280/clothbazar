﻿using AUTHORIZE_FILTER_IN_ASP.NET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AUTHORIZE_FILTER_IN_ASP.NET.Controllers
{
    public class LoginController : Controller
    {
        LoginDBEntities db = new LoginDBEntities();
        // GET: Login
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(User u,string ReturnUrl)
        {
            if (Isvalid(u)==true)
            {
                FormsAuthentication.SetAuthCookie(u.Username, false);
                Session["username"] = u.Username.ToString();
                if (ReturnUrl !=null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Index","Home");
                }
            }
            else
            {
                return View();
            }
          
        }
        public bool Isvalid(User u)
        {
            var credentials = db.Users.Where(model => model.Username ==u.Username && model.Password ==u.Password).FirstOrDefault();
            if (credentials!=null)
            {
                return (u.Username == credentials.Username && u.Password == credentials.Password);
            }
            else
            {
                return false;
            }
        }
        public ActionResult logout()
        {
            FormsAuthentication.SignOut();
            Session["username"] = null;
            
            return RedirectToAction("Index","Home");
        }
    }
}