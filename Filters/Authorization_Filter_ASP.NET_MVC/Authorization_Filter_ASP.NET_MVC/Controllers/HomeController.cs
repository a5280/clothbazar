﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Authorization_Filter_ASP.NET_MVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult Secured()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult NonSecured()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
    }
}