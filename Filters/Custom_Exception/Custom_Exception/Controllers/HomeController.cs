﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Custom_Exception.Models;
namespace Custom_Exception.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [LogCustomExceptionFilter]
        public ActionResult Index()
        {
            throw new Exception("Something Wrong");
            //return View();
        }
        public ActionResult About()
        {
            throw new NullReferenceException("Null Exception");
        }
        public ActionResult Contact()
        {
            throw new IndexOutOfRangeException("Index Out Range");
        }
    }
}