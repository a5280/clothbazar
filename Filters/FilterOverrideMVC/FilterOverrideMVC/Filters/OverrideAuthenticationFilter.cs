﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace FilterOverrideMVC.Filters
{
    public class OverrideAuthenticationFilter : FilterAttribute, IOverrideFilter
    {
        public Type FiltersToOverride
        {
            get
            {
                return typeof(IAuthenticationFilter);
            }
        }
    }
}