﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FilterOverrideMVC.Startup))]
namespace FilterOverrideMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
