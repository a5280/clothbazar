﻿using FilterOverrideMVC.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilterOverrideMVC.Controllers
{
  //  [CustomAuthenticationFilter]
    [CustomAuthorizationFilter]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    //    [OverrideAuthentication]
      //  [OverrideAuthenticationFilter]
    //  [OverrideAuthorization]
        [OverrideAuthorizationFilter]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}