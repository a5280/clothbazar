﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChildActionOnly_Attribute_Exception_ASP.NET_MVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        // This method is accessible only by a child request. A runtime 
        // exception will be thrown if a URL request is made to this method
        [ChildActionOnly]
        public ActionResult Countries(List<string> countryData)
        {
            return View(countryData);
        }
    }
}