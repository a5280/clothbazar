﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExceptionFilterInMVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        //[HandleError(View ="Error")]
        [HandleError(ExceptionType =typeof(NullReferenceException),View = "NullReference")]
        [HandleError(ExceptionType =typeof(DivideByZeroException),View = "DivideByZero")]
        [HandleError]
        public ActionResult Index()
        {
            throw new Exception("Something went wrong");
            // return View();
        }
        public ActionResult ZeroMethod()
        {
            throw new DivideByZeroException("Zero Exception");
        }
        public ActionResult NullMethod()
        {
            throw new NullReferenceException("Null Reference");
        }
     
    }
}