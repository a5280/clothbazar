﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Custom_Authorization_Filter.Startup))]
namespace Custom_Authorization_Filter
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
