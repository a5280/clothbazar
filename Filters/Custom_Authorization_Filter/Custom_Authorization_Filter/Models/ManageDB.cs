﻿using Custom_Authorization_Filter.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Custom_Authorization_Filter.Models
{
    public class ManageDB
    {
        public USER GetUserDetails(string UserName, string Password)
        {
            USER user = new USER();
            using (UserEntities db = new UserEntities())
            {
                user = db.USERS.FirstOrDefault(u => u.UserName.ToLower() == UserName.ToLower() &&
                                      u.Password == Password);
            }
            return user;
        }
    }
}