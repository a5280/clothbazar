﻿using Custom_Authentication_Filter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Custom_Authentication_Filter.Controllers
{
    public class HomeController : Controller
    {
        [CustomAuthenticationFilter]
        public ActionResult Index()
        {
            return View();
        }
        [CustomAuthenticationFilter]
        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}