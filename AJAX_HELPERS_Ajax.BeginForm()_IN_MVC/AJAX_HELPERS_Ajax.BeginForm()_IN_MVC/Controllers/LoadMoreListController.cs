﻿using AJAX_HELPERS_Ajax.BeginForm___IN_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AJAX_HELPERS_Ajax.BeginForm___IN_MVC.Controllers
{
    public class LoadMoreListController : Controller
    {
        EmpDBEntities db = new EmpDBEntities();

        // GET: LoadMoreList
        public ActionResult Index()
        {
            int num = 2;
            Session["MoreLoad"] = num;
            var data = db.Employees.ToList().Take(num);
            return View(data);
        }
        [HttpPost]
        public ActionResult Index(Employee e)
        {
            int rows =Convert.ToInt32( Session["MoreLoad"]) + 2;
            var data = db.Employees.ToList().Take(rows);
            Session["MoreLoad"] = rows;
            return PartialView("_LoadMoreData",data);
        }
    }
}