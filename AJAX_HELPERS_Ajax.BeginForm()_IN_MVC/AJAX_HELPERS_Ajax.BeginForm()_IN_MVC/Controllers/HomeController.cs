﻿using AJAX_HELPERS_Ajax.BeginForm___IN_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AJAX_HELPERS_Ajax.BeginForm___IN_MVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        EmpDBEntities db = new EmpDBEntities();

        public ActionResult Index()
        {
           var data= db.Employees.ToList();
            return View(data);
        }
        [HttpPost]
        public ActionResult Index(string q)
        {
            if (string.IsNullOrEmpty(q) == false)
            {
                List<Employee> emp = db.Employees.Where(model => model.Name.StartsWith(q)).ToList();
                return PartialView("_SearchData", emp);
            }
            else
            {
                List<Employee> emp = db.Employees.ToList();
                return PartialView("_SearchData", emp);
            }
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Employee e)
        {
            if (ModelState.IsValid==true)
            {
                db.Employees.Add(e);
                int a = db.SaveChanges();
                if (a > 0)
                {
                    //return Json("Data Inserted !!");
                    return JavaScript("alert('Data Inserted !!')");
                }
                else
                {
                    return JavaScript("alert('Data not Inserted !!')");

                    //return Json("Data not Inserted !!");
                }
            }
        
            return View();
        }
    }
}