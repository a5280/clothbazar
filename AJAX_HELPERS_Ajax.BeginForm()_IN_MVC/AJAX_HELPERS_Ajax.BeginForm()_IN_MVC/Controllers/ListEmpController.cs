﻿using AJAX_HELPERS_Ajax.BeginForm___IN_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AJAX_HELPERS_Ajax.BeginForm___IN_MVC.Controllers
{
    public class ListEmpController : Controller
    {
        EmpDBEntities db = new EmpDBEntities();

        public ActionResult Index()
        {
            return View();
        }
        // GET: ListEmp
     
        public ActionResult ListAllEmp()
        {
            var data = db.Employees.ToList();
            return PartialView("_Employee",data);
        }
   
        public ActionResult ListEmpHighAge()
        {
            var data = db.Employees.OrderByDescending(model => model.Age).Take(3).ToList();
            return PartialView("_Employee", data);
        }
    }
}