﻿using Conventional_Routing_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Conventional_Routing_MVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult GetAllStudent()
        {
            var data = Students().ToList();
            return View(data);
        }
        public ActionResult GetStudent(int id)
        {
            var data = Students().FirstOrDefault(model => model.Id == id);
            return View(data);
        }
        public ActionResult GetStudentAddress(int id)
        {
            var data = Students().Where(model => model.Id == id).Select(model => model.Address).FirstOrDefault();
            return View(data);
        }

        private List<Student> Students()
        {
            return new List<Student>()
            {
                new Student() {Id=1,name="Talha" ,Address= new Address () {Homenumber=123,Address1="Awami Chonk",City="Faisalabad"} },
                new Student() {Id=2,name="Humza",Address=new Address () {Homenumber=113,Address1="Barket Pura",City="Faisalabad"} },
                new Student() {Id=3,name="Moazzam",Address=new Address () {Homenumber=1123,Address1="Phare Ground",City="Faisalabad"} },
                new Student() {Id=4,name="Adnan",Address=new Address () {Homenumber=1234,Address1="Destgeer Pura",City="Faisalabad"}  }
            };
        }
    }
}