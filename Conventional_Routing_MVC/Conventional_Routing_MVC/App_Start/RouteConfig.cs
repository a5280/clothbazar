﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Conventional_Routing_MVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "AllStudents",
                url: "Students",
                defaults: new { Controller = "Home",Action = "GetAllStudent" }
             );

            routes.MapRoute(
                name:"Studentid",
                url:"StudentID/{id}",
                defaults: new {Controller= "Home", Action= "GetStudent" }               
                );

            routes.MapRoute(
                name: "StudentAdd",
                url: "StudentAddress/{id}",
                defaults: new { Controller = "Home",Action = "GetStudentAddress"
                }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
