﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Conventional_Routing_MVC.Models
{
    public class Address
    {
        public int Homenumber
        {
            get; set;
        }
        public string  Address1
        {
            get; set;
        }
        public string  City
        {
            get; set;
        }
    }
}