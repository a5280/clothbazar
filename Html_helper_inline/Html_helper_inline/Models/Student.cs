﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Html_helper_inline.Models
{
    public class Student
    {
        public int id
        {
            get; set;
        }
        public string name
        {
            get; set;
        }
        public bool gender
        {
            get; set;
        }
    }
}