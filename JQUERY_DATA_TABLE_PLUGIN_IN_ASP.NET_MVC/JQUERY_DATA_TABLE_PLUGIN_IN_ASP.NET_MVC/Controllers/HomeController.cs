﻿using JQUERY_DATA_TABLE_PLUGIN_IN_ASP.NET_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JQUERY_DATA_TABLE_PLUGIN_IN_ASP.NET_MVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        EmpDBEntities db = new EmpDBEntities();
        public ActionResult Index()
        {
            var data = db.Tables.ToList();
            return View(data);
        }
    }
}