﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(hostingExp.Startup))]
namespace hostingExp
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
