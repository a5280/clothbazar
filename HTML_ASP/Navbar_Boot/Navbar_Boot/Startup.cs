﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Navbar_Boot.Startup))]
namespace Navbar_Boot
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
