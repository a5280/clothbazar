﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FORM1.aspx.cs" Inherits="Css_Example.FORM1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CSS APP</title>
    <link href="CSS.css" rel="stylesheet" />
    <style>
        h2{
            color:azure;
        }
        #heading2{
            font-size:70px;
        }
        #heading22{
              background-color:blue;
        }
        .heading3{
           font-size:70px;
        }
        .heading33{
            background-color:blue;
        }

        #link1:link
        {
            color:orange;
        }
        #link2:visited
        {
             color:green;
        }
        #link3:hover
        {
              color:azure;
        }
        #link4:active
        {
               color:aqua;
        }
        #txt1:focus
        {
            background-color:skyblue;
        }
        #P1 ::after{
            content:"End ";
        }
        #img1{
            border: 5px red solid;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        
        <fieldset>
        <legend>  My Form  </legend>    
    <div>
        <h1 id="heading22" style="color:aqua">Inline CSS</h1>
        <h2 class="heading3 heading33">Internal CSS</h2>
        <h3>External CSS</h3>


        <p>Formatting TAGS</p>
        <p><b>Welcome To HTML</b></p>
        <p><i>Welcome To HTML</i></p>
        <p><small>Welcome To HTML</small></p>
        <p>Welcome<sub>To</sub>Welcome To HTML</p>
        <p>Welcome<sup>To</sup>Welcome To HTML</p>
        <p><strong>Welcome To HTML</strong></p>
        <p><del>Welcome To HTML</del></p>
        <pre >Welcome To HTML      Welcome To HTML
                    Welcome To HTML     Welcome To HTML
        </pre>
        <p>Welcome To <ins>HTML</ins></p>
        <p><dfn>Welcome</dfn>To HTML</p>
        <code>a piece of code</code>
        <blockquote>Work, Work and work</blockquote>

        <h1>Order List CITIES OF SINDH PROVINCE</h1>
        <ol style="list-style-type:lower-roman">
            <li>KARACHI</li>
            <li>HYDARABAD</li>
            <li>SHUKKAR</li>
            <li>DADU</li>
        </ol>

        <h1>UnOrder List CITIES OF SINDH PROVINCE</h1>
        <ul>
            <li>KARACHI</li>
            <li>HYDARABAD</li>
            <li>SHUKKAR</li>
            <li>DADU</li>
        </ul>

        <h1>Definitions regarding computer </h1>
        <dl>
            <dt>Computer</dt>
            <dd>Computer is an electronic machine.</dd>
        </dl>

        <p>Action Link</p>

        <a id="link1" href="About.aspx" >Click to Download</a><br />
        <a id="link2" href="About.aspx"  target="_self">Click to open webpage Inner</a><br />
        <a id="link3" href="About.aspx"  target="_blank">Click to open webpage new window</a><br />
        <a id="link4" href="WIP-6th-anniversary-wallpaper-dark.jpg" >Click to open image</a><br />


        <input type="text" id="txt1"  />
        <input type="text" id="txt2"  />

        <p id="P1">Definitions regarding computer Definitions regarding computer Definitions regarding computer Definitions<br /> regarding computer Definitions regarding computer Definitions regarding computer Definitions regarding computer Definitions regarding computer</p>
    </div>
        <div>
            <figure id="img1">
                <img src="business-cover.jpg" width="380" height="250"/>
                 <img src="business-cover.jpg" width="380" height="250"/>
                 <img src="business-cover.jpg" width="350" height="250"/>
                <figcaption>Shirt Img</figcaption>
            </figure>
        </div>
        <div >
            <table border="1">
                <thead>
                    <tr><td colspan="3">Student Sheet</td></tr>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Class</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Mehran</td>
                        <td>Programming</td>
                    </tr>
                      <tr>
                        <td>2</td>
                        <td>Zeeshan</td>
                        <td>C#.Net</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="MYDIV">

        </div>
      
        
        </fieldset>
    </form>
</body>
</html>
