﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store_P_With_Business_Object_Class_Library.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            EmployeeBusinessLayer ep = new EmployeeBusinessLayer();
            List<Employee> data = ep.GetAllEmployee();
            return View(data);
        }
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            return View();
        }
        //[HttpPost]
        //public ActionResult Create(FormCollection formCollection)
        //{
        //    //if (ModelState.IsValid)
        //    //{
        //    //    foreach (string  key in formCollection.AllKeys)
        //    //    {
        //    //        Response.Write("Key =" + key + "  ");
        //    //        Response.Write("Value =" + formCollection[key]);
        //    //        Response.Write("<br/>");
        //    //    }
        //    //}
        //    Employee e = new Employee();
        //    e.Name = formCollection["name"];
        //    e.Designation = formCollection["Designation"];
        //    e.Salary =int.Parse(formCollection["Salary"]);

        //    EmployeeBusinessLayer eb = new EmployeeBusinessLayer();
        //    eb.AddEmployee(e);
        //    return RedirectToAction("Index");
        //}
        [HttpPost]
        [ActionName("Create")]
        public ActionResult Create_Post()
        {
            Employee e = new Employee();
            EmployeeBusinessLayer eb = new EmployeeBusinessLayer();
            TryUpdateModel(e);
            if (ModelState.IsValid)
            {
              //UpdateModel<Employee>(e);
                eb.AddEmployee(e);
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(int id)
        {
            EmployeeBusinessLayer eb = new EmployeeBusinessLayer();
            Employee data = eb.GetAllEmployee().FirstOrDefault(x => x.ID == id);
            return View(data);
        }
        [HttpPost]
        [ActionName("Edit")]
        public ActionResult Edit_Post(int id)
        {
            EmployeeBusinessLayer eb = new EmployeeBusinessLayer();
            Employee emp = eb.GetAllEmployee().FirstOrDefault(x => x.ID  == id);

            UpdateModel(emp,new string[] { "ID", "Designation", "Salary" });
            if (ModelState.IsValid)
            {
                eb.UpdateEmployee(emp);
                return RedirectToAction("Index");
            }
            //eb.UpdateEmployee(e);
            return View(emp);
        }
        public ActionResult Delete(int id)
        {
            EmployeeBusinessLayer eb = new EmployeeBusinessLayer();
            eb.DeleteEmployee(id);
            return RedirectToAction("Index");
        }
    }
}