﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class Employee
    {
        public int ID
        {
            get; set;
        }
        public string  Name
        {
            get; set;
        }
        
        public string  Designation
        {
            get; set;
        }
        public int Salary
        {
            get; set;
        }
    }
}
