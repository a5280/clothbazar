﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace BusinessLayer
{
   public  class EmployeeBusinessLayer
    {
        public List<Employee> GetAllEmployee()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            List<Employee> emp = new List<Employee>();

            using (SqlConnection con=new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetEmployee",con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    Employee em = new Employee();
                    em.ID= Convert.ToInt32(rd["ID"]);
                    em.Name = rd["Name"].ToString();
                    em.Designation = rd["Designation"].ToString();
                    em.Salary = Convert.ToInt32(rd["Salary"]);
                    emp.Add(em);
                }

            }

            return emp;
        }

        public void AddEmployee(Employee employee)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("AddEmployee", con)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter paname = new SqlParameter
                {
                    ParameterName = "@name",
                    Value=employee.Name
                };
                cmd.Parameters.Add(paname);

                SqlParameter padesi = new SqlParameter
                {
                    ParameterName = "@desi",
                    Value = employee.Designation
                };
                cmd.Parameters.Add(padesi);

                SqlParameter pasal = new SqlParameter
                {
                    ParameterName = "@sal",
                    Value = employee.Salary
                };
                cmd.Parameters.Add(pasal);

              

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateEmployee(Employee emp)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("UpdateEmployee", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paid = new SqlParameter
                {
                    ParameterName = "@id",
                    Value = emp.ID
                };
                cmd.Parameters.Add(paid);

                SqlParameter paname = new SqlParameter
                {
                    ParameterName = "@name",
                    Value = emp.Name
                };
                cmd.Parameters.Add(paname);

                SqlParameter padesi = new SqlParameter
                {
                    ParameterName = "@desi",
                    Value = emp.Designation
                };
                cmd.Parameters.Add(padesi);

                SqlParameter pasal = new SqlParameter
                {
                    ParameterName = "@sal",
                    Value = emp.Salary
                };
                cmd.Parameters.Add(pasal);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void DeleteEmployee(int id)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("DeleteEmployee", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter para = new SqlParameter
                {
                    ParameterName = "@id",
                    Value = id
                    
                };
                cmd.Parameters.Add(para);
                con.Open();
                cmd.ExecuteNonQuery();

            }
        }

    }

 
}
