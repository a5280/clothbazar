﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class IndexVM
    {
        public List<Student> Student
        {
            get; set;
        }

        public List<Teacher> Teacher
        {
            get; set;
        }
    }
}