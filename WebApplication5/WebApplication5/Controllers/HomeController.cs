﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var students = GetStudent();
            var teachers = GetTeacher();

            IndexVM Model = new IndexVM();
            Model.Student = students;
            Model.Teacher = teachers;

            return View(Model);
        }

        private List<Student> GetStudent()
        {
            return new List<Student>
            {
                new Student() { StudentId = 1, Code = "L0001", Name = "Amit Gupta" },
                new Student() { StudentId = 2, Code = "L0002", Name = "Chetan Gujjar"},
                new Student() { StudentId = 3, Code = "L0003", Name = "Bhavin Patel" }
            };
        }

        private List<Teacher> GetTeacher()
        {
            return new List<Teacher>
            {
                new Teacher() { TeacherId = 1, Code = "L0001", Name = "Amit " },
                new Teacher() { TeacherId = 2, Code = "L0002", Name = "Chetan"  },
                new Teacher() { TeacherId = 3, Code = "L0003", Name = "Bhavin " }
            };
        }

    }
}