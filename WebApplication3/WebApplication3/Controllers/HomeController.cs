﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            //TempData["Name"] = "asp.net application";

            //string[] games = { "cricket", "football", "baseball", "hockey" };
            //TempData["GamesArray"] = games;

            ViewData["Var1"] = "This is View Data";
            ViewBag.Var2 = "This is View Bag";
            TempData["Var3"] = "This is Temp Data";
            Session["Var4"] = "This is Session Data";
            //TempData.Keep();
            // return RedirectToAction("about");

            Employee emp = new Employee();
            emp.id = 1;
            emp.name = "Mehran";
            emp.age = 22;

            Employee emp1 = new Employee();
            emp1.id = 2;
            emp1.name = "Adnan";
            emp1.age = 23;

            Employee emp2 = new Employee();
            emp2.id = 3;
            emp2.name = "Zeeshan";
            emp2.age = 25;

            List<Employee> EmployeeList = new List<Employee>();
            EmployeeList.Add( emp);
            EmployeeList.Add(emp1);
            EmployeeList.Add(emp2);

            return View(EmployeeList);
        }
        public ActionResult about()
        {
          
            if (TempData["GamesArray"] !=null)
            {
                TempData["GamesArray"].ToString();
            }
          //  TempData.Keep();
            return View();
        }

        public ActionResult contact()
        {
            return View();
        }
    }
}